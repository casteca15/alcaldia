<?php
    include_once("../modelo/Coordinador.php");
    include_once("../modelo/Usuario.php");
    include_once("CtMensajes.php");

    class CtCoordinador{
        private $data = array();
        private $obj = null;
        private $msj = null;
        private $usuario = null;

        public function CtCoordinador($obj, $msj, $usuario){
            $this->obj=$obj;
            $this->msj=$msj;
            $this->usuario=$usuario;
        }

        public function addData($d){
            $this->data[] = $d;
        }

        public function resetearData(){
            $this->data = null;
        }

        public function listar($t){
            if($t == 'c' || $t == 'i')
                $sql = "SELECT * FROM ins_cor WHERE tipo_ins='$t' ;";
            else{
                if($t == "t" || $t == ""){
                    $sql = "SELECT * FROM ins_cor;";
                }else{
                    $sql = "SELECT * FROM ins_cor WHERE ced like '$t%' OR (nom_ins like '$t%' OR ape_ins like '$t%');";
                }
            }
            $r = $this->obj->listar($sql);

            while($f = $r->fetch_array()){
                echo "<tr>";
                echo "<td>".$f[0]."</td>";
                echo "<td>".$f[1]."</td>";
                echo "<td>".$f[2]."</td>";
                echo "<td>".$f[3]."</td>";
                echo "<td>".$f[6]."</td>";
                echo "<td>";
                echo "<a href='?op=rco&edi=".$f[0]."' title='Editar'><span class='glyphicon glyphicon-edit'></span></a>";
                echo "<a href='?op=rco&ele=".$f[0]."' onclick='return confirm(\"Seguro?\");' title='Eliminar' style='margin-left:1em;'><span class='glyphicon glyphicon-remove'></span></a>";
                echo "</td>";
                echo "</tr>";
            }
        }

        public function registrar(){
            $this->obj->cargarDatosArreglo($this->data);
            if($this->obj->guardar()){
                return true;
            }else{
                $this->msj->error("La cedula ya existe");
            }
        }

        public function registrarUsuario(){
            $this->usuario->cargarDatosArreglo($this->data);
            if($this->usuario->guardar()){
                $this->msj->ok("Creado el usuario correctamente");
                return true;
            }else{
                $this->msj->error("La cedula ya existe");
            }
        }

        public function buscar($id){
            $r = $this->obj->buscar($id);
            if($r == "no_encontrado"){
                $this->msj->error("No existe la cedula: $id");
                return false;
            }else{
                return $r;
            }
        }

        public function buscarUsuario($id){
            $r = $this->usuario->buscar($id);
            if($r == "no_encontrado"){
                $this->msj->error("No existe la cedula: $id");
                return false;
            }else{
                return $r;
            }
        }

        public function borrar($id){
           $r = $this->obj->eliminar($id); 
           if($r=="eliminado"){
               return true;
           }else if($r == "no_encontrado" || $r==false){
                return false;
            }
        }

        public function cambiar(){
            $this->obj->cargarDatosArreglo($this->data);
            if($this->obj->editar($this->data[0])){
                return true;
            }else{
                return false;
            }
        }

        public function cambiarUsuario(){
            $this->usuario->cargarDatosArreglo($this->data);
            if($this->usuario->editar($this->data[0])){
                $this->msj->ok("Informacion de usuario actualizada");
            }else{
            }
        }
    } 
    $ctE = new CtCoordinador($obj, $msj, $usuario);
?>
