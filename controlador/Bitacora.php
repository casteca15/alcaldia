<?php
    session_start();
    include_once("Orm.php");

    class Bitacora{
        private $datos = array();
        private $tabla = "bitacora";
        private $con = null;

        public function Bitacora($orm){
            $this->con = $orm;
        }

        public function cargarDatosArreglo($datos){
            $this->datos = $datos;
        }

        public function guardar($accion){
            $a[] = 0;
            $a[] = $_SESSION['usuario'];
            $a[] = date('Y-m-d')." a las ".date('H:i:s');
            $a[] = $accion;
            $r = $this->con->insertar($a, $this->tabla);
            if(!$r){
                return false;
            }else{
                if($r->affected_rows==1) 
                    return true;
                else
                    return false;
            }
        }
    }
    $bitacora = new Bitacora($orm);
?>
