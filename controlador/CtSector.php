<?php
    include_once("../modelo/Sector.php");
    include_once("CtMensajes.php");

    class CtSector{
        private $data = array();
        private $obj = null;
        private $msj = null;

        public function CtSector($obj, $msj){
            $this->obj=$obj;
            $this->msj=$msj;
        }

        public function addData($d){
            $this->data[] = $d;
        }

        public function listar(){
            $r = $this->obj->listar();
            while($f = $r->fetch_array()){
                echo "<tr>";
                echo "<td>".$f[1]."</td>";
                echo "<td>".$f[3]." - ".$f[4]." ".$f[5]."</td>";
                echo "<td>";
                echo "<a href='?op=rse&edi=".$f[0]."' title='Editar'><span class='glyphicon glyphicon-edit'></span></a>";
                echo "<a href='?op=rse&ele=".$f[0]."' onclick='return confirm(\"Seguro?\");' title='Eliminar' style='margin-left:1em;'><span class='glyphicon glyphicon-remove'></span></a>";
                echo "</td>";
                echo "</tr>";
            }
        }

        public function listarOption(){
            $r = $this->obj->listar();
            return $r;
        }

        public function listarBuscar($t){
            if($t == ""){
                $sql = "SELECT * FROM sectores S;";
            }else{
                $sql = "SELECT * FROM sectores S WHERE S.des_com like '$t%' OR S.codigo_sector like '$t%';";
            }
            $r = $this->obj->listarP($sql);
            while($f = $r->fetch_array()){
                echo "<tr>";
                echo "<td>".$f[1]."</td>";
                echo "<td>".$f[2]."</td>";
                echo "<td>".$f[3]." - ".$f[4]." ".$f[5]."</td>";
                echo "<td>";
                echo "<a href='?op=rse&edi=".$f[0]."' title='Editar'><span class='glyphicon glyphicon-edit'></span></a>";
                echo "<a href='?op=rse&ele=".$f[0]."' onclick='return confirm(\"Seguro?\");' title='Eliminar' style='margin-left:1em;'><span class='glyphicon glyphicon-remove'></span></a>";
                echo "</td>";
                echo "</tr>";
            }
        }

        public function listarPersonal($t){
            $sql = "SELECT * FROM $t";
            $r = $this->obj->listarP($sql);
            return $r;
        }

        public function registrar(){
            $this->obj->cargarDatosArreglo($this->data);
            if($this->obj->guardar()){
                echo "<script>alert('Registro correctamente');</script>";
            }else{
                echo "<script>alert('Error no se pudo registrar');</script>";
            }
        }

        public function buscar($id){
            $r = $this->obj->buscar($id);
            if($r == "no_encontrado"){
                $this->msj->error("No existe la cedula: $id");
                return false;
            }else{
                return $r;
            }
        }

        public function borrar($id){
           $r = $this->obj->eliminar($id); 
           if($r=="eliminado"){
                $this->msj->ok("Eliminado correctamente");
           }else if($r == "no_encontrado" || $r==false){
                $this->msj->error("No existe el id: $id o estos datos estan siendo utilizados");
                return false;
            }
        }

        public function cambiar(){
            $this->obj->cargarDatosArreglo($this->data);
            if($this->obj->editar($this->data[0])){
                echo "<script>alert('Cambios realizados correctamente');</script>";
            }else{
                echo "<script>alert('No se pudieron realizar los cambios');</script>";
            }
        }
    } 
    $ctD = new CtSector($obj, $msj);
?>
