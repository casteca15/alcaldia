<?php
    include_once("../modelo/Vacaciones.php");
    include_once("CtMensajes.php");

    class CtVacaciones{
        private $data = array();
        private $obj = null;
        private $msj = null;

        public function CtVacaciones($obj, $msj){
            $this->obj=$obj;
            $this->msj=$msj;
        }

        public function addData($d){
            $this->data[] = $d;
        }

        public function listar(){
            $r = $this->obj->listar();
            while($f = $r->fetch_array()){
                echo "<tr>";
                echo "<td>".$f[0]."</td>";
                echo "<td>".$f[1]."</td>";
                echo "<td>";
                echo "<a href='?op=rpe&edi=".$f[0]."' title='Editar'><span class='glyphicon glyphicon-edit'></span></a>";
                echo "<a href='?op=rpe&ele=".$f[0]."' onclick='return confirm(\"Seguro?\");' title='Eliminar' style='margin-left:1em;'><span class='glyphicon glyphicon-remove'></span></a>";
                echo "</td>";
                echo "</tr>";
            }
        }

        public function listarOption(){
            $r = $this->obj->listar();
            return $r;
        }

        public function listarPersonal($sql){
            $r = $this->obj->listarP($sql);
            $i = 0;
            while($f = $r->fetch_array()){
                $i++;
                echo "<tr>";
                echo "<td>".$i."</td>";
                echo "<td>".$f[9]."</td>";
                echo "<td>".$f[10]." ".$f[11]."</td>";
                echo "<td>".$f[26]."</td>";
                echo "<td>".$f[2]."</td>";
                echo "<td>";
                echo "<button class='btn btn-success' onclick='window.location =\"?op=nvc&idp=".$f[0]."\";'>Ver</button>";
                echo "</td>";
                echo "</tr>";
            }
            return $r;
        }

        public function registrar(){
            $this->obj->cargarDatosArreglo($this->data);
            $r = $this->obj->guardar();
            if($r){
                $this->msj->ok("Solicitud Enviada");
            }else{
                $this->msj->error("La cedula ya existe");
            }
            return $r;
        }

        public function buscar($id){
            $r = $this->obj->buscar($id);
            if($r == "no_encontrado"){
                $this->msj->error("No existe la cedula: $id");
                return false;
            }else{
                return $r;
            }
        }

        public function borrar($id){
           $r = $this->obj->eliminar($id); 
           if($r=="eliminado"){
                $this->msj->ok("Eliminado correctamente");
           }else if($r == "no_encontrado" || $r==false){
                $this->msj->error("No existe el id: $id o estos datos estan siendo utilizados");
                return false;
            }
        }

        public function cambiar(){
            $this->obj->cargarDatosArreglo($this->data);
            if($this->obj->editar($this->data[0])){
                $this->msj->ok("Informacion actualizada");
            }else{
                $this->msj->info("La informacion no fue actualizada");
            }
        }

        public function aprobarPermiso($id){
            $sql = "UPDATE vacaciones SET est_vac='APROBADO' WHERE id_vacacion=$id;"; 
            $r = $this->obj->editarPersonalizado($sql);
            if($r){
                echo "<script>alert('Vacaciones aprobadas Correctamente');</script>";
            }
            return $r;
        }

        public function rechazarPermiso($id){
            $sql = "UPDATE vacaciones SET est_vac='RECHAZADO' WHERE id_vacacion=$id;"; 
            $r = $this->obj->editarPersonalizado($sql);
            if($r){
                echo "<script>alert('vacaciones Rechazadas Correctamente');</script>";
            }
            return $r;
        }
    } 
    $ctP = new CtVacaciones($obj, $msj);
?>
