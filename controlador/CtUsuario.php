<?php   
    session_start();
    include_once("modelo/Orm.php");
    include_once("controlador/CtMensajes.php");
    include_once("controlador/Bitacora.php");

    class CtUsuario{
        private $orm = null;    
        private $msj = null;

        public function CtUsuario($orm, $msj){
            $this->orm = $orm;
            $this->msj = $msj;
        }

        public function iniciarSesion($u, $p){
            if(empty($u) || empty($p)){
               $this->msj->error("Hay Campos Vacios"); 
            }else{
                $r = $this->orm->sesion($u, $p, "usuarios");
                if($r==false){

                    if(isset($_SESSION['intento'])){
                        $_SESSION['intento']++;
                    }else{
                        $_SESSION['intento'] = 1;
                    }

                    echo "<b style='color:red'>Usuario/Contraseña invalido</b>";
                    echo "<br><b style='color:red'>Intento ".$_SESSION['intento']."/3 (A los 3 intentos su usuario sera bloqueado) </b>";
                    if(isset($_SESSION['intento']) && $_SESSION['intento']>=3){
                        echo "<br><b style='color:red'>El usuario $u fue bloqueado</b>";
                        //$this->cambiarEstado($u, "BLOQUEADO");
                        $_SESSION['intento'] = null;
                    }
                }else{
                    $f = $r->fetch_array();
                    $_SESSION['intento'] = null;
                    $_SESSION['usuario'] = $f[0];
                    $_SESSION['nivel'] = $f[2];
                    $_SESSION['cedula'] = $f[3];
                    $_SESSION['nombre'] = $f[6]." ".$f[7];

                    $a[] = 0;
                    $a[] = $u;
                    $a[] = date('Y-m-d')." a las ".date('H:i:s');
                    $a[] = "Inicio sesion";
                    $r = $this->orm->insertar($a, "bitacora");
                    if($f[2] == "WEBMASTER"){
                        header("Location: vista/indexwebmaster.php");
                    }else{
                        echo "<script>window.location = 'vista/index.php';</script>";
                    }
                }
            }
        }

        public function cambiarEstado($u, $estado){
            $sql = "UPDATE usuario SET estado='$estado' WHERE usuario='$u';";
            return $this->orm->editarPersonalizado($sql);
        }

        public function cambiarPassword($apass, $npass){
            $sql = "UPDATE usuarios SET password=md5('$npass') WHERE usuario='".$_SESSION['usuario']."' AND password=md5('$apass');";
            return $this->orm->editarPersonalizado($sql);
        }

        public function listarPreguntas(){
            $sql = "SELECT * FROM preguntas WHERE usuario='".$_SESSION['usuario']."';";
            $r = $this->orm->consultaPersonalizada($sql);
            $i = 0;
            while($f = $r->fetch_assoc()){
                $i++;
                echo "<tr>";
                echo "<td>$i</td>";
                echo "<td>".$f['pregunta']."</td>";
                echo "<td>**********</td>";
                echo "<td>";
                echo "<a href='?op=cpw&ele=".$f['id_pregunta']."' onclick='return confirm(\"Seguro?\");' title='Eliminar' style='margin-left:1em;'><span class='glyphicon glyphicon-remove'></span></a>";
                echo "</td>";
                echo "</tr>";
            }
        }

        public function insertarPregunta($p, $r){
            $sql = "INSERT INTO preguntas VALUES(null, '".$_SESSION['usuario']."', '$p', '$r')";
            return $this->orm->insertarPersonalizado($sql);
        }

        public function eliminarPregunta($id){
           return $this->orm->eliminar('id_pregunta', $id, 'preguntas'); 
        }
    }
    $obj = new CtUsuario($orm, $msj);
?>
