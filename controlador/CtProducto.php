<?php
    include_once("../modelo/Producto.php");
    include_once("CtMensajes.php");

    class CtCliente{
        private $data = array();
        private $obj = null;
        private $msj = null;

        public function CtCliente($obj, $msj){
            $this->obj=$obj;
            $this->msj=$msj;
        }

        public function addData($d){
            $this->data[] = $d;
        }

        public function listar($o){
            if($o=='c'){
                $r = $this->obj->listarCaducados();
            }else if($o=='p'){
                $r = $this->obj->listarProximos();
            }else if($o=='r'){
                $r = $this->obj->listarRecipes();
            }else{
                $r = $this->obj->listar();
            }

            while($f = $r->fetch_array()){
                $d = $f[10]. " Días";
                if($d>=1 && $d <= 10)
                    echo "<tr class='warning'>";
                else if($d > 10)
                    echo "<tr class='success'>";
                else if($d <1){
                    echo "<tr class='danger'>";
                    $d = "VENCIDO";
                }
                echo "<td>".$f[4]."</td>";
                echo "<td>".$f[8]."</td>";
                echo "<td>".$f[5]."</td>";
                echo "<td>".$f[6]."</td>";
                echo "<td>".$f[9]."</td>";
                echo "<td>".$d."</td>";
                echo "<td>";
                echo "<a href='?op=rfa&edi=".$f[0]."' title='Editar'><span class='glyphicon glyphicon-edit'></span></a>";
                echo "<a href='?op=rfa&ele=".$f[0]."&lfa' onclick='return confirm(\"Seguro?\");' title='Eliminar' style='margin-left:1em;'><span class='glyphicon glyphicon-remove'></span></a>";
                echo "</td>";
                echo "</tr>";
            }
        }

        public function listarFacturar(){
            $r = $this->obj->listar();
            while($f = $r->fetch_array()){
                $d = $f[10]. " Días";
                if($d>=1 && $d <= 10)
                    echo "<tr class='warning'>";
                else if($d > 10)
                    echo "<tr class='success'>";
                else if($d <1){
                    continue;
                    echo "<tr class='danger'>";
                    $d = "VENCIDO";
                }
                echo "<td>".$f[4]."</td>";
                echo "<td>".$f[8]."</td>";
                echo "<td>".$f[5]."</td>";
                echo "<td>".$f[9]."</td>";
                echo "<td>".$d."</td>";
                echo "<td>";
                echo "<button class='elegir btn btn-success' id='".$f[0]."'>Seleccionar</button>";
                echo "</td>";
                echo "</tr>";
            }
        }
        public function registrar(){
            $this->obj->cargarDatosArreglo($this->data);
            if($this->obj->guardar()){
                $this->msj->ok("Registro Exitosamente");
            }else{
                $this->msj->error("No se pudo registrar");
            }
        }

        public function buscar($id){
            $r = $this->obj->buscar($id);
            if($r == "no_encontrado"){
                $this->msj->error("No existe id id: $id");
                return false;
            }else{
                return $r;
            }
        }

        public function listarOptionPresentacion(){
            $r = $this->obj->listarPersonalizado();
            return $r;
        }

        public function listarOptionClientes(){
            $r = $this->obj->listarPersonalizadoZ("SELECT * FROM cliente;");
            return $r;
        }

        public function borrar($id){
           $r = $this->obj->eliminar($id); 
           if($r=="eliminado"){
                $this->msj->ok("Eliminado correctamente");
           }else if($r == "no_encontrado" || $r==false){
                $this->msj->error("No existe el id: $id");
                return false;
            }
        }

        public function cambiar(){
            $this->obj->cargarDatosArreglo($this->data);
            if($this->obj->editar($this->data[0])){
                $this->msj->ok("Informacion actualizada");
            }else{
                $this->msj->info("La informacion no fue actualizada");
            }
        }
    } 
    $objp = new CtCliente($obj, $msj);
?>
