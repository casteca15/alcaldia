<?php
    
    class CtMensajes{

        public function CtMensajes(){
        }
        
        public function ok($texto){
            echo "<div class='alert alert-success alert-dismissable'>";
            echo "<button type='button' class='close' data-dismiss='alert'";
            echo " aria-hidden='true'>";
            echo "&times;";
            echo "</button>";
            echo "$texto";
            echo "</div>";
        }

        public function error($texto){
            echo "<div class='alert alert-danger alert-dismissable'>";
            echo "<button type='button' class='close' data-dismiss='alert'";
            echo " aria-hidden='true'>";
            echo "&times;";
            echo "</button>";
            echo "$texto";
            echo "</div>";
        }

        public function info($texto){
            echo "<div class='alert alert-info alert-dismissable'>";
            echo "<button type='button' class='close' data-dismiss='alert'";
            echo " aria-hidden='true'>";
            echo "&times;";
            echo "</button>";
            echo "$texto";
            echo "</div>";
        }

    }
    
    $msj = new CtMensajes();

?>
