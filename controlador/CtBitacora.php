<?php
    include_once("../modelo/Orm.php");
    include_once("CtMensajes.php");

    class CtBitacora{
        private $data = array();
        private $obj = null;
        private $msj = null;

        public function CtBitacora($obj, $msj){
            $this->obj=$obj;
            $this->msj=$msj;
        }

        public function addData($d){
            $this->data[] = $d;
        }

        public function listar($sql){
            $r = $this->obj->consultaPersonalizada($sql);
            while($f = $r->fetch_array()){
                echo "<tr>";
                echo "<td>".$f[4]." ".$f[5]." ".$f[6]."</td>";
                echo "<td>".$f[3]."</td>";
                echo "<td>".$f[2]."</td>";
                echo "</tr>";
            }
        }

        public function listarOption(){
            $r = $this->obj->listar();
            return $r;
        }

        public function listarEmpleado($sql){
            $r = $this->obj->consultaPersonalizada($sql);
            $i = 0;
            while($f = $r->fetch_array()){
                $i++;
                echo "<tr>";
                echo "<td>".$i."</td>";
                echo "<td>".$f[0]."</td>";
                echo "<td>".$f[1]." ".$f[2]."</td>";
                echo "<td>";
                echo "<button class='btn btn-success' onclick='window.location =\"?op=bit&tip=e&id=".$f[0]."\";'>Ver</button>";
                echo "</td>";
                echo "</tr>";
            }
            return $r;
        }

        public function registrar(){
            $this->obj->cargarDatosArreglo($this->data);
            if($this->obj->guardar()){
                $this->msj->ok("Cambio de guardia realizado.");
            }else{
                $this->msj->error("No se pudo enviar el cambio de guardia");
            }
        }

        public function buscar($id){
            $r = $this->obj->buscar($id);
            if($r == "no_encontrado"){
                $this->msj->error("Cambio no existe: $id");
                return false;
            }else{
                return $r;
            }
        }

        public function borrar($id){
           $r = $this->obj->eliminar($id); 
           if($r=="eliminado"){
                $this->msj->ok("Eliminado correctamente");
           }else if($r == "no_encontrado" || $r==false){
                $this->msj->error("No existe el id: $id o estos datos estan siendo utilizados");
                return false;
            }
        }

        public function cambiar(){
            $this->obj->cargarDatosArreglo($this->data);
            if($this->obj->editar($this->data[0])){
                $this->msj->ok("Informacion actualizada");
            }else{
                $this->msj->info("La informacion no fue actualizada");
            }
        }


    } 
    $ctB = new CtBitacora($orm, $msj);
?>
