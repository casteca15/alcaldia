<?php
    include_once("../modelo/Solictante.php");
    include_once("../modelo/Usuario.php");
    include_once("CtMensajes.php");

    class CtSolicitante{
        private $data = array();
        private $obj = null;
        private $msj = null;
        private $usuario = null;

        public function CtSolicitante($obj, $msj, $usuario){
            $this->obj=$obj;
            $this->msj=$msj;
            $this->usuario=$usuario;
        }

        public function addData($d){
            $this->data[] = $d;
        }

        public function resetearData(){
            $this->data = null;
        }

        public function listar($t){
            if($t != ""){
                $sql = "SELECT * FROM solicitantes S, sectores SE where S.id_sector=SE.id_sector AND (S.ced_sol like '$t%' OR S.nom_sol like '$t%' OR S.ape_sol like '$t%');";
            }else{
                $sql = "SELECT * FROM solicitantes S, sectores SE where S.id_sector=SE.id_sector;";
            }

            $r = $this->obj->listar($sql);

            while($f = $r->fetch_array()){
                echo "<tr>";
                echo "<td>".$f[1]."-".$f[0]."</td>";
                echo "<td>".$f[2]." ".$f[3]."</td>";
                echo "<td>".$f[4]."</td>";
                echo "<td>".$f[7]."</td>";
                echo "<td>".$f[12]."</td>";
                echo "<td>";
                echo "<a href='?op=rso&edi=".$f[0]."' title='Editar'><span class='glyphicon glyphicon-edit'></span></a>";
                echo "<a href='?op=rso&ele=".$f[0]."' onclick='return confirm(\"Seguro?\");' title='Eliminar' style='margin-left:1em;'><span class='glyphicon glyphicon-remove'></span></a>";
                echo "</td>";
                echo "</tr>";
            }
        }

        public function registrar(){
            $this->obj->cargarDatosArreglo($this->data);
            if($this->obj->guardar()){
                return true;
            }else{
                $this->msj->error("La cedula ya existe");
            }
        }

        public function registrarUsuario(){
            $this->usuario->cargarDatosArreglo($this->data);
            if($this->usuario->guardar()){
                $this->msj->ok("Creado el usuario correctamente");
                return true;
            }else{
                $this->msj->error("La cedula ya existe");
            }
        }

        public function buscar($id){
            $r = $this->obj->buscar($id);
            if($r == "no_encontrado"){
                $this->msj->error("No existe la cedula: $id");
                return false;
            }else{
                return $r;
            }
        }

        public function buscarUsuario($id){
            $r = $this->usuario->buscar($id);
            if($r == "no_encontrado"){
                $this->msj->error("No existe la cedula: $id");
                return false;
            }else{
                return $r;
            }
        }

        public function borrar($id){
           $r = $this->obj->eliminar($id); 
           if($r=="eliminado"){
               return true;
           }else if($r == "no_encontrado" || $r==false){
                return false;
            }
        }

        public function cambiar(){
            $this->obj->cargarDatosArreglo($this->data);
            if($this->obj->editar($this->data[0])){
                return true;
            }else{
                return false;
            }
        }

        public function cambiarUsuario(){
            $this->usuario->cargarDatosArreglo($this->data);
            if($this->usuario->editar($this->data[0])){
                $this->msj->ok("Informacion de usuario actualizada");
            }else{
            }
        }
    } 
    $ctE = new CtSolicitante($obj, $msj, $usuario);
?>
