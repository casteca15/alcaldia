-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: alcaldia
-- ------------------------------------------------------
-- Server version	5.5.43-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catastro`
--

DROP TABLE IF EXISTS `catastro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catastro` (
  `id_catastro` int(11) NOT NULL AUTO_INCREMENT,
  `nro_exp` varchar(12) DEFAULT NULL,
  `dir_dom` text,
  `dir_par` text,
  `discusion` text,
  `aprob` text,
  PRIMARY KEY (`id_catastro`),
  KEY `nro_exp` (`nro_exp`),
  CONSTRAINT `catastro_ibfk_1` FOREIGN KEY (`nro_exp`) REFERENCES `expedientes` (`nro_exp`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catastro`
--

LOCK TABLES `catastro` WRITE;
/*!40000 ALTER TABLE `catastro` DISABLE KEYS */;
INSERT INTO `catastro` VALUES (1,'2015-3','Direccion 11','Direccion 22','discusion 1','listo');
/*!40000 ALTER TABLE `catastro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `control_previo`
--

DROP TABLE IF EXISTS `control_previo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control_previo` (
  `id_control_previo` int(11) NOT NULL AUTO_INCREMENT,
  `nro_exp` varchar(10) DEFAULT NULL,
  `acta` varchar(6) DEFAULT NULL,
  `acta2` varchar(6) DEFAULT NULL,
  `fecha_ordi` date DEFAULT NULL,
  `fecha_extra` date DEFAULT NULL,
  `fecha_extra_2` date DEFAULT NULL,
  `resolucion` varchar(10) DEFAULT NULL,
  `gaceta` varchar(10) DEFAULT NULL,
  `fecha_reso` date DEFAULT NULL,
  PRIMARY KEY (`id_control_previo`),
  UNIQUE KEY `nro_exp` (`nro_exp`),
  CONSTRAINT `control_previo_ibfk_1` FOREIGN KEY (`nro_exp`) REFERENCES `expedientes` (`nro_exp`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `control_previo`
--

LOCK TABLES `control_previo` WRITE;
/*!40000 ALTER TABLE `control_previo` DISABLE KEYS */;
INSERT INTO `control_previo` VALUES (2,'2015-4','111','222','2015-11-09','2015-11-10','2015-11-30','5555','9999','2015-11-19'),(3,'2015-3','5555','6666','2015-11-09','2015-11-11','2015-11-18','7776','9999','2015-11-27');
/*!40000 ALTER TABLE `control_previo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expedientes`
--

DROP TABLE IF EXISTS `expedientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expedientes` (
  `nro_exp` varchar(10) NOT NULL,
  `ced_sol` int(11) DEFAULT NULL,
  `fec_exp` date DEFAULT NULL,
  `fec_ava` date DEFAULT NULL,
  `est_ext` text,
  `fec_con` date DEFAULT NULL,
  `fec_reg` date DEFAULT NULL,
  `nro_arc` int(11) DEFAULT NULL,
  `fec_ent` date DEFAULT NULL,
  PRIMARY KEY (`nro_exp`),
  KEY `ced_sol` (`ced_sol`),
  CONSTRAINT `expedientes_ibfk_1` FOREIGN KEY (`ced_sol`) REFERENCES `solicitantes` (`ced_sol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expedientes`
--

LOCK TABLES `expedientes` WRITE;
/*!40000 ALTER TABLE `expedientes` DISABLE KEYS */;
INSERT INTO `expedientes` VALUES ('2015-1',15444222,'2015-07-30','2015-07-10','TERMINADO','2015-09-26','2015-08-29',22,'2015-08-31'),('2015-2',21022119,'2015-10-20','2015-10-02','CONTROL PREVIO','2015-10-31','0000-00-00',0,'0000-00-00'),('2015-3',21024060,'2015-10-25','2015-10-29','TERMINADO','2015-10-31','2015-10-31',1,'2015-10-30'),('2015-4',21921831,'2015-10-25','0000-00-00','POR INSPECCIONAR','0000-00-00','0000-00-00',0,'0000-00-00'),('2015-5',8777222,'2015-11-21','0000-00-00','INICIADO','0000-00-00','0000-00-00',0,'0000-00-00'),('2015-6',9888111,'2015-11-21','0000-00-00','INICIADO','0000-00-00','0000-00-00',0,'0000-00-00');
/*!40000 ALTER TABLE `expedientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historial_cartas`
--

DROP TABLE IF EXISTS `historial_cartas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historial_cartas` (
  `id_historia` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_hist` text,
  `fecha_hist` datetime DEFAULT NULL,
  `tipo_hist` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_historia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historial_cartas`
--

LOCK TABLES `historial_cartas` WRITE;
/*!40000 ALTER TABLE `historial_cartas` DISABLE KEYS */;
/*!40000 ALTER TABLE `historial_cartas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_cor`
--

DROP TABLE IF EXISTS `ins_cor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_cor` (
  `ced` int(11) NOT NULL,
  `nom_ins` varchar(20) DEFAULT NULL,
  `ape_ins` varchar(25) DEFAULT NULL,
  `tel_ins` varchar(12) DEFAULT NULL,
  `tel_ins2` varchar(12) DEFAULT NULL,
  `tel_ins3` varchar(12) DEFAULT NULL,
  `sex_ins` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`ced`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_cor`
--

LOCK TABLES `ins_cor` WRITE;
/*!40000 ALTER TABLE `ins_cor` DISABLE KEYS */;
INSERT INTO `ins_cor` VALUES (5690099,'lorenzoo','medina','9231923121','','','Masculino'),(18777111,'Andres','Eloyy','2131293123','88888888888','99999999999','Masculino'),(19888111,'Rosbelymm','Moraaaa','2734762346',NULL,NULL,'Femenino'),(20111222,'Rosbely ta','Mora','4121112233',NULL,NULL,'Femenino'),(20111223,'Irannys','Zapataa','4121112233',NULL,NULL,'Femenino'),(21024062,'Luis','perez','1231231231',NULL,NULL,'Masculino'),(23444555,'ernesto','fernandez','123123128',NULL,NULL,'Masculino'),(76666222,'luis','lopez','9999999999','22222222222','33333333333','Masculino');
/*!40000 ALTER TABLE `ins_cor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inspecciones`
--

DROP TABLE IF EXISTS `inspecciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inspecciones` (
  `id_inspeccion` int(11) NOT NULL AUTO_INCREMENT,
  `nro_exp` varchar(10) DEFAULT NULL,
  `fec_ins` date DEFAULT NULL,
  `ced_ins` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_inspeccion`),
  KEY `nro_exp` (`nro_exp`),
  KEY `ced_ins` (`ced_ins`),
  CONSTRAINT `inspecciones_ibfk_1` FOREIGN KEY (`nro_exp`) REFERENCES `expedientes` (`nro_exp`),
  CONSTRAINT `inspecciones_ibfk_2` FOREIGN KEY (`ced_ins`) REFERENCES `ins_cor` (`ced`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inspecciones`
--

LOCK TABLES `inspecciones` WRITE;
/*!40000 ALTER TABLE `inspecciones` DISABLE KEYS */;
INSERT INTO `inspecciones` VALUES (12,'2015-1','2015-07-16',20111223),(13,'2015-3','2015-10-01',18777111),(14,'2015-2','2015-10-01',20111223),(15,'2015-4','2015-11-09',20111222);
/*!40000 ALTER TABLE `inspecciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `observaciones`
--

DROP TABLE IF EXISTS `observaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `observaciones` (
  `id_observacion` int(11) NOT NULL AUTO_INCREMENT,
  `nro_exp` varchar(10) DEFAULT NULL,
  `observacion` text,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id_observacion`),
  KEY `nro_exp` (`nro_exp`),
  CONSTRAINT `observaciones_ibfk_1` FOREIGN KEY (`nro_exp`) REFERENCES `expedientes` (`nro_exp`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `observaciones`
--

LOCK TABLES `observaciones` WRITE;
/*!40000 ALTER TABLE `observaciones` DISABLE KEYS */;
INSERT INTO `observaciones` VALUES (23,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\n','2015-07-30 10:55:21'),(24,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\nSe asigno fecha (2015-07-16) e inspector (20111223)','2015-07-30 11:15:58'),(25,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\nSe cargaron los datos del terreno, norte: 25 mts, sur: 25 mts, este: 15 mts, oeste: 15 mts.','2015-07-30 11:20:17'),(26,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de avaluo','2015-07-30 11:20:39'),(27,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de control previo','2015-07-30 11:20:50'),(28,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de registro','2015-07-30 11:20:58'),(29,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de entrega y el numero del expediente','2015-07-30 11:21:16'),(30,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de control previo','2015-07-30 11:24:43'),(31,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\nSe asigno fecha (2015-07-16) e inspector (20111223)','2015-07-30 13:56:44'),(32,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\nSe cargaron los datos del terreno, norte: 20 mts, sur: 20 mts, este: 30 mts, oeste: 30 mts.','2015-07-30 14:21:31'),(33,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de avaluo','2015-07-30 14:21:39'),(34,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de registro','2015-07-30 14:28:15'),(35,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de control previo','2015-07-30 14:28:31'),(36,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de registro','2015-07-30 14:33:06'),(37,'2015-1','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de entrega y el numero del expediente','2015-07-30 14:37:07'),(38,'2015-2','root REALIZO LA SIGUIENTE OBSERVACION:\nIniciamos el expediente','2015-10-20 15:13:52'),(39,'2015-3','root REALIZO LA SIGUIENTE OBSERVACION:\niniciando el expediente','2015-10-25 18:07:56'),(40,'2015-3','root REALIZO LA SIGUIENTE OBSERVACION:\nSe asigno fecha (2015-10-01) e inspector (18777111)','2015-10-25 18:49:18'),(41,'2015-3','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de avaluo','2015-10-25 20:37:11'),(42,'2015-3','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de control previo','2015-10-25 20:37:19'),(43,'2015-3','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de registro','2015-10-25 20:37:30'),(44,'2015-3','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de entrega y el numero del expediente','2015-10-25 20:37:38'),(45,'2015-2','root REALIZO LA SIGUIENTE OBSERVACION:\nSe asigno fecha (2015-10-01) e inspector (20111223)','2015-10-25 21:04:55'),(46,'2015-2','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de avaluo','2015-10-25 21:05:30'),(47,'2015-2','root REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de control previo','2015-10-25 21:05:43'),(48,'2015-4','root REALIZO LA SIGUIENTE OBSERVACION:\n','2015-10-25 21:54:30'),(49,'2015-4','root REALIZO LA SIGUIENTE OBSERVACION:\nSe asigno fecha (2015-11-09) e inspector (20111222)','2015-11-09 11:11:48'),(50,'2015-5','root REALIZO LA SIGUIENTE OBSERVACION: EL EXPEDIENTE FUE INICIADO ','2015-11-21 13:35:06'),(51,'2015-6','root REALIZO LA SIGUIENTE OBSERVACION: EL EXPEDIENTE FUE INICIADO ','2015-11-21 15:41:58');
/*!40000 ALTER TABLE `observaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sectores`
--

DROP TABLE IF EXISTS `sectores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sectores` (
  `id_sector` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_sector` int(11) DEFAULT NULL,
  `des_com` text,
  `cedula` int(11) DEFAULT NULL,
  `nombres` varchar(25) DEFAULT NULL,
  `apellidos` varchar(25) DEFAULT NULL,
  `tele1` varchar(12) DEFAULT NULL,
  `tele2` varchar(12) DEFAULT NULL,
  `tele3` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id_sector`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sectores`
--

LOCK TABLES `sectores` WRITE;
/*!40000 ALTER TABLE `sectores` DISABLE KEYS */;
INSERT INTO `sectores` VALUES (1,2,'Los proceres - luisa caceres de arismendy',21939834,'jose','castillo','21903014090','',''),(3,2,'La arenosa - Carrera',82198931,'carlos','alberto','21345675675','',''),(4,2,'la peñita',12938129,'rosbely','mora','20938918931','',''),(7,2,'Los cortijos',87273713,'jasinto','perez','12398129831','',''),(8,1,'Sector La  flecha',62343248,'jose','gonzales','18931289398','66666666666','88888888883'),(9,1,'la pastora',87721376,'mmmmmm','rrrrrrrrrrrrrrrrrr','28131231293','',''),(11,1,'el progreso',88777777,'alinger','duran','21983123192','','');
/*!40000 ALTER TABLE `sectores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitantes`
--

DROP TABLE IF EXISTS `solicitantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitantes` (
  `ced_sol` int(11) NOT NULL,
  `nac_sol` varchar(1) DEFAULT NULL,
  `nom_sol` varchar(20) DEFAULT NULL,
  `ape_sol` varchar(25) DEFAULT NULL,
  `tel_sol` varchar(12) DEFAULT NULL,
  `tel2_sol` varchar(12) DEFAULT NULL,
  `tel3_sol` varchar(12) DEFAULT NULL,
  `sex_sol` varchar(12) DEFAULT NULL,
  `id_sector` int(11) DEFAULT NULL,
  `dir_sol` text,
  PRIMARY KEY (`ced_sol`),
  KEY `id_sector` (`id_sector`),
  CONSTRAINT `solicitantes_ibfk_1` FOREIGN KEY (`id_sector`) REFERENCES `sectores` (`id_sector`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitantes`
--

LOCK TABLES `solicitantes` WRITE;
/*!40000 ALTER TABLE `solicitantes` DISABLE KEYS */;
INSERT INTO `solicitantes` VALUES (8777222,'V','luis','gomez','12210931203','9231219312','12300123123','Masculino',1,'los proceres '),(9888111,'V','carlos daniel','hernandez peña','21381239123','','','Masculino',7,'los proceres'),(15444222,'V','yaneiraa','Reyez','1283912381','1283912382','128391233','Femenino',1,NULL),(21022119,'V','Alinger','escalona','1231412343','1231412344','123141235','Masculino',1,NULL),(21024060,'V','Luisa','Jimenez','2131231231',NULL,NULL,'Femenino',3,NULL),(21921831,'V','alicia','machad','23123123123','','','Femenino',7,NULL);
/*!40000 ALTER TABLE `solicitantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terrenos`
--

DROP TABLE IF EXISTS `terrenos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terrenos` (
  `id_datos` int(11) NOT NULL AUTO_INCREMENT,
  `id_inspeccion` int(11) DEFAULT NULL,
  `norte_t` text,
  `sur_t` text,
  `oeste_t` text,
  `este_t` text,
  `norte` int(11) DEFAULT NULL,
  `sur` int(11) DEFAULT NULL,
  `este` int(11) DEFAULT NULL,
  `oeste` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_datos`),
  KEY `terrenos_ibfk_1` (`id_inspeccion`),
  CONSTRAINT `terrenos_ibfk_1` FOREIGN KEY (`id_inspeccion`) REFERENCES `inspecciones` (`id_inspeccion`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terrenos`
--

LOCK TABLES `terrenos` WRITE;
/*!40000 ALTER TABLE `terrenos` DISABLE KEYS */;
INSERT INTO `terrenos` VALUES (12,13,'tierra','mar abierto','aire verde','luz gris',100,120,170,150),(13,14,'asdasdasdas','asdasdasd','adsdasdasdas','asdasda',2131,2131,1232,3123);
/*!40000 ALTER TABLE `terrenos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `usuario` varchar(25) NOT NULL,
  `password` text,
  `nivel` varchar(15) DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('8777222','f7e8793cd3662e8e74bb0927133d93ae','SOLICITANTE','ACTIVO'),('9888111','22d91d110ea0f358e9298d4ea15774a7','SOLICITANTE','BLOQUEADO'),('carlos','827ccb0eea8a706c4c34a16891f84e7b','SECRETARIA-1','ACTIVO'),('manuel','96917805fd060e3766a9a1b834639d35','SECRETARIA-2','ACTIVO'),('root','827ccb0eea8a706c4c34a16891f84e7b','JEFE','ACTIVO');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-21 16:32:50
