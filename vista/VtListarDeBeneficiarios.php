<?php
    include_once("../controlador/CtSolicitante.php");
    include_once("../controlador/CtSector.php");
    include_once("../modelo/Orm.php");
?>
<div class="container">
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title">LISTADO DE BENEFICIARIOS</h3>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>CÉDULA</th>
                        <th>NOMBRE Y APELLIDO</th>
                        <th>TELEFONO</th>
                        <th>SEXO</th>
                        <th>COMUNIDAD</th>
                        <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql = "SELECT * FROM solicitantes S, sectores SE where S.id_sector=SE.id_sector;";
                    $r = $orm->consultaPersonalizada($sql);

                    while($f = $r->fetch_array()){
                        echo "<tr>";
                        echo "<td>".$f[1]."-".$f[0]."</td>";
                        echo "<td>".$f[2]." ".$f[3]."</td>";
                        echo "<td>".$f[4]."</td>";
                        echo "<td>".$f[7]."</td>";
                        echo "<td>".$f[12]."</td>";
                        echo "<td>";
                        echo "<a href='?op=rso&edi=".$f[0]."' title='Editar'><span class='glyphicon glyphicon-edit'></span></a>";
                        echo "</td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            <br><br>
        </div>
    </div>
</div>
