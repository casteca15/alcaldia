<?php
    include_once("../modelo/Orm.php");

    if(isset($_GET['btb'])){
        $exp = $_GET['exp'];

        $sql = "SELECT * FROM expedientes E, solicitantes S WHERE E.ced_sol=S.ced_sol AND E.nro_exp='$exp';";
        $r = $orm->consultaPersonalizada($sql);
        if($r->num_rows == 0){
            echo "<script>alert('El expediente seleccionado no existe');</script>";
            echo "<script>window.location = '?op=catas';</script>";
        }else{
            $ENC = true;
            $rs = $r->fetch_assoc();
            $sql = "SELECT * FROM catastro WHERE nro_exp='$exp';";
            $r = $orm->consultaPersonalizada($sql);
            $ri = $r->fetch_assoc();
            if($r->num_rows == 1){
                $ENX = true;
            }
        }
    }else if(isset($_POST['btg']) || isset($_POST['btc'])){
        $idc = $_POST['idc'];
        $exp = $_POST['exp'];
        $dir1 = $_POST['dir1'];
        $dir2 = $_POST['dir2'];
        $dis = $_POST['dis'];
        $apro = $_POST['apro'];

        if(isset($_POST['btg'])){
            $sql = "INSERT INTO catastro VALUES(null, '$exp', '$dir1', '$dir2', '$dis', '$apro');";
            if($r = $orm->insertarPersonalizado($sql)){
                echo "<script>alert('Datos almacenados correctamente');</script>";
                echo "<script>window.location = '?op=catas&exp=$exp&btb';</script>";
            }else{
                echo "<script>alert('No se pudo almacenar la informacion');</script>";
                echo "<script>window.location = '?op=catas';</script>";
            }
        }else if(isset($_POST['btc'])){
            $sql = "UPDATE catastro SET dir_dom='$dir1', dir_par='$dir2', discusion='$dis', aprob='$apro' WHERE id_catastro='$idc';";
            if($orm->editarPersonalizado($sql)){
                echo "<script>alert('Datos actualizados correctamente');</script>";
                echo "<script>window.location = '?op=catas&exp=$exp&btb';</script>";
            }else{
                echo "<script>alert('No se pudo actualizar la informacion');</script>";
                echo "<script>window.location = '?op=catas&exp=$exp&btb';</script>";
            }
        }
    }

?>
<h2 class="alert-success" style="margin:0;">MODULO DE CATASTRO</h2>
<br>
<br>
<div class="col-md-3" style="margin: auto;padding: 0.5em;"></div>
<div class="panel panel-primary col-md-6" style="margin: auto;padding: 0.5em;">
    <?php if(!$ENC){ ?>
    <form action="?op=catas" method="GET">
        <input type="hidden" name="op" value="catas">
        <div class="input-group input-group-sm">
            <span class="input-group-addon">NUMERO DEL EXPEDIENTE</span>
            <input type="text" class="form-control" name="exp" required value="">
            <span class="input-group-addon"></span>
            <input type="submit" name="btb" class="btn btn-success form-control" value="Buscar">
        </div>
    </form>

    <?php }if($ENC){ ?>
    <form action="?op=catas" method="POST">
        <input type="hidden" name="op" value="cata">
        <input type="hidden" name="exp" value="<?php echo $_GET['exp'];?>">
        <input type="hidden" name="idc" value="<?php echo $ri['id_catastro'];?>">
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">DATOS DEL EXPEDIENTE</span>
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">EXPEDIENTE</span>
            <input type="text" class="form-control solonumeros" name="" required value="<?php echo $_GET['exp'];?>" readonly>
            <span class="input-group-addon">ESTATUS</span>
            <input type="text" class="form-control" name="" id="name" required value="<?php echo $rs['est_ext'];?>" readonly>
            <span class="input-group-addon">CI SOLICITANTE</span>
            <input type="text" class="form-control" name="" id="name" required value="<?php echo $rs['ced_sol'];?>" readonly>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">INFO SOLICITANTE</span>
            <input type="text" class="form-control solonumeros" name="" required value="<?php echo $rs['nom_sol']." ".$rs['ape_sol'];?>" readonly>
            <span class="input-group-addon">FECHA DE INGRESO</span>
            <input type="text" class="form-control" required value="<?php echo $rs['fec_exp'];?>" readonly>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">DATOS CATASTRALES</span>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">DIRECCION DE DOMICILIO</span>
        </div>
        <div class="input-group input-group-sm">
        <textarea style="max-width: 100%;width: 100%;resize: none;" name="dir1" rows="4" cols="100" required><?php echo $ri['dir_dom'];?></textarea>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">DIRECCION DE PARCELA</span>
        </div>
        <div class="input-group input-group-sm">
        <textarea style="max-width: 100%;width: 100%;resize: none;" name="dir2" rows="4" cols="100" required><?php echo $ri['dir_par'];?></textarea>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">2DA DISCUSION</span>
        </div>
        <div class="input-group input-group-sm">
        <textarea style="max-width: 100%;width: 100%;resize: none;" name="dis" rows="4" cols="100" required><?php echo $ri['discusion'];?></textarea>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">APROBACION DE EJIDO</span>
        </div>
        <div class="input-group input-group-sm">
        <textarea style="max-width: 100%;width: 100%;resize: none;" name="apro" rows="4" cols="100" required><?php echo $ri['aprob'];?></textarea>
        </div>
        <br>
        <?php
            if($ENX){
        ?>
            <button class='btn btn-info' name="btc" id="btc">Guardar Datos</button>
            <a href="pdfCatastro.php?exp=<?php echo $_GET['exp'];?>" class="btn btn-primary">PDF</a>
        <?php }else{ ?>

            <button class='btn btn-info' name="btg" id="btg">Guardar Datos</button>
        <?php
            }
        ?>
    </form>
    <?php } ?>
</div>
