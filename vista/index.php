<?php
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    session_start();
    if(!$_SESSION['cedula']){
        header("Location: ../");
    }

    include_once("../modelo/Orm.php");
    include_once("../controlador/Bitacora.php");

    $ni = $_SESSION['nivel'];
    $op = $_GET['op'];


    switch($op){
        case "catas": 
            $r = "VtCatastro.php";
            $title = "Documentos de Catastro";
            break;
        case "car_co": 
            $r = "VtCargarPrevio.php";
            $title = "Cargar datos del control previo";
            break;
        case "conf_ing": 
            $r = "VtConfig.php";
            $title = "Configurar nombre del jefe actual";
            break;
        case "inspec": 
            $r = "VtInspeccion.php";
            $title = "Inspección";
            break;
        case "rco": 
            $r = "VtCoordinadores.php";
            $title = "Inspectores";
            break;
        case "rso":
            $r = "VtSolicitante.php";
            $title = "Solicitantes";
            break;
        case "rse": 
            $r = "VtSector.php";
            $title = "Sectores";
            break;
        case "rus": 
            $r = "VtUsuario.php";
            $title = "Usuarios del sistema";
            break;
        case "lbe": 
            $r = "VtListarDeBeneficiarios.php";
            $title = "Listado de Beneficiarios";
            break;
        case "lis_co": 
            $r = "VtListarDeControlPrevio.php";
            $title = "Listado de control previo";
            break;
        case "sex": 
            $r = "VtSeguirExpediente.php";
            $title = "Seguimiento de expediente";
            break;
        case "ver_se": 
            $r = "VtListarExpedientes.php";
            $title = "Expedientes Registrados";
            break;
        case "men": 
            $r = "VtMensura.php";
            $title = "Carta de Mensura y Constancia de tramitacion";
            break;
        case "cex": 
            $r = "VtCrearExpediente.php";
            $title = "Inicializar Expediente";
            break;
        case "lex": 
            $r = "VtListadoDeExpedientes.php";
            $title = "Listado de expedientes";
            break;
        case "lei": 
            $r = "VtListadoDeInspecciones.php";
            $title = "Listado de Inspecciones";
            break;
        case "lea": 
            $r = "VtListadoDeAvaluo.php";
            $title = "Listado de expedientes en Avaluo";
            break;
        case "cam": 
            $r = "VtPassword.php";
            $title = "Cambiar Contraseña";
            break;
        case "sal": 
            session_destroy();
            $bitacora->guardar("Cerro sesion");
            header("Location: ../");
            break;
        default:
            $r = "inicio.php";
            $title="Inicio";
            break;
    }
?>
<!DOCTYPE HTML>
<html lang="ES">
	<head>
		<meta charset="utf-8">
        <title><?php echo $title; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/estilos.css">
		<link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/screen.css">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--<link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">-->

		<script src="js/jquery.js"></script>
		<script src="dist/jquery.validate.js"></script>
        <script src="js/require.js" data-main="js/validaciones.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-ui.js"></script>
		<script src="js/validCampo.js"></script>
		<script src="js/validacionesFechasLetras.js"></script>
	</head>
	<body>
    <div id="contenedor" style="max-width: 100%; width:100%;">
        <a href="index.php"><img src="../vista/img/minis.png"></a>
    </div>
    <nav class="navbar navbar-default topnav" role="navigation">
        <div class="container topnav">
            <div class="navbar-header">
                <a class="btn btn-success navbar-brand"  href="index.php"  id="btn-menu">Inicio</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <?php if($_SESSION['nivel'] == 'SOLICITANTE'){ ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="../vista/img/cerrarsesion.png">
                            Salir <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="?op=sal" style="color: #3c763d;" onclick="return confirm('Esta seguro que desea salir?');">Cerrar Sesion</a></li>
                        </ul>

                    </li>
                    <?php }else{ ?>
                    <?php
                        if($_SESSION['nivel'] == 'JEFE' || $_SESSION['nivel'] == 'SECRETARIA-1'){
                    ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="../vista/img/crear.png">
                            Registros <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="?op=rse">Sectores</a></li>
                            <li><a href="?op=rco&t=t">Inspectores</a></li>
                            <li><a href="?op=rso">Solicitantes</a></a></li>
                        </ul>
                    </li>
                    <?php
                        }
                    ?>
                    <?php
                        if($_SESSION['nivel'] == 'JEFE' || $_SESSION['nivel'] == 'SECRETARIA-2'){
                    ?>
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="../vista/img/abrir.png">
                        Procesos  <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="?op=ver_se">Seguimiento de Expedientes</a></li>
                        <li><a href="?op=inspec">Inspeccion</a></li>
                        <li><a href="?op=car_co">Control Previo</a></li>
                        <li><a href="?op=catas">Catastro</a></li>
                    </ul>
                    </li>
                    <?php
                        }
                    ?>
                    <?php
                        if($_SESSION['nivel'] == 'JEFE' || $_SESSION['nivel'] == 'SECRETARIA-1' || $_SESSION['nivel'] == 'SECRETARIA-2'){
                    ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="../vista/img/acrobat.png">
                            Reportes <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="?op=lei">Listado de Inspecciones</a></li>
                            <li><a href="?op=men">Carta de Mensura y/o Constancia de Tramitacion</a></li>
                            <li><a href="?op=lea&tipo=ava">Listado de enviados avaluo</a></li>
                            <li><a href="?op=lea&tipo=con">Listado de enviados control previo</a></li>
                            <li><a href="?op=lea&tipo=reg">Listado de enviados registro</a></li>
                            <li><a href="?op=lea&tipo=ter">Listado para entregar</a></li>
                            <li><a href="?op=lex">Listado de Expedientes</a></li>
                            <li><a href="?op=lbe">Listado de Beneficiarios</a></li>
                            <li><a href="?op=lis_co">Listado de control previo</a></li>
                    

                        </ul>
                    </li>
                    <?php
                        }
                    ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="../vista/img/login.png">
                            Mantenimiento<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="?op=cam">Cambiar Contraseña</a></li>
                            <?php
                                if($_SESSION['nivel'] == 'JEFE'){
                            ?>
                            <li><a href="?op=rus">Usuarios del sistema</a></li>
                            <li><a href="respaldar.php">Respaldar base de datos</a></li>
                            <li><a href="?op=conf_ing">Configurar Jefe</a></li>
                            <?php
                                }
                            ?>
                            <li><a href="?op=ayu">Ayuda</a></li>
                        </ul>
                    </li>


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="../vista/img/cerrarsesion.png">
                            Salir <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="?op=sal" style="color: #3c763d;" onclick="return confirm('Esta seguro que desea salir?');">Cerrar Sesion</a></li>
                        </ul>

                    </li>
                </ul>
                <?php
                    }
                ?>
            </div>
            <div style="float: right;margin-top: 1.5em;font-size: 11px; color: #1E8E77;">
                <b><?php echo $_SESSION['usuario']." - ".date("Y-m-d"); ?></b>
                <br>
                <b><?php echo $_SESSION['nivel']; ?></b>
            </div>
        </div>
    </nav> 
<?php
        if($nxx>=180){
            echo "<div class='alert alert-danger alert-dismissable'>";
            echo "<button type='button' class='close' data-dismiss='alert'";
            echo " aria-hidden='true'>";
            echo "&times;";
            echo "</button>";
            echo "Hace mas de 180 días usted tiene la misma contraseña, por su seguridad establezca una nueva clave";
            echo "&nbsp;&nbsp;<a href='?op=cpw'>Click aqui</a>";
            echo "</div>";
        }
?>
        <div id="central" class="row row-centered">
            <?php
                // Incluye el archivo segun la opcion seleccionada
                if($_SESSION['nivel'] == 'SOLICITANTE' && !isset($_GET['op'])){
                    $sql = "SELECT * FROM expedientes WHERE ced_sol='".$_SESSION['usuario']."';";
                    $ksd = $orm->consultaPersonalizada($sql);
                    $ksdx = $ksd->fetch_assoc();
                    echo "<br><br><br><a href='?op=sex&id=".$ksdx['nro_exp']."' class='btn btn-primary'>VER MI EXPEDIENTE</a>";
                }else if($_SESSION['nivel'] == 'SOLICITANTE' && isset($_GET['op'])){
                    include($r);
                }else{
                    include($r);
                }
            ?>
        </div>
        <br><br>
    <footer>
        <div class="row">
            <div class="col-md-6">
                <br>
                <b>Desarrolladores: </b> TODOS LOS DERECHOS RESERVADOS
            </div>
            <div class="col-md-6">
                <br>
                <ul class="list-inline intro-social-buttons">
                    <li>
                        <a href="#" class="btn btn-primary" id="btn-social">
                        <i class="fa fa-twitter"></i> 
                        <span class="network-name">Twitter</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-primary" id="btn-social">
                        <i class="fa fa-facebook"></i> 
                        <span class="network-name">Facebook</span></a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-primary" id="btn-social">
                        <i class="fa fa-google-plus fa-fw"></i> 
                        <span class="network-name">Google +</span></a>
                    </li>
                </ul><br>
            </div>
        </div>
    </footer>
	</body>
</html>	
