<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include ("../PDF/fpdf.php");
    include_once("../modelo/Orm.php");
	
	$cedula = $_GET['ced'];

    $sql = "SELECT * FROM solicitantes SO, sectores S, expedientes EX, inspecciones I WHERE SO.ced_sol='$cedula' AND SO.id_sector=S.id_sector AND SO.ced_sol=EX.ced_sol AND EX.nro_exp=I.nro_exp;";

    $r = $orm->consultaPersonalizada($sql);
    $f = $r->fetch_assoc();

	class Mipdf extends FPDF{

		function Header(){
			$this->Image("img/minis.png", 5,5,200);
		    $this->SetFont("Arial","B",12);
		    $this->Cell(30);

		    $this->setTextColor(0,0,0);
		    $this->Ln(12);
		    $this->Cell(190,10, utf8_decode("SEGUIMIENTO Y CONTROL DE EXPEDIENTES - CTU".date('Y')),0,1,'C',false);
		    $this->Ln(2);
		}

		Function Footer()
		{
			$this->SetY(-20);
			$this->Cell(20);
		}

	}
    $nro =$f['nro_exp'];
	$mipdf=new Mipdf();
	$mipdf->addPage();
	$mipdf->SetFont("Arial","B",9);
    $mipdf->Ln();
    $mipdf->Cell(30,9,"EXPEDIENTE                                                      ".$nro,0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"NACIONALIDAD                                                 ".(($f['nac_sol']=='V')?'VENEZOLANA':'EXTRANJERA'),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"CEDULA                                                              ".$f['ced_sol'],0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"NOMBRE                                                             ".strtoupper($f['nom_sol']),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"APELLIDO                                                           ".strtoupper($f['ape_sol']),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"TELEFONO                                                         ".strtoupper($f['tel_sol']),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"COMUNIDAD                                                      ".strtoupper($f['des_sec']),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"FECHA DE SOLICITUD                                      ".strtoupper($f['fec_exp']),0,'C');
    $mipdf->Ln();
    $mipdf->Ln();
    $mipdf->Cell(30,9,"FECHA PARA INSPECCION                             ".strtoupper($f['fec_ins']),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"FECHA PARA AVALUO                                    ".strtoupper($f['fec_ava']),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"FECHA PARA CONTROL P                              ".strtoupper($f['fec_con']),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"FECHA PARA REGISTRO                                ".strtoupper($f['fec_reg']),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"NUMERO EN ARCHIVO                                    ".strtoupper($f['nro_arc']),0,'C');
    $mipdf->Ln();
    $mipdf->Ln();
    $mipdf->Cell(30,9,"OBSERVACIONES: ",0,'C');
    $mipdf->Ln();

    $mipdf->Output();
?>
