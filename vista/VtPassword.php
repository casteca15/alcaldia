<h2 class="alert-success" style="margin:0;">Cambiar Contraseña</h2>
<br>
<?php
    include_once("../controlador/CtUsuario.php");

    if(isset($_POST['btc'])){
        $apas = $_POST['apas'];
        $npas = $_POST['npas'];

        if($obj->cambiarPassword($apas, $npas)){
            echo "<script>alert('Contraseña actualizada correctamente');</script>";
            echo "<script>window.location = '?op=cam';</script>";
        }else{
            echo "<script>alert('No se actualizo la contraseña');</script>";
            echo "<script>window.location = '?op=cam';</script>";
        }
    }
?>
<div class="container">
    <div class="col-md-4" style=""></div>
        <div class="panel panel-primary col-md-4" style="margin:auto;padding: 0.5em;margin-left:1em;">
            <div class="panel-heading">
                <h3 class="panel-title">Cambiar Contraseña</h3>
            </div>
            <form id="formulario" class="bs-example bs-example-form" action="?op=cam" method="POST" role="form" style="">
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">CONTRASEÑA ACTUAL</span>
                    <input type="password" class="form-control" title="" minlength="4" maxlength="100" name="apas" required maxlength="50" >
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">NUEVA CONTRASEÑA&nbsp;&nbsp; </span>
                    <input type="password" class="form-control" title="" minlength="4" maxlength="100" name="npas" required maxlength="50" id="clave">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">CONFIRMAR CONTRASEÑA&nbsp;&nbsp; </span>
                    <input type="password" class="form-control" title="" minlength="4" maxlength="100" id="conf" required maxlength="50" >
                </div>
                <b style="color: red;display: none" id="msje">Contraseña no coinciden</b>
                <br>
                <button type="submit" class="btn btn-info" name="btc" id="btc">Cambiar Contraseña</button>
                </div>
            </form>
        </div>
    </div>
</div>
    <script>
        $(document).on('ready', function(){
            $("#opc").on('change', function(){
                $("#preg").val($("#opc option:selected").val());
                $("#opc").val('---');
            });
            $("#conf").on('keyup', function(){
                tx = $("#clave").val();
                tx2 = $(this).val();
                if(tx2 != tx){
                    $("#msje").show();
                    $("#btc").hide();
                }else{
                    $("#msje").hide();
                    $("#btc").show();
                }
            });
        });
    </script>
