<?php
    include_once("../controlador/CtPermiso.php");
    session_start();
    if($_SESSION['nivel'] == "NORMAL"){
        $extra = " AND ced_emp_per='".$_SESSION['cedula']."'";
    }else{
        $extra = "";
    }
    if(isset($_GET['s'])){
        $tit = "Permisos Sin Procesar";
        $sql = "SELECT * FROM permiso as P, empleado as E, estado as ES, peaje as PE WHERE P.ced_emp_per=E.ced_emp AND E.id_estado=ES.id_estado AND E.id_peaje=PE.id_peaje AND est_per='SIN PROCESAR'$extra;";
    }else if(isset($_GET['a'])){
        $sql = "SELECT * FROM permiso as P, empleado as E, estado as ES, peaje as PE WHERE P.ced_emp_per=E.ced_emp AND E.id_estado=ES.id_estado AND E.id_peaje=PE.id_peaje AND P.est_per='APROBADO'$extra;";
        $tit = "Permisos Aprobados";
    }else if(isset($_GET['r'])){
        $sql = "SELECT * FROM permiso as P, empleado as E, estado as ES, peaje as PE WHERE P.ced_emp_per=E.ced_emp AND E.id_estado=ES.id_estado AND E.id_peaje=PE.id_peaje AND P.est_per='RECHAZADO'$extra;";
        $tit = "Permisos Rechazados";
    }
?>
<h2 class="alert-success" style="margin:0;"><?php echo $tit; ?></h2>
<br>
<div class="container">
    <a class="btn btn-default" href="?op=lps&a">PERMISOS APROBADOS</a>
    <a class="btn btn-default" href="?op=lps&r">PERMISOS RECHAZADOS</a>
    <?php if($_SESSION['nivel'] == "ADMINISTRADOR"){ ?>
    <a class="btn btn-default" href="?op=lps&s">PERMISOS SIN PROCESAR</a>
    <?php } ?>
    <br><br>
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $tit; ?></h3>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>CÉDULA</th>
                        <th>NOMBRE Y APELLIDO</th>
                        <th>PEAJE</th>
                        <th>FECHA SOLICITUD</th>
                        <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $ctP->listarPersonal($sql);
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
