<?php
    include_once("../modelo/Orm.php");

    include_once("../controlador/CtVacaciones.php");
    session_start();
    $dias = array("Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo");
?>
<script src="js/real_time.js"></script>
<h2 class="alert-success" style="margin:0;">Reporte Individual Por Nomina</h2>
<br>
<div class="row">
    <div class="col-md-3" ></div>
    <div class="col-md-5" style="margin: auto;padding: 0.5em;">
        <form action="?op=vnom" method="GET" id="form">
            <div class="input-group input-group-sm">
                <input type="hidden" name="op" value='vnom'>
                <span class="input-group-addon">BUSCAR EMPLEADO</span>
                <input type="text" class="form-control" name="ced" id="ced">
                <span class="input-group-addon" id="nombre"></span>
            </div>
            <br><br>
            <div class="input-group input-group-sm">
                <span class="input-group-addon">MES</span>
                <select class='form-control' name="mes">
                    <option value='01'>ENERO</option>
                    <option value='02'>FEBRERO</option>
                    <option value='03'>MARZO</option>
                    <option value='04'>ABRIL</option>
                    <option value='05'>MAYO</option>
                    <option value='06'>JUNIO</option>
                    <option value='07'>JULIO</option>
                    <option value='08'>AGOSTO</option>
                    <option value='09'>SEPTIEMBRE</option>
                    <option value='10'>OCTUBRE</option>
                    <option value='11'>NOVIEMBRE</option>
                    <option value='12'>DICIEMBRE</option>
                </select>
                <span class="input-group-addon">AÑO</span>
                <select class='form-control' name="ano">
                    <?php 
                        $a = date('Y');
                        for($i = 2010; $i <= $a; $i++){
                            echo "<option>".$i."</option>";
                        }   
                    ?>
                </select>
            </div>
            <br><br>
            <input type="button" id="btver" class='form-control btn btn-success' value="Ver Reporte" disabled>
            <br><br>

        </form>
    </div>
</div>
<?php
    if(isset($_GET['ced'])){
        $ced = $_GET['ced'];
        $mes = $_GET['mes'];
        $ano = $_GET['ano'];

        $sqlem = "SELECT * FROM empleado E, estado ES, cargo C WHERE E.ced_emp='$ced' AND E.id_estado=ES.id_estado AND E.id_cargo=C.id_cargo";
        $rem = $orm->consultaPersonalizada($sqlem);
        if($rem->num_rows == 0){
            echo "<script>alert('Usuario no existe');</script>";
            echo "<script>window.location = '?op=vnom';</script>";
        }
        $fem = $rem->fetch_assoc();

        function getUltimoDiaMes($elAnio,$elMes) {
            return date("d",(mktime(0,0,0,$elMes+1,1,$elAnio)-1));
        }

        function diaSemana($ano,$mes,$dia) {
            // 0->domingo     | 6->sabado
            $dia= date("w",mktime(0, 0, 0, $mes, $dia, $ano));
            if($dia==0)
                return 7;
            return $dia;
        }

    ?>
    <div class='row'> 
        <form action="../excel.php" method="POST" target="__blank" id="forexcel">
            <div class="col-md-3"></div> 
            <div class="col-md-5 input-group input-group-sm">
                <input type="hidden" id="data" name="data">
                <input type="button" class='btn btn-warning' value='Exportar a Excel' id="bti">
            </div>
        </form>
    </div>

    <br><br>
    <div class="row">
        <div class="col-md-12" >
            <table border=1 style="width: 100%;" id="tabla">
                <tr>
                    <th colspan=3 style="text-align: center;" bgcolor="#eee">NOMBRE Y APELLIDO</th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee">C.I</th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee">MES</th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee">AÑO</th>
                    <th colspan=3 style="text-align: center;" bgcolor="#eee">ESTADO</th>
                    <th colspan=3 style="text-align: center;" bgcolor="#eee">ERP</th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee">CARGO</th>
                </tr>


                <tr>
                <th colspan=3 style="text-align: center;" bgcolor="#eee" class="nombre"><?php echo $fem['nom_emp']." ".$fem['ape_emp'];?></th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee" id="cedula"><?php echo $fem['ced_emp'];?></th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" id="mes"><?php echo $mes;?></th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee" id="ano"><?php echo $ano;?></th>
                    <th colspan=3 style="text-align: center;" bgcolor="#eee" id="estado"><?php echo $fem['estado'];?></th>
                    <th colspan=3 style="text-align: center;" bgcolor="#eee" id="erp">LOS HIJITOS</th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee" id="cargo"><?php echo $fem['cargo'];?></th>
                </tr>

                <tr>
                    <th colspan=1 rowspan=2 style="text-align: center;" bgcolor="#eee" >DIA DE LA SEMANA</th>
                    <th colspan=1 rowspan=2 style="text-align: center;" bgcolor="#eee" >FECHA</th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee" >HORARIO DE TRABAJO</th>
                    <th colspan=4 style="text-align: center;" bgcolor="#eee" >SOLICITUDES DE PERMISOS</th>
                    <th colspan=4 style="text-align: center;" bgcolor="#eee" >CAMBIOS DE GUARDIA</th>
                    <th colspan=4 style="text-align: center;" bgcolor="#eee" >PERIODO VACACIONAL</th>
                </tr>

                <tr>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" >ENTRADA</th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" >SALIDA</th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" >DESDE</th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" >HASTA</th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" >REMUNERADO</th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" >DIAS</th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee" >FECHA | TURNO</th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee" >SUPLENTE</th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" >PERIODO</th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" >DIAS</th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" >DESDE</th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" >HASTA</th>
                </tr>
                <?php 
                    for($i = 1 ; $i <= getUltimoDiaMes($ano, $mes); $i++){
                        $di = diaSemana($ano."", $mes."", $i);
                        $sql = "SELECT * FROM permiso WHERE ced_emp_per='$ced' and fec_per='$ano-$mes-$i';";
                        $r = $orm->consultaPersonalizada($sql);
                        $np+= $r->num_rows;
                        $f = $r->fetch_assoc();

                        $sql2 = "SELECT * FROM guardia WHERE ced_emp_env='$ced' AND fec_gua='$ano-$mes-$i';";
                        $r2 = $orm->consultaPersonalizada($sql2);
                        $ng+= $r2->num_rows;
                        $f2 = $r2->fetch_assoc();

                        $sql3 = "SELECT *, datediff(fec_fin_vac, fec_ini_vac) as r FROM vacaciones WHERE ced_emp_vac='$ced' AND fec_vac='$ano-$mes-$i';";
                        $r3 = $orm->consultaPersonalizada($sql3);
                        $nv+= $r3->num_rows;
                        $f3 = $r3->fetch_assoc();
                ?>
                <tr>
                    <td><?php echo substr($dias[$di-1], 0, 3); ?></td>
                    <td><?php echo "$i/$mes/$ano"; ?></td>
                    <td></td>
                    <td></td>
                    <td><?php echo $f['fec_ini_per'];?></td>
                    <td><?php echo $f['fec_fin_per'];?></td>
                    <td></td>
                    <td><?php echo $f['dur_per'];?></td>
                    <td colspan=2><?php echo $f2['dia_cam'];?></td>
                    <td colspan=2><?php echo $f2['ced_emp_cam'];?></td>

                    <td><?php echo substr($f3['fec_vac'], 0, 4);?></td>
                    <td><?php echo $f3['r']?></td>
                    <td><?php echo $f3['fec_ini_vac']?></td>
                    <td><?php echo $f3['fec_fin_vac']?></td>
                </tr>
                <?php
                    }
                ?>
            <tr>
                    
                    <th colspan=3 style="text-align: center;" bgcolor="#eee" ></th>
                    <th colspan=3 style="text-align: center;" bgcolor="#eee" >TOTAL DE PERMISOS SOLICITADOS</th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee" ><?php echo $np;?></th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee" >TOTAL DE CAMBIOS DE GUARDIA</th>
                    <th colspan=2 style="text-align: center;" bgcolor="#eee" ><?php echo $ng;?></th>
                    <th colspan=3 style="text-align: center;" bgcolor="#eee" >TOTAL DE VACACIONES</th>
                    <th colspan=1 style="text-align: center;" bgcolor="#eee" ><?php echo $nv;?></th>
            </tr>

            </table>
        </div>
    </div>
    <br><br>
<?php 
    }
?>

<script>
        $("#ced").on('keyup', function(){
            ced = $(this).val();
            $.post('real_time.php', {tipo: "ced_emp", id: ced}, function(data){
                if(data!=null){
                    $("#nombre").text(data.nom_emp+" "+data.ape_emp);
                    $("#btver").attr('disabled', false);
                }else{
                    $("#nombre").text("Empleado no encontrado");
                    $("#btver").attr('disabled', true);
                }
            });
        });

        $("#btver").on('click', function(){
            $("#form").submit();
        });

        $("#bti").on('click', function(){
            $("#data").val($("<div>").append($("#tabla").eq(0).clone()).html());
            $("#forexcel").submit();
        });


</script>
