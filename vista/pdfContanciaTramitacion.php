<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include ("../PDF/fpdf.php");
    include_once("../modelo/Orm.php");
    $myfile = fopen("ingeniero.txt", "r") or die("Unable to open file!");
    $ing = fgets($myfile);
    fclose($myfile);
	
	$cedula = $_GET['ced'];

    $sql = "SELECT EX.nro_exp, SO.ced_sol ced_sol, SO.nom_sol nom_sol, SO.ape_sol ape_sol, T.norte_t nor, T.este_t est, T.sur_t su, T.oeste_t oes, T.norte norte, T.sur sur, T.este este, T.oeste oeste  FROM solicitantes SO, sectores S, expedientes EX, inspecciones I, terrenos T WHERE SO.ced_sol='$cedula' AND SO.id_sector=S.id_sector AND SO.ced_sol=EX.ced_sol AND EX.nro_exp=I.nro_exp AND I.id_inspeccion=T.id_inspeccion;";
    

    $r = $orm->consultaPersonalizada($sql);
    $f = $r->fetch_assoc();
    if($r->num_rows == 0){
        echo "<script>alert('Esta persona no tiene expediente creado, ni inspecciones');</script>";
        echo "<script>window.location ='index.php';</script>";
    }

	class Mipdf extends FPDF{
		function Header(){
			$this->Image("img/minis.png", 5,5,200);
		    $this->SetFont("Arial","B",12);
		    $this->Cell(30);

		    $this->setTextColor(0,0,0);
		    $this->Ln(12);
		    $this->Cell(190,10,utf8_decode("CONSTANCIA DE TRAMITACION"),0,1,'C',false);
		    $this->Ln(2);
		}

		Function Footer()
		{
			$this->SetY(-20);
			$this->Cell(20);
		}

	}
    $nro = $f['nro_exp'];
	$mipdf=new Mipdf();
	$mipdf->addPage();
	$mipdf->SetFont("Arial","B",9);
    $mipdf->Cell(30,9,"NRO EXP: $nro",0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"BENEFICIARIO: ".utf8_decode(strtoupper($f['nom_sol']." ".$f['ape_sol'])),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"TITULAR DE LA CEDULA DE IDENTIDAD: ".utf8_decode(strtoupper($f['nac_sol']."-".$f['ced_sol'])),0,'C');
    $mipdf->Ln();
    $mipdf->Multicell(190,9,utf8_decode("DIRECCION DEL INMUEBLE: ".utf8_decode(strtoupper($f['des_sec']))));

    $mipdf->Multicell(190,9,utf8_decode("LINDEROS:
        NORTE: ".strtoupper($f['nor']." - ".$f['norte']." METROS")."
        SUR: ".strtoupper($f['su']." - ".$f['sur']." METROS"))."
        ESTE: ".strtoupper($f['est']." - ".$f['este']." METROS")."
        OESTE: ".utf8_decode(strtoupper($f['oes']." - ".$f['oeste']." METROS")));
    $mipdf->Ln();
	$mipdf->SetFont("Arial","B",8);
	$mipdf->Multicell(190,9,utf8_decode("Mediante la presente hacemos constar que el beneficiario antes mencionado, se encuentra en fase de tramitación de documentos."),0,'C');
    $mipdf->Ln();
    $mipdf->Multicell(190,9,utf8_decode("Constrancia de Tramitacion que expide a los fines legales consiguientes, en el despacho del ciudadano, Ingeniero $ing jefe de la oficina tecnica municipal para la regularizacion de la tenencia de las tierras urbanas del municipio Guanare del Estado Portuguesa, a los ".date('d')." de ".date('m')." del ".date('Y')),0,'C');
    $mipdf->Multicell(190,9,utf8_decode("
            ING. $ing\n  JEFE DE COMITE DE TIERRAS URBANAS"),0,'C');
    $mipdf->Output();
?>
