<?php
    include_once("../controlador/CtGuardia.php");
    session_start();

    $sql = "SELECT * FROM expedientes E, solicitantes S WHERE E.ced_sol=S.ced_sol AND (E.est_ext='CONTROL PREVIO' OR E.est_ext='PARA REGISTRO' OR E.est_ext='TERMINADO') ORDER BY E.fec_exp DESC";
    $r = $orm->consultaPersonalizada($sql);

?>
<h2 class="alert-success" style="margin:0;"><?php echo $title; ?></h2>
<br>
<div class="container">
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $title; ?></h3>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>NRO</th>
                        <th>INFORMACION DEL SOLICITANTE</th>
                        <th>FECHA DE APERTURA</th>
                        <th>ESTATUS</th>
                        <th colspan="2">OPCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($f = $r->fetch_assoc()){
                            echo "<tr>";
                            echo "<td>".$f['nro_exp']."</td>";
                            echo "<td>".$f['ced_sol']." - ".$f['nom_sol']." ".$f['ape_sol']."</td>";
                            echo "<td>".$f['fec_exp']."</td>";
                            echo "<td>".$f['est_ext']."</td>";
                            echo "<td><a href='?op=sex&id=".$f['nro_exp']."' class='btn btn-success'>VER</a></td>";
                            echo "<td><a target='__blank' href='pdfControl.php?ced=".$f['ced_sol']."' class='btn btn-primary'>PDF</a></td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
