<?php
    include_once("../modelo/Orm.php");
    session_start();
    
    if(isset($_GET['sec'])){
        $t = $_GET['sec'];
        if($t == "t"){
            $sql = "SELECT * FROM inspecciones I, sectores SE, ins_cor C, terrenos T, expedientes E, solicitantes SO WHERE I.ced_ins=C.ced AND I.id_inspeccion=T.id_inspeccion AND I.nro_exp=E.nro_exp AND E.ced_sol=SO.ced_sol AND SO.id_sector=SE.id_sector;";
            $tit = "LISTADO DE INSPECCIONES";
        }else{
            $sec = $_GET['sec'];
            $sql = "SELECT * FROM inspecciones I, sectores SE, ins_cor C, terrenos T, expedientes E, solicitantes SO WHERE I.ced_ins=C.ced AND I.id_inspeccion=T.id_inspeccion AND I.nro_exp=E.nro_exp AND E.ced_sol=SO.ced_sol AND SO.id_sector=SE.id_sector AND SE.id_sector=$sec;";
            $tit = "LISTADO DE INSPECCIONES POR COMUNIDAD";
        }
    }else{
            $sql = "SELECT * FROM inspecciones I, sectores SE, ins_cor C, terrenos T, expedientes E, solicitantes SO WHERE I.ced_ins=C.ced AND I.id_inspeccion=T.id_inspeccion AND I.nro_exp=E.nro_exp AND E.ced_sol=SO.ced_sol AND SO.id_sector=SE.id_sector;";
            $tit = "LISTADO DE INSPECCIONES";
    }

    $r = $orm->consultaPersonalizada($sql);
    $n = $r->num_rows;
?>
<h2 class="alert-success" style="margin:0;"><?php echo $tit." ($n)"; ?></h2>
<br>
<div class="container">
    <form action="" method="GET">
        <div class="input-group input-group-sm">
            <input type="hidden" name="op" value="lei">
            <span class="input-group-addon">SELECCIONE INSPECCIONES POR COMUNIDAD</span>
            <select name="sec" class="form-control">
                <option value="t">TODOS</option>
                <?php 
                    $sql1 = "SELECT * FROM sectores;";
                    $r1 = $orm->consultaPersonalizada($sql1);
                    while($f1 = $r1->fetch_array()){
                        if($t == $f1[0])
                            echo "<option value='".$f1[0]."' selected>".$f1[1]."</option>";
                        else
                            echo "<option value='".$f1[0]."'>".$f1[1]."</option>";
                    }
                ?>
            </select>
            <span class="input-group-addon"></span>
            <input type="submit" class='btn btn-success form-control' value='VER'>
        </div>
    </form>
    <br>
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $tit; ?></h3>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>INSPECTOR</th>
                        <th>EXPEDIENTE</th>
                        <th>NORTE</th>
                        <th>SUR</th>
                        <th>ESTE</th>
                        <th>OESTE</th>
                        <th>DIRECCION DEL TERRENO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($f = $r->fetch_assoc()){
                            echo "<tr>";
                            echo "<td>".$f['ced']." ".$f['nom_ins']." ".$f['ape_ins']."</td>";
                            echo "<td>".$f['nro_exp']."</td>";
                            echo "<td>".$f['norte_t']."</td>";
                            echo "<td>".$f['sur_t']."</td>";
                            echo "<td>".$f['este_t']."</td>";
                            echo "<td>".$f['oeste_t']."</td>";
                            echo "<td>".$f['des_com']."</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
