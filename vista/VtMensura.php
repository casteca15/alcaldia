<?php
    include_once("../controlador/CtSolicitante.php");
    include_once("../controlador/CtSector.php");
    include_once("../modelo/Orm.php");
    $t = $_GET['t'];

    if(isset($_GET['btg'])){
        if($_GET['tipo'] == "m"){
            echo "<script>window.location = 'pdfMensura.php?ced=".$_GET['ced']."';</script>";
        }else{
            echo "<script>window.location = 'pdfContanciaTramitacion.php?ced=".$_GET['ced']."';</script>";
        }
    }
?>
<script src="js/real_time.js"></script>
<h2 class="alert-success" style="margin:0;">Carta de Mensura y Constancia de tramitacion</h2>
<br>
<br>
<br>
<div class="col-md-4" style="margin: auto;padding: 0.5em;"></div>
<div class="panel panel-primary col-md-5" style="margin: auto;padding: 0.5em;">
    <form method="get" target="__blank" action="">
        <input type="hidden" name="op" value="men">
        <div class="input-group input-group-sm">
            <span class="input-group-addon">INTRODUZCA LA CI DEL SOLICITANTE</span>
            <input type="text" class="form-control solonumeros cedexiste" name="ced" required value="">
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">NOMBRE DEL SOLICITANTE</span>
            <input type="text" class="form-control solonumeros" name="" id="name" required value="" readonly>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">TIPO DE SOLICITUD</span>
            <select name="tipo" class="form-control" required>
                <option></option>
                <option value='m'>CARTA DE MENSURA</option>
                <option value='c'>CONSTANCIA DE TRAMITACION</option>
            </select>
        </div>
        <br><br>
        <button class='btn btn-info abrirdialog' id="btg" disabled name="btg">Generar Carta</button>
    </form>
</div>
<script>
$(document).on('ready', function(){
    $(".cedexiste").on('keyup', function(){
        ced = $(this).val();
        tab = "solicitantes";
        $.post('real_time.php', {tab: tab, tipo: "ced_sol", id: ced}, function(data){
            console.log(data);
            if(data==null){
                $("#btg").attr('disabled', true);
                $("#name").val("");
            }else{
                $("#btg").attr('disabled', false);
                $("#name").val(data.nom_sol+" "+data.ape_sol);
            }
        });
    });
});
</script>
