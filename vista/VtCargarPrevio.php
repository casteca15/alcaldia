<?php
    include_once("../modelo/Orm.php");

    if(isset($_GET['btb'])){
        $exp = $_GET['exp'];

        $sql = "SELECT * FROM inspecciones I, ins_cor C WHERE I.nro_exp='$exp' AND I.ced_ins=C.ced;";
        $r = $orm->consultaPersonalizada($sql);
        if($r->num_rows == 0){
        }else{
            $ENC = true;
            $rs = $r->fetch_assoc();
            $sql = "SELECT * FROM terrenos WHERE id_inspeccion='".$rs['id_inspeccion']."';";
            $r = $orm->consultaPersonalizada($sql);
            $ri = $r->fetch_assoc();

            $sql = "SELECT * FROM control_previo WHERE nro_exp='$exp';";
            $r = $orm->consultaPersonalizada($sql);
            $r3 = $r->fetch_assoc();

            $sql = "SELECT * FROM expedientes WHERE nro_exp='$exp';";
            $r = $orm->consultaPersonalizada($sql);
            $rsol = $r->fetch_assoc();
        }
    }else if(isset($_POST['btg'])){
        $ide = $_POST['exp'];
        $idi = $_POST['idi'];
        $acta1 = $_POST['acta1'];
        $acta2 = $_POST['acta2'];

        $feco = $_POST['fechao'];
        $fece = $_POST['fechae'];

        $reso = $_POST['reso'];
        $fecr = $_POST['fechar'];

        $gace = $_POST['gaceta'];
        $fec2 = $_POST['fechae2'];

        $sql = "INSERT INTO control_previo VALUES(null, '$ide', '$acta1', '$acta2', '$feco', '$fece', '$fec2', '$reso', '$gace', '$fecr');";
        if($r = $orm->insertarPersonalizado($sql)){
            $id = $r->insertId;
            echo "<script>alert('Datos almacenados correctamente');</script>";
            echo "<script>window.location = '?op=car_co&exp=$ide&btb';</script>";
        }else{
            echo "<script>alert('No se pudo almacenar la informacion');</script>";
            echo "<script>window.location = '?op=inspec';</script>";
        }
    }
    
?>

<script src="js/real_time.js"></script>
<h2 class="alert-success" style="margin:0;">CARGAR DATOS DEL CONTROL PREVIO</h2>
<br>
<br>
<div class="col-md-3" style="margin: auto;padding: 0.5em;"></div>
<div class="panel panel-primary col-md-6" style="margin: auto;padding: 0.5em;">
    <?php if(!$ENC){ ?>
    <form action="?op=car_co" method="GET">
        <input type="hidden" name="op" value="car_co">
        <div class="input-group input-group-sm">
            <span class="input-group-addon">NUMERO DEL EXPEDIENTE</span>
            <input type="text" class="form-control" name="exp" required value="">
            <span class="input-group-addon"></span>
            <input type="submit" name="btb" class="btn btn-success form-control" value="Buscar">
        </div>
    </form>
    <?php } ?>
    <?php if($ENC){ ?>
    <br>
    <div class="input-group input-group-sm">
        <span class="input-group-addon">DATOS DEL INSPECTOR</span>
    </div>
    <div class="input-group input-group-sm">
        <span class="input-group-addon">EXPEDIENTE</span>
        <input type="text" class="form-control solonumeros" name="" required value="<?php echo $_GET['exp'];?>" readonly>
        <span class="input-group-addon">CI</span>
        <input type="text" class="form-control solonumeros cedexiste" name="ced" required value="<?php echo $rs['ced_ins'];?>" readonly>
        <span class="input-group-addon">NOMBRE Y APELLIDO</span>
        <input type="text" class="form-control solonumeros" name="" id="name" required value="<?php echo $rs['nom_ins'].' '.$rs['ape_ins'];?>" readonly>
    </div>
    <br>
    <div class="input-group input-group-sm">
        <span class="input-group-addon">TELEFONO</span>
        <input type="text" class="form-control" name="" id="sector" required value="<?php echo $rs['tel_ins'];?>" readonly>
        <span class="input-group-addon">FECHA DE LA INSPECCION</span>
        <input type="text" class="form-control solonumeros" name="fec" required value="<?php echo $rs['fec_ins'];?>" readonly>
    </div>
    <br>
    <form action="?op=car_co" method="POST">
        <input type="hidden" name="idi" value="<?php echo $rs['id_inspeccion'];?>">
        <input type="hidden" name="exp" value="<?php echo $_GET['exp'];?>">
        <div class="input-group input-group-sm">
            <span class="input-group-addon">DATOS DEL CONTROL PREVIO</span>
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">ACTA N</span>
            <input type="text" class="form-control todoletras" name="acta1" id="" minlength="2" maxlength="10" required value="<?php echo $r3['acta'];?>">
            <span class="input-group-addon">FECHA ORDINARIA</span>
            <input type="text" class="form-control calendario" name="fechao" id="" minlength="6" maxlength="10" required value="<?php echo $r3['fecha_ordi'];?>">
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">ACTA N</span>
            <input type="text" class="form-control todoletras" name="acta2" id="" minlength="2" maxlength="10" required value="<?php echo $r3['acta2'];?>">
            <span class="input-group-addon">FECHA EXTRAORDINARIA</span>
            <input type="text" class="form-control calendario" name="fechae" id="" minlength="2" maxlength="10" required value="<?php echo $r3['fecha_extra'];?>">
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">RESOLUCIÓN #</span>
            <input type="text" class="form-control todoletras" name="reso" id="" minlength="4" maxlength="20" required value="<?php echo $r3['resolucion'];?>">
            <span class="input-group-addon">FECHA RESOLUCIÓN</span>
            <input type="text" class="form-control calendario" name="fechar" id="" minlength="6" maxlength="10" required value="<?php echo $r3['fecha_reso'];?>">
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">GACETA MUNICIPAL #</span>
            <input type="text" class="form-control" name="gaceta" id="" minlength="4" maxlength="20" required value="<?php echo $r3['gaceta'];?>">
            <span class="input-group-addon">FECHA EXTRAORDINARIA</span>
            <input type="text" class="form-control calendario" name="fechae2" id="" minlength="6" maxlength="10" required value="<?php echo $r3['fecha_extra_2'];?>">
        </div>
        <br>
        <?php
            if($r3){
                echo "<br><b>Este expediente ya tiene la data del control previo</b>";
            ?>
                <a href="pdfControl.php?ced=<?php echo $rsol['ced_sol'];?>" target="__blank" class="btn btn-success">VER PDF</a>
            <?php
            }else{
        ?>
        <button class='btn btn-info' name="btg" id="btg">Guardar datos del control previo</button>
        <?php
            }
        ?>
        <?php
            }
        ?>
    </form>
</div>
<script>
$(document).on('ready', function(){
    $(".cedexiste").on('keyup', function(){
        ced = $(this).val();
        tab = "solicitantes";
        $.post('real_time.php', {tab: tab, tipo: "ced_sol", id: ced}, function(data){
            console.log(data);
            if(data==null){
                $("#btg").attr('disabled', true);
                $("#name").val("");
            }else{
                $("#btg").attr('disabled', false);
                $("#name").val(data.nom_sol+" "+data.ape_sol);
                $("#sector").val(data.des_sec);
                $("input[name='norte']").val(data.norte);
                $("input[name='sur']").val(data.sur);
                $("input[name='este']").val(data.este);
                $("input[name='oeste']").val(data.oeste);
            }
        });
    });
});
</script>
