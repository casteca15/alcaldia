<?php
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    session_start();
    if(!$_SESSION['usuario']){
        header("Location: ../");
    }
    include_once("../modelo/Orm.php");
    include_once("../controlador/Bitacora.php");
    $ni = $_SESSION['nivel'];
    $op = $_GET['op'];
    switch($op){
        case "bit": 
            $r = "VtBitacora.php";
            $title = "Bitacora";
            break;
        case "cpw": 
            $r = "VtPassword.php";
            $title = "Cambiar Contraseña";
            break;
        case "sal": 
            session_destroy();
            $bitacora->guardar("Cerro sesion");
            header("Location: ../");
            break;
        default:
            $title="Inicio";
            break;
    }
?>
<!DOCTYPE HTML>
<html lang="ES">
	<head>
        <title><?php echo $title; ?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/screen.css">
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
        <script src="js/require.js" data-main="js/validaciones.js"></script>
	</head>
	<body>
        <center><a href="indexwebmaster.php"><img src="img/minis.png" style="max-width: 90%;width: 100%;margin:auto;height:50px;"></a></center>
        <nav class="navbar navbar-default navbar-static-top" id="cabecera" role="navigation" style="background: #eee">
            <div class="navbar-header">
                <a class="navbar-brand" href="indexwebmaster.php">Inicio</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Bitacora <b class="caret"></b></a>
                        <ul class="dropdown-menu">

                            <li><a href="?op=bit&tip=g">General</a></li>
                            <li><a href="?op=bit&tip=e">Por Usuario</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Configuración <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="?op=cpw">Cambiar Contraseña</a></li>
                            <?php if($ni=="WEBMASTER"){ ?>
                            <li><a href="respaldar.php" target="__blank">Respaldar Base de Datos</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Salir <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="?op=sal" onclick="return confirm('Esta seguro que desea salir?');">Cerrar Sesion</a></li>
                    </ul>
                    </li>
                </ul>
            </div>
            <div style="float: right;margin-top: 1.5em;font-size: 11px;">
                <b><?php echo $_SESSION['nivel']; ?></b>
                <br>
                <b><?php echo $_SESSION['nombre']; ?></b>
            </div>
        </nav>
        <div id="central" class="row row-centered">
            <?php
                include($r);
            ?>
        </div>
        <div id="pie">
            <b>Desarrolladores: </b> Miguel L.
        </div>
	</body>
</html>	
