<?php
    include('../PDF/fpdf.php');
    include("../modelo/Orm.php");
    $tipo = $_GET['idp'];
    $fi = $_GET['fecc1'];
    $ff = $_GET['fecc2'];

    $ti = $_GET['tipo'];
    if($ti == "GENERAL"){
        $titulo = "REPORTE DE ".$tipo." SOLICITADO/AS DESDE $fi HASTA $ff";
    }else{
        $ced = $_GET['ced'];
        $sqln = "SELECT * FROM empleado WHERE ced_emp='$ced';";
        $r = $orm->consultaPersonalizada($sqln);
        $nombre = $r->fetch_assoc();

        $titulo = "REPORTE DE ".$tipo." SOLICITADO/AS DESDE $fi HASTA $ff POR:";
        $sub = $ced." - ".$nombre['nom_emp']." ".$nombre['ape_emp'];
    }

    if($tipo == "PERMISOS"){
        $sql = "SELECT *, P.fec_ini_per fini, P.fec_fin_per ffin, P.dur_per dura, P.est_per esta FROM permiso as P, empleado as E, estado as ES, departamento D, peaje PE WHERE ".(($ti=='INDIVIDUAL')?'E.ced_emp=\''.$ced.'\' AND':'')." P.ced_emp_per=E.ced_emp AND E.id_estado=ES.id_estado AND E.id_dep=D.id_dep AND E.id_peaje=PE.id_peaje AND P.fec_ini_per>='$fi' AND P.fec_ini_per<='$ff' ORDER BY E.ced_emp";
    }else if($tipo == "VACACIONES"){
        $sql = "SELECT *, P.fec_ini_vac fini, P.fec_fin_vac ffin, (P.fec_fin_vac - P.fec_ini_vac) dura, P.est_vac esta FROM vacaciones as P, empleado as E, estado as ES, departamento D, peaje PE WHERE ".(($ti=='INDIVIDUAL')?'E.ced_emp=\''.$ced.'\' AND':'')." P.ced_emp_vac=E.ced_emp AND E.id_estado=ES.id_estado AND E.id_dep=D.id_dep AND E.id_peaje=PE.id_peaje AND P.fec_ini_vac>='$fi' AND P.fec_ini_vac<='$ff' ORDER BY E.ced_emp";
    }else{
        return;
    }

    $r = $orm->consultaPersonalizada($sql);

    class PDF extends FPDF{

        function Header(){
            $this->SetFont('Arial', 'B', 16);
            $this->Image('img/minis.png', 5, 5, 350, 20); 
        }

        function setTitulo($title){
            $this->titulo = $title;
        }
    }

    $pdf = new PDF('L', 'mm', 'Legal');
    $pdf->AddPage();
    $pdf->Ln(15);
    
    $pdf->SetFont('Arial', 'B', 15);
    $pdf->Cell(350,10, "Republica Bolivariana de Venezuela Ministerio Poder Popular",0, 0,'C');
    $pdf->Ln();
    $pdf->Cell(350,10, "para el Transporte Terrestre",0, 0,'C');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetTextColor(220,50,50);
    $pdf->Cell(350,10, $titulo,0, 0,'C');
    if($ti == "INDIVIDUAL"){
        $pdf->Ln();
        $pdf->Cell(350,10, $sub,0, 0,'C');
    }
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetDrawColor(0,80,180);
    $pdf->SetTextColor(000,000,000);
    $pdf->Ln();
    $pdf->Cell(10,8, "#",1, 0,'C');
    $pdf->Cell(25,8, "CEDULA",1, 0,'C');
    $pdf->Cell(70,8, "NOMBRE Y APELLIDO",1, 0,'C');
    $pdf->Cell(45,8, "DEPARTAMENTO",1, 0,'C');
    $pdf->Cell(45,8, "PEAJE",1, 0,'C');
    $pdf->Cell(40,8, "FECHA DE INICIO",1, 0,'C');
    $pdf->Cell(40,8, "FECHA DE FIN",1, 0,'C');
    $pdf->Cell(15,8, "DIAS",1, 0,'C');
    $pdf->Cell(40,8, "ESTATUS",1, 0,'C');
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln();
    $i = 0;
    while($f = $r->fetch_assoc()){
        $i++;
        $pdf->Cell(10,8, $i,1, 0,'C');
        $pdf->Cell(25,8, $f['ced_emp'],1, 0,'C');
        $pdf->Cell(70,8, $f['nom_emp']." ".$f['ape_emp'],1, 0,'C');
        $pdf->Cell(45,8, substr($f['nom_dep'], 0, 17),1, 0,'C');
        $pdf->Cell(45,8, substr($f['peaje'],0, 17),1, 0,'C');
        $pdf->Cell(40,8, $f['fini'],1, 0,'C');
        $pdf->Cell(40,8, $f['ffin'],1, 0,'C');
        $pdf->Cell(15,8, $f['dura'],1, 0,'C');
        $pdf->Cell(40,8, $f['esta'],1, 0,'C');
        $pdf->Ln();
    }
    $pdf->Ln();

    $pdf->Output();
?>
