<script src="js/real_time.js"></script>
<?php
    include_once("../modelo/Orm.php");
?>
<h2 class="alert-success" style="margin:0;">Usuarios WebMaster</h2>
<br>
<div class="container">
    <button class='btn btn-info abrirdialog'>Nuevo Usuario Webmaster</button>
    <br>
    <br>
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title">Usuarios Webmaster registrados</h3>
        </div>
        <br>
        <div class="table-responsive">
            <?php
                if(isset($_GET['ele'])){
                    $u = $_GET['ele'];
                    $sql = "DELETE FROM usuario WHERE usuario='$u';";
                    if($orm->consultaPersonalizada($sql)){
                        echo "<script>alert('Eliminado correctamente');</script>;";
                    }
                }
            ?>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>USUARIO</th>
                        <th>CONTRASEÑA</th>
                        <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $sql = "SELECT * FROM usuario WHERE nivel='WEBMASTER';";
                        $r = $orm->consultaPersonalizada($sql);
                        while($f = $r->fetch_assoc()){
                            echo "<tr>";
                            echo "<td>".$f['usuario']."</td>";
                            echo "<td>".$f['password']."</td>";
                            echo "<td>";
                            echo "<a href='?op=web&ele=".$f['usuario']."' onclick='return confirm(\"Seguro?\");' title='Eliminar' style='margin-left:1em;'><span class='glyphicon glyphicon-remove'></span></a>";
                            echo "</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>

    </div>
    <div class="col-md-1" style="">
    </div>
    <div id='mensajes_grandes' style="<?php if(isset($_GET['edi'])) echo 'display:block'; ?>">
        <a href="<?php echo '?op='.$op; ?>" id='cerrar_mensaje_grande'> X </a>
        <div class="panel panel-primary col-md-12" style="margin:auto;padding: 0.5em;">
    <div class="panel panel-primary col-md-12" style="margin:auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title">Nuevo Usuario Webmaster</h3>
        </div>
        <?php
        if(isset($_POST['btg']) || isset($_POST['btc'])){
            $ced = $_POST['idp'];
            $nom = $_POST['nom'];
            $pas = $_POST['pas'];
            if(isset($_POST['btg'])){
                $sql = "INSERT INTO usuario VALUES('$nom', md5('$pas'), 'WEBMASTER', null);";
                if($orm->insertarPersonalizado($sql)){
                    echo "<script>alert('Usuario registrado correctamente');</script>";
                    echo "<script>window.location = '?op=$op';</script>";
                }
            }
        }
        ?>
        <form id="formulario" class="bs-example bs-example-form" action="?op=web" method="POST" role="form" style="" style=''>
            <br>
            <div class="input-group input-group-sm">
                <?php if($ENC){
                ?>
                <input type="hidden" class="form-control" name="idp" required value="<?php echo $rs[0]; ?>" readonly>
                <?php
                }
                ?>
            </div>
            <br>
            <div class="input-group input-group-sm">
                <span class="input-group-addon">NOMBRE DEL USUARIO</span>
                <input type="text" class="form-control" title="" minlength="4" maxlength="100" name="nom" required maxlength="50" style="text-transform: lowercase;">
                <span class="input-group-addon">CONTRASEÑA DEL USUARIO</span>
                <input type="password" class="form-control" title="" minlength="4" maxlength="100" name="pas" required maxlength="50"  style="text-transform: lowercase;">
            </div>
            <br>
                <?php
                    if($ENC){
                ?>
                <button type="submit" class="btn btn-info" name="btc">Guardar Cambios</button>
                <?php
                    }else{
                ?>
                <button type="submit" class="btn btn-success" name="btg">Guardar</button>
                <?php
                    }
                ?>
            </div>
        </form>
    </div>
</div>
</div>
<script>
</script>
