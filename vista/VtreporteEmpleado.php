<?php
    include_once("../modelo/Orm.php");

    if($_GET['tipo']=="e"){
        $sql = "SELECT E.ced_emp, E.nom_emp, E.ape_emp, D.nom_dep, ES.estado, P.peaje from empleado as E, departamento as D, estado as ES, peaje P WHERE E.id_dep=D.id_dep AND E.id_estado=ES.id_estado AND E.id_peaje=P.id_peaje ORDER BY E.ced_emp;";
        $title = "Reporte de Empleados";
        $r = $orm->consultaPersonalizada($sql);
    }else{
        $sql = "select E.ced_emp, E.nom_emp, E.ape_emp, E.tdc_emp, E.tdp_emp, E.tdz_emp, D.nom_dep, P.peaje FROM empleado as E, departamento as D, peaje P WHERE E.id_dep=D.id_dep AND E.id_peaje=P.id_peaje ORDER BY E.ced_emp;";
        $title = "Relación de Tallas";
        $r = $orm->consultaPersonalizada($sql);
    }

    if($_GET['tipo']=="e"){
?>
<h2 class="alert-success" style="margin:0;">Reporte de Empleados</h2>
<br>

<div class="container">
    <a class="btn btn-default" href="?op=repe&tipo=npi">Ver Relación de Tallas</a>
    <a class="btn btn-default" target="__blank" href="pdf.php">EXPORTAR PDF</a>
    <br>
    <br>
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $title; ?></h3>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>CEDULA</th>
                        <th>NOMBRE</th>
                        <th>APELLIDO</th>
                        <th>DEPARTAMENTO</th>
                        <th>PEAJE</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 0;
                        while($f = $r->fetch_array()){
                            $i++;
                            echo "<tr>";
                            echo "<td>$i</td>";
                            echo "<td>".$f[0]."</td>";
                            echo "<td>".$f[1]."</td>";
                            echo "<td>".$f[2]."</td>";
                            echo "<td>".$f[3]."</td>";
                            echo "<td>".$f[5]."</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    </div>
    <?php
        }else{
    ?>
    <h2 class="alert-success" style="margin:0;">Reporte de Empleados</h2>
    <br>
<div class="container">
    <a class="btn btn-default" href="?op=repe&tipo=e">Ver Empleados</a>
    <a class="btn btn-default" target="__blank" href="pdfTallas.php">EXPORTAR PDF</a>
    <br>
    <br>
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $title; ?></h3>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>CEDULA</th>
                        <th>NOMBRE</th>
                        <th>APELLIDO</th>
                        <th>TALLA DE CAMISA</th>
                        <th>TALLA DE PANTALON</th>
                        <th>TALLA DE ZAPATO</th>
                        <th>DEPARTAMENTO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 0;
                        while($f = $r->fetch_array()){
                            $i++;
                            echo "<tr>";
                            echo "<td>$i</td>";
                            echo "<td>".$f[0]."</td>";
                            echo "<td>".$f[1]."</td>";
                            echo "<td>".$f[2]."</td>";
                            echo "<td>".$f[3]."</td>";
                            echo "<td>".$f[4]."</td>";
                            echo "<td>".$f[5]."</td>";
                            echo "<td>".$f[6]."</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
    }
?>
