<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include ("../PDF/fpdf.php");
    include_once("../modelo/Orm.php");
	
	$exp = $_GET['exp'];

    $sql = "SELECT * FROM expedientes E, catastro C, solicitantes S WHERE E.nro_exp=C.nro_exp AND E.nro_exp='$exp' AND E.ced_sol=S.ced_sol;";

    $r = $orm->consultaPersonalizada($sql);
    $f = $r->fetch_assoc();

	class Mipdf extends FPDF{

		function Header(){
			$this->Image("img/minis.png", 5,5,200);
		    $this->SetFont("Arial","B",12);
		    $this->Cell(30);

		    $this->setTextColor(0,0,0);
		    $this->Ln(12);
		    $this->Cell(190,10, utf8_decode("REPUBLICA BOLIVARIANA DE VENEZUELA ESTADO PORTUGUESA"),0,1,'C',false);
		    $this->Ln(2);
		    $this->Cell(190,10, utf8_decode("ALCALDIA DEL MUNICIPIO GUANARE - DIRECCIÓN DE CATASTRO"),0,1,'C',false);
		}

		Function Footer()
		{
			$this->SetY(-20);
			$this->Cell(20);
		}

	}
    $nro =$f['nro_exp'];
	$mipdf=new Mipdf();
	$mipdf->addPage();
	$mipdf->SetFont("Arial","B",9);
    $mipdf->Ln();
    $mipdf->Cell(30,9,"EXPEDIENTE N#                                                  ".$exp,0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"SOLICITANTE                                                       ".$f['ced_sol']." - ".$f['nom_sol']." ".$f['ape_sol'],0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"FECHA DE INGRESO                                           ".$f['fec_exp'],0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"TIPO DE OPERACION                                         __________________________________ ",0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"DIRECCION DE DOMICILIO                               ".($f['dir_dom']),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"DIRECCION DE PARCELA                                 ".($f['dir_par']),0,'C');
    $mipdf->Ln();
    $mipdf->Ln();
    $mipdf->Ln();
	$mipdf->Cell(190,10, utf8_decode("APROBADO EN CAMARA"),0,1,'C',false);
    $mipdf->Ln();
    $mipdf->Ln();
    $mipdf->Cell(30,9,"2DA DISCUSION                                             ".($f['discusion']),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"CONFIRMACION DE EJIDO                       ".($f['aprob']),0,'C');
    $mipdf->Ln();
    $mipdf->Ln();

    $mipdf->Output();
?>
