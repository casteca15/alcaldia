<script src="js/real_time.js"></script>
<?php
    include_once("../controlador/Orm.php");
?>
<h2 class="alert-success" style="margin:0;">Modulo de Usuarios </h2>
<br>
<div class="container">
    <button class='btn btn-info abrirdialog'>Nuevo Usuario</button>
    <br>
    <br>
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title">Usuarios Registrados</h3>
        </div>
        <br>
        <div class="table-responsive">
            <?php
                if(isset($_GET['ele'])){
                    $id = $_GET['ele'];
                    if($orm->eliminar("usuario", $id, "usuarios")){
                        echo "<script>alert('Eliminado correctamente');</script>";
                    }else{
                        echo "<script>alert('No se pudo eliminar');</script>";
                    }
                }
            ?>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>USUARIO</th>
                        <th>PASSWORD</th>
                        <th>NIVEL</th>
                        <th>ESTATUS</th>
                        <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $sql = "SELECT * FROM usuarios;";
                        $r = $orm->consultaPersonalizada($sql);
                        while($f = $r->fetch_assoc()){
                            echo "<tr>";
                            echo "<td>".$f['usuario']."</td>";
                            echo "<td>*****************</td>";
                            echo "<td>".$f['nivel']."</td>";
                            echo "<td>".$f['estado']."</td>";
                            echo "<td><a class='btn btn-' href='?op=rus&edi=".$f['usuario']."'><span class='glyphicon glyphicon-edit'></a></td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
    <a href="index.php" class="btn btn-danger">Salir</a>
        </div>

    </div>
    <div class="col-md-1" style="">
    </div>
    <div id='mensajes_grandes' style="<?php if(isset($_GET['edi'])) echo 'display:block'; ?>">
        <a href="<?php echo '?op='.$op; ?>" id='cerrar_mensaje_grande'> X </a>
    <div class="panel panel-primary col-md-12" style="margin:auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title">Nuevo Usuario</h3>
        </div>
        <?php
        if(isset($_POST['btg']) || isset($_POST['btc'])){
            $usu = $_POST['usu'];
            $pas = $_POST['pas'];
            $niv = $_POST['nivel'];
            $est = $_POST['esta'];
            $sql = "INSERT INTO usuarios VALUES('$usu', md5('$pas'), '$niv', '$est');";
            if(isset($_POST['btg'])){
               if($orm->insertarPersonalizado($sql)){
                echo "<script>alert('Registro Correctamente');</script>";
                echo "<script>window.location = '?op=$op';</script>";
                }else{
                    echo "<script>alert('No se pudo registrar el usuario, asegurese que no exista ya un usuario con el mismo nombre');</script>";
                    echo "<script>window.location = '?op=$op';</script>";
                }
            }
            else if(isset($_POST['btc'])){
                $sql = "UPDATE usuarios SET nivel='$niv', estado='$est' WHERE usuario='$usu';";
                if($orm->editarPersonalizado($sql)){
                    echo "<script>alert('Actualizo correctamente');</script>";
                    echo "<script>window.location = '?op=$op';</script>";
                }else{
                    echo "<script>alert('No se realizo ningun cambio');</script>";
                    echo "<script>window.location = '?op=$op';</script>";
                }
            }
        }else if(isset($_GET['edi'])){
            $id = $_GET['edi'];
            $sql = "SELECT * FROM usuarios WHERE usuario='$id';";
            $r = $orm->consultaPersonalizada($sql);
            if($f = $r->fetch_array()){ 
                $ENC = true;
                $rs = $f; 
            }else{
                $ENC = false;
            }
            
        }
        ?>
        <form id="formulario" class="bs-example bs-example-form validate" action="?op=rus" method="POST" role="form" style="">
            <br>
            <div class="input-group input-group-sm">
                <span class="input-group-addon">USUARIO</span>
                <input type="text" class="form-control" title="" minlength="4" maxlength="20" name="usu" required value="<?php echo $rs[0]; ?>" style="text-transform: none;" <?php if($ENC) echo 'readonly';?>>
                <span class="input-group-addon">NIVEL DE ACCESO</span>
                <select name="nivel" class="form-control" required>
                    <option value="">--</option>
                    <option <?php if($rs[2] == "JEFE") echo "selected";?>>JEFE</option>
                    <option <?php if($rs[2] == "SOLICITANTE") echo "selected";?>>SOLICITANTE</option>
                    <option <?php if($rs[2] == "SECRETARIA-1") echo "selected";?>>SECRETARIA-1</option>
                    <option <?php if($rs[2] == "SECRETARIA-2") echo "selected";?>>SECRETARIA-2</option>
                </select>
                <span class="input-group-addon">ESTATUS</span>
                <select name="esta" class="form-control" required>
                    <option value="">--</option>
                    <option <?php if($rs[3] == "ACTIVO") echo "selected";?>>ACTIVO</option>
                    <option <?php if($rs[3] == "BLOQUEADO") echo "selected";?>>BLOQUEADO</option>
                </select>
            </div>
            <br>
            <?php if(!$ENC){?>
            <div class="input-group input-group-sm">
                <span class="input-group-addon">CONTRASEÑA</span>
                <input type="password" class="form-control" title="" minlength="4" id="pas" name="pas" required value="<?php echo $rs[1]; ?>" maxlength="100" >
                <span class="input-group-addon">CONFIRMAR CONTRASEÑA</span>
                <input type="password" class="form-control" title="" minlength="4" name="cpas" required value="<?php echo $rs[1]; ?>" maxlength="100" >
            </div>
            <?php } ?>
        <br><br>
        <button type="button" class="btn btn-primary" name="btnuevo">Nuevo</button>
        <button type="submit" class="btn btn-success" name="btg" id="btguardarempleado">Guardar</button>
        <button type="submit" class="btn btn-info" name="btc">Editar</button>
        <a href="?op=rus&ele=<?php echo $rs[0];?>" class="btn btn-danger" name="bte">Eliminar</a>
        <a href="?op=rus&t" class="btn btn-danger" name="btcancelar">Cancelar</a>
            </div>
        </form>
    </div>
</div>
</div>
<script>
    $(document).on('ready', function(){

        <?php if(!$ENC){?>
            $("input, select").attr('disabled', true);
            $("button[name='btg']").attr('disabled', true);
            $("button[name='btc']").attr('disabled', true);
            $("a[name='bte']").attr('disabled', true);
        <?php }else{ ?>
            $("input, select").attr('disabled', false);
            $("button[name='btg']").attr('disabled', true);
            $("button[name='btc']").attr('disabled', false);
            $("a[name='bte']").attr('disabled', false);
        <?php } ?>

            $("button[name='btnuevo']").on('click', function(){
                $("input, select").attr('disabled', false);
               $("button[name='btg']").attr('disabled', false); 
            });

    });

</script>
