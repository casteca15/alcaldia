<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include ("../PDF/fpdf.php");
    include_once("../modelo/Orm.php");
    $myfile = fopen("ingeniero.txt", "r") or die("Unable to open file!");
    $ing = fgets($myfile);
    fclose($myfile);
	
	$cedula = $_GET['ced'];

    $sql = "SELECT * FROM solicitantes SO, sectores S, expedientes EX WHERE SO.ced_sol='$cedula' AND SO.id_sector=S.id_sector AND SO.ced_sol=EX.ced_sol;";

    $r = $orm->consultaPersonalizada($sql);
    $f = $r->fetch_assoc();

    $sql = "SELECT * FROM expedientes E, inspecciones I, terrenos T WHERE E.ced_sol='$cedula' AND I.nro_exp=E.nro_exp AND I.id_inspeccion=T.id_inspeccion;";
    $r = $orm->consultaPersonalizada($sql);
    $fx = $r->fetch_assoc();
    if($r->num_rows == 0){
        echo "<script>alert('Este documento no ha sido inspeccionado');</script>";
        echo "<script>window.location = 'index.php';</script>";
    }
    
    $exp = $f['nro_exp'];
    $sql_control = "SELECT * FROM control_previo WHERE nro_exp='$exp';";
    $r = $orm->consultaPersonalizada($sql_control);
    if($r->num_rows == 0){
        echo "<script>alert('Debe cargar los datos de control previo, antes de entrar aqui. Vaya al modulo de procesos');</script>";
        echo "<script>window.location = 'index.php';</script>";
    }
    $f_control = $r->fetch_assoc();

	class Mipdf extends FPDF{
		function Header(){
			$this->Image("img/minis.png", 5,5,200);
		    $this->SetFont("Arial","B",12);
		    $this->Cell(30);

		    $this->setTextColor(0,0,0);
		    $this->Ln(10);
		    $this->Cell(190,10,utf8_decode("CONTROL PREVIO"),0,1,'C',false);
		    $this->Ln(2);
		}

		Function Footer()
		{
			$this->SetY(-20);
			$this->Cell(20);
		}

	}
    $nro =$f['nro_exp'];
	$mipdf=new Mipdf();
	$mipdf->addPage();
	$mipdf->SetFont("Arial","B",9);
    $mipdf->Cell(30,9,"CCP - ".$f_control['id_control_previo'],0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"NRO EXP: $nro",0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"BENEFICIARIO: ".utf8_decode(strtoupper($f['nom_sol']." ".$f['ape_sol'])),0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"TITULAR DE LA CEDULA DE IDENTIDAD: ".utf8_decode(strtoupper($f['nac_sol']."-".$f['ced_sol'])),0,'C');
    $mipdf->Ln();
    $mipdf->Multicell(190,9,utf8_decode("DIRECCION DEL INMUEBLE: ".utf8_decode(strtoupper($f['des_sec']))));

    $mipdf->Multicell(190,9,utf8_decode("LINDEROS:
        NORTE: ".strtoupper($fx['norte_t']." TIENE ".$fx['norte']." METROS")."
        SUR: ".strtoupper($fx['sur_t']." TIENE ".$fx['sur']." METROS"))."
        ESTE: ".strtoupper($fx['este_t']." TIENE ".$fx['este']." METROS")."
        OESTE: ".strtoupper($fx['oeste_t']." TIENE ".$fx['oeste']." METROS"));
    $mipdf->Cell(30,9,"FECHA DE APROBACION EN EL CONCEJO MUNICIPAL",0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"ACTA N:    ".$f_control['acta']."     ORDINARIA FECHA:     ".$f_control['fecha_ordi']." PRIMERA DISCUSION",0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"ACTA N:    ".$f_control['acta2']."     EXTRAORDINARIA FECHA:     ".$f_control['fecha_extra']." SEGUNDO DISCUSION",0,'C');
    $mipdf->Ln();
    $mipdf->Ln();
    $mipdf->Cell(30,9,"AVALUO BS. 0,001                                                    AVALUO BS. 0,001",0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"VALOR TOTAL BS. 0,001                                                    VALOR TOTAL BS. 0,001",0,'C');
    $mipdf->Ln();
    $mipdf->Cell(30,9,"OPERACION: COMPRA",0,'C');
    $mipdf->Ln();
	$mipdf->SetFont("Arial","B",8);
	$mipdf->Multicell(190,9,utf8_decode("Esta solicitud ha cumplido con el requisito de control previo, que ha realizado por la TRANSFERENCIA DEL CONTROL PREVIO a la Alcaldia del Municipio Guanare del Estado Portuguesa, Segun Resolucion N#. ".$f_control['resolucion']." de fecha ".$f_control['fecha_reso'].", la cual fue publicada en Gaceta Municipal N# ".$f_control['gaceta'].", extraordinaria de fecha ".$f_control['fecha_extra_2']),0,'C');
    $mipdf->Ln();
    $mipdf->Multicell(190,9,utf8_decode("CONTROL PREVIO que expide a los fines legales consiguientes, en el despacho del ciudadano, Ingeniero JESUS DANIEL MONTOYA jefe de la oficina tecnica municipal para la regularizacion de la tenencia de las tierras urbanas del municipio Guanare del Estado Portuguesa, a los ".date('d')." de ".date('m')." del ".date('Y')),0,'C');
    $mipdf->Multicell(190,9,utf8_decode("
            ING. $ing\n  JEFE DE COMITE DE TIERRAS URBANAS"),0,'C');
    $mipdf->Output();
?>
