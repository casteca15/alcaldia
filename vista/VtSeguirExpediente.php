<?php
    include_once("../controlador/CtSolicitante.php");
    include_once("../controlador/CtSector.php");
    include_once("../modelo/Orm.php");

    $estatus = array("INICIANDO", "INICIADO", "POR INSPECCIONAR", "INSPECCIONADO", "EN AVALUO", "CONTROL PREVIO", "PARA REGISTRO", "TERMINADO");
    
    if(!isset($_GET['id'])){
        echo "<script>window.location = 'index.php';</script>";
    }
    $id = $_GET['id'];
    $sql = "SELECT * FROM expedientes WHERE nro_exp='$id';";
    $r = $orm->consultaPersonalizada($sql);
    if($r->num_rows == 0){
        echo "<script>alert('Expediente incorrecto');</script>";
        echo "<script>window.location = 'index.php';</script>";
    }

    $rs = $r->fetch_array();

    $sql = "SELECT * FROM inspecciones where nro_exp='$id';";
    $r = $orm->consultaPersonalizada($sql);
    $fs = $r->fetch_array();
    $nro = $rs[0];
    $es = $rs[4]; 

    $sql = "SELECT * FROM terrenos WHERE id_inspeccion='".$fs[0]."';";
    $r = $orm->consultaPersonalizada($sql);
    $xs = $r->fetch_array();

    if(isset($_POST['btgo'])){
        $obs = $_SESSION['usuario']." REALIZO LA SIGUIENTE OBSERVACION:\n".$_POST['obs'];
        $fe = date('Y-m-d H:i:s');
        $sql = "INSERT INTO observaciones VALUES(null, '$id', '$obs', '$fe')";
        if($orm->insertarPersonalizado($sql)){
            echo "<script>alert('Observacion almacenada');</script>";
            echo "<script>window.location = '?op=sex&id=$id';</script>";
        }
    }else if(isset($_POST['btgi'])){
        $ceds = $_POST['ceds'];
        $feci= $_POST['feci'];
        $ins = $_POST['ins'];


        // Insertamos inspecciones
        $sql = "INSERT INTO inspecciones VALUES(null, '$id', '$feci', '$ins');";
        // Eliminamos inspecciones existentes
        $orm->eliminar("nro_exp", $id, "inspecciones");
        if($rins = $orm->insertarPersonalizado($sql)){
            // Insertamos observaciones
            $obs = $_SESSION['usuario']." REALIZO LA SIGUIENTE OBSERVACION:\nSe asigno fecha ($feci) e inspector ($ins)";
            $fea = date('Y-m-d')." ".date('H:i:s');
            $sql = "INSERT INTO observaciones VALUES(null, '$id', '$obs', '$fea')";
            $orm->insertarPersonalizado($sql);

            // Cambiamos el estatus de la inspeccion
            $sql = "UPDATE expedientes set est_ext='POR INSPECCIONAR' WHERE nro_exp='$id';";
            $orm->editarPersonalizado($sql);

            echo "<script>alert('Fecha de inspeccion almacenada');</script>";
            echo "<script>window.location = '?op=sex&id=$id';</script>";
        }

    }else if(isset($_POST['btgdi'])){
        $idins = $_POST['id_ins'];
        $norte = $_POST['nortem'];
        $sur   = $_POST['surm'];
        $este  = $_POST['estem'];
        $oeste = $_POST['oestem'];

        $orm->eliminar("id_inspeccion", $idins, "terrenos");

        $sql = "INSERT INTO terrenos VALUES(null, '$idins', '$norte', '$sur', '$este', '$oeste')";
        if($rdatos = $orm->insertarPersonalizado($sql)){
            $sql = "UPDATE expedientes set est_ext='INSPECCIONADO' WHERE nro_exp='$id';";
            $orm->editarPersonalizado($sql);

            // ALmacenamos observacion
            $fea = date('Y-m-d')." ".date('H:i:s');
            $obs = $_SESSION['usuario']." REALIZO LA SIGUIENTE OBSERVACION:\nSe cargaron los datos del terreno, norte: $norte mts, sur: $sur mts, este: $este mts, oeste: $oeste mts.";
            $sql = "INSERT INTO observaciones VALUES(null, '$id', '$obs', '$fea')";
            $orm->insertarPersonalizado($sql);

            echo "<script>alert('Datos de la inspeccion cargados');</script>";
            echo "<script>window.location = '?op=sex&id=$id';</script>";
        }else{
            echo "<script>alert('No se pudo almacenar los datos, asegurese de que ya tiene fecha de inspeccion e inspector');</script>";
            echo "<script>window.location = '?op=sex&id=$id';</script>";
        }
    }else if(isset($_POST['btgfava'])){
        $fe = $_POST['fecha_ava'];
        $sql = "UPDATE expedientes set fec_ava='$fe', est_ext='EN AVALUO' WHERE nro_exp='$id';";
        if($orm->editarPersonalizado($sql)){
            $fe = date('Y-m-d H:i:s');
            $obs = $_SESSION['usuario']." REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de avaluo";
            $sql = "INSERT INTO observaciones VALUES(null, '$id', '$obs', '$fe')";
            $orm->insertarPersonalizado($sql);
            echo "<script>alert('Fecha de avaluo cargada');</script>";
            echo "<script>window.location = '?op=sex&id=$id';</script>";
        }
    }else if(isset($_POST['btgfctp'])){
        $fe = $_POST['fecha_con'];
        $sql = "UPDATE expedientes set fec_con='$fe', est_ext='CONTROL PREVIO' WHERE nro_exp='$id';";
        if($orm->editarPersonalizado($sql)){
            $fe = date('Y-m-d H:i:s');
            $obs = $_SESSION['usuario']." REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de control previo";
            $sql = "INSERT INTO observaciones VALUES(null, '$id', '$obs', '$fe')";
            $orm->insertarPersonalizado($sql);
            echo "<script>alert('Fecha de control previo cargada');</script>";
            echo "<script>window.location = '?op=sex&id=$id';</script>";
        }
    }else if(isset($_POST['btgfprg'])){
        $fe = $_POST['fecha_con'];
        $sql = "UPDATE expedientes set fec_reg='$fe', est_ext='PARA REGISTRO' WHERE nro_exp='$id';";
        if($orm->editarPersonalizado($sql)){
            $fe = date('Y-m-d H:i:s');
            $obs = $_SESSION['usuario']." REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de registro";
            $sql = "INSERT INTO observaciones VALUES(null, '$id', '$obs', '$fe')";
            $orm->insertarPersonalizado($sql);
            echo "<script>alert('Fecha de registro cargada');</script>";
            echo "<script>window.location = '?op=sex&id=$id';</script>";
        }
    }else if(isset($_POST['bt_terminar'])){
        $fe = $_POST['fecha_ent'];
        $no = $_POST['nro'];
        $sql = "UPDATE expedientes set fec_ent='$fe', nro_arc='$no', est_ext='TERMINADO' WHERE nro_exp='$id';";
        if($orm->editarPersonalizado($sql)){
            $fe = date('Y-m-d H:i:s');
            $obs = $_SESSION['usuario']." REALIZO LA SIGUIENTE OBSERVACION:\n Se cargo la fecha de entrega y el numero del expediente";
            $sql = "INSERT INTO observaciones VALUES(null, '$id', '$obs', '$fe')";
            $orm->insertarPersonalizado($sql);
            echo "<script>alert('Fecha de entrega y numero de registro cargados');</script>";
            echo "<script>window.location = '?op=sex&id=$id';</script>";
        }
    }

?>

<script src="js/real_time.js"></script>
<h2 class="alert-success" style="margin:0;">SEGUIMIENTO DE EXPEDIENTE</h2>
<br>
<br>
<div class="" style="margin: auto;padding: 0.5em;">
    <div class="input-group input-group-sm">
        <span class="input-group-addon">ESTATUS DEL EXPEDIENTE</span>
    <?php 
    $color  = "style='background: #52C341;'";
    for($i = 0; $i < count($estatus); $i++){
        if($estatus[$i] == $rs[4]){
            echo "<span $color class='input-group-addon' title='Actualmente'>*<b> ".$estatus[$i]."</b> *</span>";
            $color  = "style='background: #FF7474;'";
        }else{
            echo "<span $color class='input-group-addon'>".$estatus[$i]."</span>";
        }
    }
    ?>
    </div>
</div>
<br><br>
<div class="col-md-1" style="margin: auto;padding: 0.5em;"></div>
<div class="panel panel-primary col-md-6" style="margin: auto;padding: 0.5em;">
        <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>DATOS DEL SOLICITANTE</b></span>
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">NUMERO DEL EXPEDIENTE</span>
            <input type="text" class="form-control solonumeros" name="exp" required value="<?php echo $nro;?>" readonly>
            <span class="input-group-addon">ESTATUS</span>
            <input type="text" class="form-control" name="est" required value="<?php echo $es;?>" readonly>

        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">CI</span>
            <input type="text" class="form-control solonumeros cedexiste" name="ced" required value="<?php echo $rs[1];?>" readonly>
            <span class="input-group-addon">NOMBRE Y APELLIDO</span>
            <input type="text" class="form-control solonumeros" name="" id="name" required value="" readonly>
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">COMUNIDAD</span>
            <input type="text" class="form-control solonumeros" name="" id="sector" required value="" readonly>
            <span class="input-group-addon">FECHA DE LA SOLICITUD</span>
            <input type="text" class="form-control solonumeros" name="fec" required value="<?php echo $rs[2];?>" readonly>
        </div>
        <br>
        <br>
        <form method="POST" action="?op=sex&id=<?php echo $rs[0];?>">
            <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>ASIGNAR INSPECCION</b></span>
            </div>
            <div class="input-group input-group-sm">
                <span class="input-group-addon">FECHA DE LA INSPECCION</span>
                <input type="text" class="form-control calendario" name="feci" minlength="5" maxlength="150"  readonly required value="<?php echo $fs[2];?>">
                <span class="input-group-addon">INSPECTOR</span>
                <input type="hidden" name="ceds" value="<?php echo $rs[1];?>">
                <select name="ins" class="form-control" required>
                    <option value="">--</option>
                    <?php 
                        $sql = "SELECT * FROM ins_cor;";
                        $r = $orm->consultaPersonalizada($sql);
                        while($f = $r->fetch_array()){
                            if($fs[3] == $f[0])
                                echo "<option value='".$f[0]."' selected>".$f[0]." - ".$f[1]." ".$f[2]."</option>";
                            else
                                echo "<option value='".$f[0]."'>".$f[0]." - ".$f[1]." ".$f[2]."</option>";
                        }
                    ?>
                </select>
            </div>
            <br>
            <input type="submit" value="Asignar Inspector" class="btn btn-success" name="btgi" <?php if($rs[4] != 'INICIADO') echo 'style=\'display:none;\''; ?>>
        </form>
        <br>
        <?php if($xs){ ?>
        <div class="input-group input-group-sm">
            <span class="input-group-addon"><b>DATOS DEL TERRENO</b></span>
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">NORTE</span>
            <input type="text" class="form-control todoletras" id="" minlength="5" maxlength="150" readonly value="<?php echo $xs[2];?>">
            <span class="input-group-addon">METROS</span>
            <input type="text" class="form-control todoletras" id="" minlength="5" maxlength="150" readonly value="<?php echo $xs[6];?>">
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">SUR</span>
            <input type="text" class="form-control todoletras"  id="" minlength="5" maxlength="150"  readonly value="<?php echo $xs[3];?>">
            <span class="input-group-addon">METROS</span>
            <input type="text" class="form-control todoletras"  id="" minlength="5" maxlength="150"  readonly value="<?php echo $xs[7];?>">
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">OESTE</span>
            <input type="text" class="form-control todoletras"  id="" minlength="5" maxlength="150"  readonly value="<?php echo $xs[4];?>">
            <span class="input-group-addon">METROS</span>
            <input type="text" class="form-control todoletras"  id="" minlength="5" maxlength="150"  readonly value="<?php echo $xs[9];?>">
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">ESTE</span>
            <input type="text" class="form-control todoletras" id="" minlength="5" maxlength="150" readonly  value="<?php echo $xs[5];?>">
            <span class="input-group-addon">METROS</span>
            <input type="text" class="form-control todoletras"  id="" minlength="5" maxlength="150"  readonly value="<?php echo $xs[8];?>">
        </div>
        <?php }?>
        <!--
        <br>
        <form method="POST" action="?op=sex&id=<?php echo $rs[0];?>">
            <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>DATOS DE LA INSPECCIÓN</b></span>
                <input type="hidden" name="id_ins" value="<?php echo $fs[0];?>">
            </div>
            <div class="input-group input-group-sm">
                <span class="input-group-addon">NORTE</span>
                <input type="text" class="form-control solonumeros" name="nortem" minlength="2" maxlength="5" required value="<?php echo $xs[2];?>">
                <span class="input-group-addon">METROS</span>
                <span class="input-group-addon">SUR</span>
                <input type="text" class="form-control solonumeros" name="surm" minlength="2" maxlength="5" required value="<?php echo $xs[3];?>">
                <span class="input-group-addon">METROS</span>
            </div>
            <div class="input-group input-group-sm">
                <span class="input-group-addon">ESTE</span>
                <input type="text" class="form-control solonumeros" name="estem" minlength="2" maxlength="5" required value="<?php echo $xs[4];?>">
                <span class="input-group-addon">METROS</span>
                <span class="input-group-addon">OESTE</span>
                <input type="text" class="form-control solonumeros" name="oestem" minlength="2" maxlength="5" required value="<?php echo $xs[5];?>">
                <span class="input-group-addon">METROS</span>
            </div>
            <br>
            <input type="submit" value="Guardar Datos de la Inspeccion" class="btn btn-success" name="btgdi">
        </form>
        -->
        <br>
        <form method="POST" action="?op=sex&id=<?php echo $rs[0];?>">
            <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>FECHA DE ENVIO A EVALUO</b></span>
                <input type="text" class="form-control calendario" name="fecha_ava" maxlength="5" required value="<?php echo $rs[3]; ?>" readonly >
                <span class="input-group-addon"></span>
                <input type="submit" value="Guardar Fecha" class="btn btn-success form-control" name="btgfava"<?php if($rs[4] != 'INSPECCIONADO') echo 'disabled'; ?> >
            </div>
        </form>
        <br>
        <form method="POST" action="?op=sex&id=<?php echo $rs[0];?>">
            <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>FECHA DE CONTROL PREVIO</b></span>
                <input type="text" class="form-control calendario" name="fecha_con" maxlength="5" required value="<?php echo $rs[5];?>" readonly>
                <span class="input-group-addon"></span>
                <input type="submit" value="Guardar Fecha" class="btn btn-success form-control" name="btgfctp" <?php if($rs[4] != 'EN AVALUO') echo 'disabled'; ?>>
                
                <?php
                    if($rs[4] == "CONTROL PREVIO"){
                ?>
                <span class="input-group-addon"></span>
                <a target="__blank" href="pdfControl.php?ced=<?php echo $rs[1]; ?>" class="btn btn-primary form-control">VER</a>
                <?php
                    }
                ?>
            </div>
        </form>
        <br>
        <form method="POST" action="?op=sex&id=<?php echo $rs[0];?>">
            <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>FECHA PARA REGISTRO</b></span>
                <input type="text" class="form-control calendario" name="fecha_con" maxlength="5" required value="<?php echo $rs[6];?>" readonly>
                <span class="input-group-addon"></span>
                <input type="submit" value="Guardar Fecha" class="btn btn-success form-control" name="btgfprg" <?php if($rs[4] != 'CONTROL PREVIO') echo 'disabled'; ?>>
            </div>
        </form>
        <br>
        <form method="POST" action="?op=sex&id=<?php echo $rs[0];?>">
            <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>NRO DE ARCHIVO</b></span>
                <input type="text" class="form-control solonumeros" name="nro" value='<?php echo $rs[7]; ?>'>
                <span class="input-group-addon"><b>FECHA DE ENTREGA</b></span>
                <input type="text" class="form-control calendario" name="fecha_ent" maxlength="5" required value="<?php echo $rs[8];?>" readonly>
                <span class="input-group-addon"></span>
                <input type="submit" value="Guardar" class="btn btn-success form-control" onclick="" name="bt_terminar" <?php if($rs[4] != 'PARA REGISTRO') echo 'disabled'; ?>>
            </div>
            <br><br>
            <a href="pdfExpediente.php?ced=<?php echo $rs[1];?>" target="__blank" class="btn btn-danger">IMPRIMIR</a>
        </form>
</div>
<div class="col-md-1" style="margin: auto;padding: 0.5em;"></div>
<div class="panel panel-primary col-md-4" style="margin: auto;padding: 0.5em;">
    <form action="?op=sex&id=<?php echo $rs[0];?>" method="POST">
    <div class="input-group input-group-sm">
            <span class="input-group-addon">NUEVA OBSERVACION</span>
            <textarea name="obs" class="" rows="3" style="width: 100%;" maxlength="200" minlength="5" maxlength="250" required></textarea>
    </div>
    <br>
            <input type="submit" value="Guardar Observacion" class="btn btn-success" name="btgo">
        </form>
    <br>
    <div class="input-group input-group-sm">
        <span class="input-group-addon">OBSERVACIONES DEL EXPEDIENTE</span>
    </div>
    <?php 
    $sql = "SELECT * FROM observaciones WHERE nro_exp='$id' ORDER BY fecha DESC;";
    $r = $orm->consultaPersonalizada($sql);
    while($f = $r->fetch_array()){
        echo "<div class='input-group input-group-sm'>";
        echo "    <span class='input-group-addon'>".$f[3]."</span>";
        echo "    <textarea rows='6' style='width: 100%;' readonly>".$f[2]."</textarea>";
        echo "</div>";
    }
    ?>


</div>
<script>
$(document).on('ready', function(){

    $(".cedexiste").on('keyup', function(){
        ced = $(this).val();
        tab = "solicitantes";
        $.post('real_time.php', {tab: tab, tipo: "ced_sol", id: ced}, function(data){
            console.log(data);
            if(data==null){
                $("#btg").attr('disabled', true);
                $("#name").val("");
            }else{
                $("#btg").attr('disabled', false);
                $("#name").val(data.nom_sol+" "+data.ape_sol);
                $("#sector").val(data.des_com);
                $("input[name='norte']").val(data.norte);
                $("input[name='sur']").val(data.sur);
                $("input[name='este']").val(data.este);
                $("input[name='oeste']").val(data.oeste);
            }
        });
    });
    <?php
    if(isset($rs)){
    ?>
        $(".cedexiste").keyup();
    <?php
    }
    ?>
    <?php if($_SESSION['nivel'] == 'SOLICITANTE'){ ?>
        $("input[type='submit']").each(function(){
            $(this).remove();
        });
    <?php
    }
    ?>
});
</script>
