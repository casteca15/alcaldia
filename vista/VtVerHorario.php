<?php
    include_once("../modelo/Orm.php");
    $sql = "SELECT * FROM grupos";
    $r = $orm->consultaPersonalizada($sql);
    $dias = array("Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo");
    $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
?>

<h2 class="alert-success" style="margin:0;">Ver Horarios</h2>
<div class="container">
    <form class="bs-example bs-example-form" id="formulario" action="pdfHorario.php" method="GET" role="form" style="">
    <br>
        <div class="panel panel-primary col-md-5" style="margin: auto;padding: 0.5em;">
            <div class="panel-heading">
                <h3 class="panel-title">VER HORARIO POR GRUPO</h3>
            </div>
                <select name="id" class="form-control">
                    <option value="t">TODOS LOS CARGOS</option>
                    <?php 
                        while($f = $r->fetch_array()){
                            echo "<option value='".$f[0]."'>".$f[1]."</option>";
                        }
                    ?>
                </select>
                <br><br>
                <input type="submit" class="btn btn-info"  value="PDF" name="btb">

    </div>
    <div class="col-md-2" style="margin: auto;padding: 0.5em;"></div>
    <div class="panel panel-primary col-md-5" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title">MES Y AÑO</h3>
        </div>
        <select name="mes" class="form-control" required>
            <?php
                for($i = 0; $i < 12; $i++)
                    echo "<option value='".($i+1)."'>".$meses[$i]."</option>";
            ?>
        </select>
        <br><br>
        <select name="ano" class="form-control">
            <?php
                for($i = 2015; $i <= date("Y")+1; $i++)
                    echo "<option>".$i."</option>";
            ?>
        </select>
    </div>
</form>
</div>
