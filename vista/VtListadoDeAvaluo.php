<?php
    include_once("../modelo/Orm.php");
    session_start();
    $tipo =$_GET['tipo'];
    if(isset($_GET['fec'])){
        $fec = $_GET['fec'];
        $condicion = "AND E.fec_exp='$fec'";
        $tie = "APERTURADOS EL DIA: ".$fec;
    }

    if($_GET['tipo'] == 'ava'){
        $fec = $_GET['fec'];
        $sql = "select * from expedientes E, solicitantes S, sectores SE WHERE E.ced_sol=S.ced_sol AND S.id_sector=SE.id_sector AND E.est_ext='EN AVALUO' $condicion ORDER BY E.fec_exp;";
        $tit = "LISTADO DE EXPEDIENTES EN AVALUDO $tie";
    }else if($_GET['tipo'] == 'con'){
        $sql = "select * from expedientes E, solicitantes S, sectores SE WHERE E.ced_sol=S.ced_sol AND S.id_sector=SE.id_sector AND E.est_ext='CONTROL PREVIO' $condicion ORDER BY E.fec_exp;";
        $tit = "LISTADO DE EXPEDIENTES EN CONTROL PREVIO $tie";
    }else if($_GET['tipo'] == 'reg'){
        $sql = "select * from expedientes E, solicitantes S, sectores SE WHERE E.ced_sol=S.ced_sol AND S.id_sector=SE.id_sector AND E.est_ext='PARA REGISTRO' $condicion ORDER BY E.fec_exp;";
        $tit = "LISTADO DE EXPEDIENTES EN REGISTRO $tie";
    }else if($_GET['tipo'] == 'ter'){
        $sql = "select * from expedientes E, solicitantes S, sectores SE WHERE E.ced_sol=S.ced_sol AND S.id_sector=SE.id_sector AND E.est_ext='TERMINADO' $condicion ORDER BY E.fec_exp;";
        $tit = "LISTADO DE EXPEDIENTES TERMINADOS $tie";
    }

    $r = $orm->consultaPersonalizada($sql);
    $n = $r->num_rows;
?>
<h2 class="alert-success" style="margin:0;"><?php echo $tit." ($n)"; ?></h2>
<br>
<div class="container">
    <br>
    <form action="" method="get" style="max-width: 40%;width:100%;margin: auto;">
        <input type="hidden" name="op" value="lea">
        <input type="hidden" name="tipo" value="<?php echo $tipo;?>">
        <input type="text" class="form-control calendario" name="fec" placeholder="BUSCAR POR FECHAS DE APERTURA DEL EXPEDIENTE">
        <br>
        <input type="submit" class="btn btn-primary" value="Buscar">
    </form>
    <br>
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $tit; ?></h3>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>CEDULA</th>
                        <th>NOMBRE</th>
                        <th>APELLIDO</th>
                        <th>DIRECCION DEL TERRENO</th>
                        <th>FECHA DE APERTURA</th>
                        <th>FECHA DE ENTREGA</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($f = $r->fetch_assoc()){
                            echo "<tr>";
                            echo "<td>".$f['ced_sol']."</td>";
                            echo "<td>".$f['nom_sol']."</td>";
                            echo "<td>".$f['ape_sol']."</td>";
                            echo "<td>".$f['des_com']."</td>";
                            echo "<td>".$f['fec_exp']."</td>";
                            if($_GET['tipo'] == 'ava')
                                echo "<td>".$f['fec_ava']."</td>";
                            else if($_GET['tipo'] == 'con')
                                echo "<td>".$f['fec_con']."</td>";
                            else if($_GET['tipo'] == 'reg')
                                echo "<td>".$f['fec_reg']."</td>";
                            else if($_GET['tipo'] == 'ter')
                                echo "<td>".$f['fec_ent']."</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
