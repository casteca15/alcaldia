<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include ("../PDF/fpdf.php");
    include_once("../modelo/Orm.php");
	
	$cedula = $_GET['ced'];
    $myfile = fopen("ingeniero.txt", "r") or die("Unable to open file!");
    $ing = fgets($myfile);
    fclose($myfile);

    $sql = "SELECT * FROM solicitantes SO, sectores S WHERE SO.ced_sol='$cedula' AND SO.id_sector=S.id_sector;";
    $r = $orm->consultaPersonalizada($sql);
    $f = $r->fetch_array();

    $sql = "SELECT * FROM expedientes E, inspecciones I, terrenos T WHERE E.ced_sol='$cedula' AND I.nro_exp=E.nro_exp AND I.id_inspeccion=T.id_inspeccion;";
    $r = $orm->consultaPersonalizada($sql);
    $fx = $r->fetch_assoc();

	class Mipdf extends FPDF{
		function Header(){
			$this->Image("img/minis.png", 5,5,200);
		    $this->SetFont("Arial","B",12);
		    $this->Cell(30);

		    $this->setTextColor(0,0,0);
		    $this->Ln(20);
		    $this->Cell(190,10,utf8_decode("CONSTANCIA DE LA UNIDAD DE MENSURA"),0,1,'C',false);
		    $this->Ln(10);
		}

		Function Footer()
		{
			$this->SetY(-20);
			$this->Cell(20);
		}

	}
	$mipdf=new Mipdf();
	$mipdf->addPage();
		$mipdf->Multicell(190,9,utf8_decode("CTU: ____ N  N CATASTRAL:"),0,'C');

		$mipdf->Multicell(190,9,utf8_decode("EL Suscrito Ingeniero $ing, Director de Catastro del Municipio Guanare, hace constar que el Ciudadano: ".strtoupper($f[2]." ".$f[3])." Portador de la Cédula de Identidad ".$f[1]."-".$f[0].", Estado Civil Soltero, mayor de edad, de Nacionalidad ".(($f[1]=='V')?'Venezolano/a':'Extranjero/a')." y de este domicilio está tramitando: COMPRA, de un lote de terreno Municipal ubicado en: ".strtoupper($f[10]).", DE LA PARROQUIA GUANARE DEL MUNICIPIO GUANARE ESTADO PORTUGUESA.
			LINDEROS:
			NORTE: ".strtoupper($fx['norte_t'])."
			SUR: ".strtoupper($fx['sur_t']))."
			ESTE: ".strtoupper($fx['este_t'])."
			OESTE: ".strtoupper($fx['oeste_t']));

		$mipdf->Multicell(190,9,utf8_decode("La presente no concede al interesado ningún derecho contrario a los intereses Municipales y podrán ser revocados por el funcionario que lo emita por la Cámara y Comisión de Ejidos, debiendo el interesado cumplir con todos los requisitos legales de la Ordenanza de Ejidos Municipales."),0,'C');

		$mipdf->Multicell(190,9,utf8_decode("Constancia que se expide a parte interesada en Guanare a los ".date('d')." días del mes ".date('m')." del ".date('Y')),0,'C');

		$mipdf->Multicell(190,9,utf8_decode("
				Atentamente,


				ING. $ing
				DIRECTOR DE CATASTRO"),0,'C');

		$mipdf->Multicell(190,9,utf8_decode("FTC/LISBETH"),0,'C');
		
		$mipdf->Output();
?>
