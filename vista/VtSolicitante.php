<?php
    include_once("../controlador/CtSolicitante.php");
    include_once("../controlador/CtSector.php");
    include_once("../modelo/Orm.php");
    include_once("../controlador/CtSector.php");
    include_once("../modelo/Orm.php");

    $sql = "SELECT * FROM expedientes WHERE nro_exp like '".date('Y')."%';";
    $r = $orm->consultaPersonalizada($sql);
    $nro = date("Y")."-".($r->num_rows+1);
?>
<script src="js/real_time.js"></script>
<h2 class="alert-success" style="margin:0;">Modulo para registrar Solicitantes</h2>
<br>
<button class='btn btn-info abrirdialog'>Registrar Solicitante</button>
<br>
<br>

<div class="container">
<div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
    <div class="panel-heading">
        <h3 class="panel-title">Solicitantes Registrados</h3>
    </div>
    <br>
    <div class="table-responsive">
        <?php

            if(isset($_GET['ele'])){
                if($ctE->borrar($_GET['ele'])){
                    echo "<script>alert('Persona eliminada correctamente');</script>";
                    echo "<script>window.location = '?op=$op';</script>";
                }else{
                    echo "<script>alert('No se pudo eliminar');</script>";
                    echo "<script>window.location = '?op=$op';</script>";
                }
            }

            $t = $_GET['t'];
            if(isset($_GET['txt'])){
                $t = $_GET['txt'];
            }
        ?>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="6" style="text-align: center;">
                        <form action="index.php?op=rso" method="GET" id="formulario">
                            <input type="hidden" name="op" value="rso" id="opcion_buscar">
                            <input type="text" name="txt" placeholder="Buscar por cedula, nombre o apellido" style="max-width: 30%;width: 100%;border-radius:0.5em;" id="caja_buscar" class="">
                            <input type="submit" class="btn btn-success" value="Buscar" disabled="false" id="boton_buscar">
                        </form>
                    </th>
                </tr>
                <tr>
                    <th>CÉDULA</th>
                    <th>NOMBRE Y APELLIDO</th>
                    <th>TELEFONO</th>
                    <th>SEXO</th>
                    <th>COMUNIDAD</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $ctE->listar($t);
                ?>
            </tbody>
        </table>
        <br><br>
        <a href="index.php" class="btn btn-danger">Salir</a>
    </div>
</div>

<div id='mensajes_grandes' style="<?php if(isset($_GET['edi'])) echo 'display:block'; ?>">
    <a href="<?php echo '?op='.$op; ?>" id='cerrar_mensaje_grande'> X </a>
    <div class="panel panel-primary col-md-12" style="margin:auto;padding: 0.5em;">
    <div class="panel-heading">
        <h3 class="panel-title">Nuevo Solicitante</h3>
    </div>
    <?php
    if(isset($_POST['btg']) || isset($_POST['btc'])){

        $ced = $_POST['ced'];
        $nac = $_POST['naci'];
        $nom = $_POST['nom'];
        $ape = $_POST['ape'];
        $tel = $_POST['tel'];
        $tel2 = $_POST['tel2'];
        $tel3 = $_POST['tel3'];
        $sex = $_POST['sex'];
        $sec = $_POST['sector'];
        $dir = $_POST['dir'];

        $ctE->addData($ced);
        $ctE->addData($nac);
        $ctE->addData($nom);
        $ctE->addData($ape);
        $ctE->addData(strval($tel));
        $ctE->addData(strval($tel2));
        $ctE->addData(strval($tel3));
        $ctE->addData($sex);
        $ctE->addData($sec);
        $ctE->addData($dir);

        if(isset($_POST['btg'])){
            if($ctE->registrar()){
                $ne = $_POST['exp'];
                $fe = date('Y-m-d');
                $es = "INICIADO";
                $ob = $_SESSION['usuario']." REALIZO LA SIGUIENTE OBSERVACION: EL EXPEDIENTE FUE INICIADO ";
                $sql = "INSERT INTO usuarios VALUES('$ced', md5('$ced'), 'SOLICITANTE', 'BLOQUEADO');";
                $orm->insertarPersonalizado($sql);
                $sql = "INSERT INTO expedientes VALUES('$ne', '$ced', '$fe', '', '$es', '', '', '', '')";
                $orm->insertarPersonalizado($sql);
                $sql = "INSERT INTO observaciones VALUES(null, '$ne', '$ob', '$fe ".date('H:i:s')."');";
                $orm->insertarPersonalizado($sql);
                echo "<script>alert('Registrado correctamente');</script>";
                echo "<script>window.location = '?op=$op';</script>";
            }
        }else if(isset($_POST['btc'])){
            // CARGA FAMILIAR
            $cedc  = $_POST['cedc'];
            $nombs = $_POST['nomc'];
            $apels = $_POST['apec'];
            $pares = $_POST['parc'];
            for($i = 0; $i < count($nombs); $i++){
                $a[0] = 0;
                $a[1] = $ced;
                $a[2] = $cedc[$i];
                $a[3] = $nombs[$i];
                $a[4] = $apels[$i];
                $a[5] = $pares[$i];
                $orm->insertar($a, "carga_familiar");
            }
            if($ctE->cambiar() || $i > 0){

                echo "<script>alert('Informacion actualizada correctamente');</script>";
            }else{
                echo "<script>alert('No se pudo actualizar la información');</script>";
            }
            echo "<script>window.location = '?op=$op&edi=$ced';</script>";
        }
    }else if(isset($_GET['edi'])){
        $r = $ctE->buscar($_GET['edi']);
        if($r==false){
            $ENC = false;
        }else{
            $ENC = true;
            $rs = $r; 
            $r2 = $rx; 

            $sql1 = "SELECT * FROM expedientes WHERE ced_sol='".$_GET['edi']."';";
            $rx = $orm->consultaPersonalizada($sql1);
            $fx = $rx->fetch_assoc();
        }
    }else if(isset($_GET['elec'])){
        $id = $_GET['elec'];
        $ced = $_GET['ced'];
        if($orm->eliminar("id_carga", $id, "carga_familiar")){
            echo "<script>alert('Pariente eliminado correctamente');</script>";
            echo "<script>window.location = '?op=$op&edi=$ced';</script>";
        }
    }
    ?>
    <form class="bs-example bs-example-form" id="formulario" action="?op=rso" method="POST" role="form" style="">
        <br>
        <div class="input-group input-group-sm">

            <?php if($ENC){
            ?>
            <span class="input-group-addon">NUMERO DEL EXPEDIENTE</span>
            <input type="text" class="form-control solonumeros" name="exp" required value="<?php echo $fx['nro_exp'];?>" readonly>
            <span class="input-group-addon">NACIONALIDAD</span>
            <select name="naci" class="form-control" required>
                <option value="">--</option>
                <option <?php if($rs[1] == "V") echo "selected";?>>V</option>
                <option <?php if($rs[1] == "E") echo "selected";?>>E</option>
            </select>
            <span class="input-group-addon">CÉDULA</span>
            <input type="text" class="form-control solonumeros" name="ced" required value="<?php echo $rs[0]; ?>" readonly maxlength=8>
            <?php
            }else{
            ?>
            <span class="input-group-addon">NUMERO DEL EXPEDIENTE</span>
            <input type="text" class="form-control solonumeros" name="exp" required value="<?php echo $nro;?>" readonly>
            <span class="input-group-addon">NACIONALIDAD</span>
            <select name="naci" class="form-control" required>
                <option value="">--</option>
                <option>V</option>
                <option>E</option>
            </select>
            <span class="input-group-addon">CÉDULA</span>
            <input type="text" class="form-control solonumeros cedexiste" id="solicitantes"  name="ced" minlength="6" required maxlength=8>
            <b style="color: red; display: none;" id="errorcedula">Ya existe un solicitante con esta cedula</b>
            <?php
            }
            ?>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">NOMBRE</span>
            <input type="text" class="form-control sololetras" name="nom" minlength="3" required value="<?php echo $rs[2]; ?>" maxlength="20">
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">APELLIDO</span>
            <input type="text" class="form-control sololetras"  name="ape" minlength="3" required value="<?php echo $rs[3]; ?>" maxlength="25">
            <span class="input-group-addon">TELEFONO 1</span>
            <input type="text" class="form-control solonumeros"  name="tel" minlength="11" maxlength="11" required value="<?php echo $rs[4];?>" maxlength="11" placeholder="Ej: 4121234567">
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">TELEFONO 2</span>
            <input type="text" class="form-control solonumeros"  name="tel2" maxlength="11" value="<?php echo $rs[5];?>" maxlength="11" placeholder="Ej: 4121234567">

            <span class="input-group-addon">TELEFONO 3</span>
            <input type="text" class="form-control solonumeros"  name="tel3" maxlength="11" value="<?php echo $rs[6];?>" maxlength="11" placeholder="Ej: 4121234567">

            <span class="input-group-addon">SEXO</span>
            <select name="sex" class='form-control' required>
                <option value="">--</option>
                <option <?php if($rs[7] == "Masculino") echo "selected"; ?>>Masculino</option>
                <option <?php if($rs[7] == "Femenino") echo "selected"; ?>>Femenino</option>
            </select>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">DIRECCION DEL SOLICITANTE</span>
        </div>
        <div class="input-group input-group-sm">
            <textarea name="dir" style="max-width: 100%;width: 100%;resize: none;" cols="100" rows="3"><?php echo $rs[9];?></textarea>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">COMUNIDAD A LA QUE PERTENECE</span>
            <select name="sector" class='form-control' required>
                <option value="">--</option>
                <?php
                    $r = $ctD->listarOption();
                    while($f = $r->fetch_assoc()){
                        if($ENC){
                            if($rs[8] == $f['id_sector'])
                                echo "<option value='".$f['id_sector']."' selected>".$f['des_com']."</option>";
                            else
                                echo "<option value='".$f['id_sector']."'>".$f['des_com']."</option>";
                        }else{
                            echo "<option value='".$f['id_sector']."'>".$f['des_com']."</option>";
                        }
                    }
                ?>
            </select>
        </div>
        <br><br>
        <button type="button" class="btn btn-primary" name="btnuevo">Nuevo</button>
        <button type="submit" class="btn btn-success" name="btg" id="btguardarempleado">Guardar</button>
        <button type="submit" class="btn btn-info" name="btc">Editar</button>
        <a href="?op=rso&ele=<?php echo $rs[0];?>" class="btn btn-danger" name="bte">Eliminar</a>
        <a href="?op=rso" class="btn btn-danger" name="btcancelar">Cancelar</a>

</div>
        </div>
    </form>
</div>

</div>
    <script>
        $(document).on('ready', function(){

            $(".cedexiste").on('keyup', function(){
                var ced = $(this).val().trim();

                $.post('real_time.php', {tipo: 'ced_sol', id: ced, tab: 'solicitantes'}, function(data){
                    if(data == null){
                    }else{
                        window.location = "?op=rso&edi="+ced;
                    }
                });

            });

        <?php if(!$ENC){?>
            $("input, select").attr('disabled', true);
            $("button[name='btg']").attr('disabled', true);
            $("button[name='btc']").attr('disabled', true);
            $("a[name='bte']").attr('disabled', true);
        <?php }else{ ?>
            $("input, select").attr('disabled', false);
            $("button[name='btg']").attr('disabled', true);
            $("button[name='btc']").attr('disabled', false);
            $("a[name='bte']").attr('disabled', false);
        <?php } ?>

            $("button[name='btnuevo']").on('click', function(){
                $("input, select").attr('disabled', false);
               $("button[name='btg']").attr('disabled', false); 
            });

            var numero = 0;
            $("#btadd").on('click', function(){
                numero++;
                idf= 'fila_'+numero;

                n = $("#noc").val().trim();
                if(n.length < 2){
                    alert("Nombre de la carga familiar, muy corto minimo 2");
                    return;
                }

                a = $("#apc").val().trim();
                if(a.length < 2){
                    alert("Apellido de la carga familiar, muy corto minimo 2");
                    return;
                }

                p = $("#pac option:selected").val();
                if(p.length == 0){
                    alert("Seleccione parentesco, de la carga familiar");
                    return;
                }
                var cedc = $("#ced_carga").val().trim();
                $("#noc").val('');
                $("#apc").val('');
                $("#pac").val('');
                $("#ced_carga").val('');
                    
                html = "<tr id='"+idf+"'>";
                html+= "<input type='hidden' name='cedc[]' value='"+cedc+"'>";
                html+= "<input type='hidden' name='nomc[]' value='"+n+"'>";
                html+= "<input type='hidden' name='apec[]' value='"+a+"'>";
                html+= "<input type='hidden' name='parc[]' value='"+p+"'>";
                html+= "<td>"+cedc+"</td>";
                html+= "<td>"+n+"</td>";
                html+= "<td>"+a+"</td>";
                html+= "<td>"+p+"</td>";
                html+= "<td><input type='button' class='quitar btn btn-danger' value='X' title='Quitar'></td>";
                html+= "</tr>";
                $("#tablacarga").append(html);
            });

            $(document).on('click', '.quitar', function(){
                $(this).parent().parent().remove();
            });
            $("#caja_buscar").attr('disabled', false);
            $("#opcion_buscar").attr('disabled', false);
            $("#boton_buscar").attr('disabled', false);
        });
</script>

