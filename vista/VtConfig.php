<?php

    if(isset($_POST['btb'])){
        $nom = $_POST['jefe'];
        $myfile = fopen("ingeniero.txt", "w") or die("Unable to open file!");
        fwrite($myfile, $nom);
        fclose();
        echo "<script>alert('Nombre actualizado');</script>";
        echo "<script>window.location = '?op=conf_ing';</script>";
    }else{
        $myfile = fopen("ingeniero.txt", "r") or die("Unable to open file!");
        $ing = fgets($myfile);
        fclose($myfile);
    }

?>
<h2 class="alert-success" style="margin:0;">CONFIGURAR NOMBRE DEL JEFE ACTUAL</h2>
<br>
<br>
<div class="col-md-3" style="margin: auto;padding: 0.5em;"></div>
<div class="panel panel-primary col-md-6" style="margin: auto;padding: 0.5em;">
    <form action="?op=conf_ing" method="POST">
        <input type="hidden" name="op" value="inspec">
        <div class="input-group input-group-sm">
            <span class="input-group-addon">NOMBRE DEL JEFE</span>
            <input type="text" class="form-control" name="jefe" required value="<?php echo $ing; ?>">
            <span class="input-group-addon"></span>
            <input type="submit" name="btb" class="btn btn-success form-control" value="CAMBIAR">
        </div>
    </form>
</div>
