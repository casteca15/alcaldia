<?php
    include_once("../modelo/Orm.php");

    if(isset($_GET['btb'])){
        $exp = $_GET['exp'];

        $sql = "SELECT * FROM inspecciones I, ins_cor C WHERE I.nro_exp='$exp' AND I.ced_ins=C.ced;";
        $r = $orm->consultaPersonalizada($sql);
        if($r->num_rows == 0){
            echo "<script>alert('Este expediente no tiene asignado inspector, debe ser asignado primero en el modulo de seguimiento');</script>";
            echo "<script>window.location = '?op=inspec';</script>";
        }else{
            $ENC = true;
            $rs = $r->fetch_assoc();
            $sql = "SELECT * FROM terrenos WHERE id_inspeccion='".$rs['id_inspeccion']."';";
            $r = $orm->consultaPersonalizada($sql);
            $ri = $r->fetch_assoc();
        }
    }else if(isset($_POST['btg'])){
        $ide = $_POST['exp'];
        $idi = $_POST['idi'];
        $nor = $_POST['norte'];
        $norn = $_POST['norten'];

        $sur = $_POST['sur'];
        $surn = $_POST['surn'];

        $est = $_POST['este'];
        $estn = $_POST['esten'];

        $ost = $_POST['oeste'];
        $ostn = $_POST['oesten'];

        $sql = "INSERT INTO terrenos VALUES(null, '$idi', '$nor', '$sur', '$ost', '$est', '$norn', '$surn', '$estn', '$ostn');";
        if($r = $orm->insertarPersonalizado($sql)){
            $id = $r->insertId;
            $sql = "UPDATE expedientes set est_ext='INSPECCIONADO' WHERE nro_exp='$ide';";
            $orm->editarPersonalizado($sql);
            echo "<script>alert('Datos almacenados correctamente');</script>";
            echo "<script>window.location = '?op=inspec&exp=$id';</script>";
        }else{
            echo "<script>alert('No se pudo almacenar la informacion');</script>";
            echo "<script>window.location = '?op=inspec';</script>";
        }
    }
    
?>

<script src="js/real_time.js"></script>
<h2 class="alert-success" style="margin:0;">Cargar datos de la inspección</h2>
<br>
<br>
<div class="col-md-3" style="margin: auto;padding: 0.5em;"></div>
<div class="panel panel-primary col-md-6" style="margin: auto;padding: 0.5em;">
    <?php if(!$ENC){ ?>
    <form action="?op=inspec" method="GET">
        <input type="hidden" name="op" value="inspec">
        <div class="input-group input-group-sm">
            <span class="input-group-addon">NUMERO DEL EXPEDIENTE</span>
            <input type="text" class="form-control" name="exp" required value="">
            <span class="input-group-addon"></span>
            <input type="submit" name="btb" class="btn btn-success form-control" value="Buscar">
        </div>
    </form>
    <?php }if($ENC){ ?>
    <br>
    <div class="input-group input-group-sm">
        <span class="input-group-addon">DATOS DEL INSPECTOR</span>
    </div>
    <div class="input-group input-group-sm">
        <span class="input-group-addon">EXPEDIENTE</span>
        <input type="text" class="form-control solonumeros" name="" required value="<?php echo $_GET['exp'];?>" readonly>
        <span class="input-group-addon">CI</span>
        <input type="text" class="form-control solonumeros cedexiste" name="ced" required value="<?php echo $rs['ced_ins'];?>" readonly>
        <span class="input-group-addon">NOMBRE Y APELLIDO</span>
        <input type="text" class="form-control solonumeros" name="" id="name" required value="<?php echo $rs['nom_ins'].' '.$rs['ape_ins'];?>" readonly>
    </div>
    <br>
    <div class="input-group input-group-sm">
        <span class="input-group-addon">TELEFONO</span>
        <input type="text" class="form-control solonumeros" name="" id="sector" required value="<?php echo $rs['tel_ins'];?>" readonly>
        <span class="input-group-addon">FECHA DE LA INSPECCION</span>
        <input type="text" class="form-control solonumeros" name="fec" required value="<?php echo $rs['fec_ins'];?>" readonly>
    </div>
    <br>
    <form action="?op=inspec" method="POST">
        <input type="hidden" name="idi" value="<?php echo $rs['id_inspeccion'];?>">
        <input type="hidden" name="exp" value="<?php echo $_GET['exp'];?>">
        <div class="input-group input-group-sm">
            <span class="input-group-addon">DATOS INSPECCIONADOS DEL TERRENO</span>
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">NORTE</span>
            <input type="text" class="form-control todoletras" name="norte" id="" minlength="5" maxlength="150" required value="<?php echo $ri['norte_t'];?>">
            <span class="input-group-addon">METROS</span>
            <input type="text" class="form-control solonumeros" name="norten" id="" minlength="2" maxlength="4" required value="<?php echo $ri['norte'];?>">
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">SUR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <input type="text" class="form-control todoletras" name="sur" id="" minlength="5" maxlength="150" required value="<?php echo $ri['sur_t'];?>">
            <span class="input-group-addon">METROS</span>
            <input type="text" class="form-control solonumeros" name="surn" id="" minlength="2" maxlength="4" required value="<?php echo $ri['sur'];?>">
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">OESTE</span>
            <input type="text" class="form-control todoletras" name="oeste" id="" minlength="5" maxlength="150" required value="<?php echo $ri['oeste_t'];?>">
            <span class="input-group-addon">METROS</span>
            <input type="text" class="form-control solonumeros" name="oesten" id="" minlength="2" maxlength="4" required value="<?php echo $ri['oeste'];?>">
        </div>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">ESTE&nbsp;&nbsp;&nbsp;</span>
            <input type="text" class="form-control todoletras" name="este" id="" minlength="5" maxlength="150" required value="<?php echo $ri['este_t'];?>">
            <span class="input-group-addon">METROS</span>
            <input type="text" class="form-control solonumeros" name="esten" id="" minlength="2" maxlength="4" required value="<?php echo $ri['este'];?>">
        </div>
        <br>
        <?php
            if($ri){
                echo "<br><b>Este expediente ya fue inspeccionado</b>";
            }else{
        ?>
        <button class='btn btn-info' name="btg" id="btg">Guardar Datos de la inspeccion</button>
        <?php
            }
        ?>
        <?php
            }
        ?>
    </form>
</div>
<script>
$(document).on('ready', function(){
    $(".cedexiste").on('keyup', function(){
        ced = $(this).val();
        tab = "solicitantes";
        $.post('real_time.php', {tab: tab, tipo: "ced_sol", id: ced}, function(data){
            console.log(data);
            if(data==null){
                $("#btg").attr('disabled', true);
                $("#name").val("");
            }else{
                $("#btg").attr('disabled', false);
                $("#name").val(data.nom_sol+" "+data.ape_sol);
                $("#sector").val(data.des_sec);
                $("input[name='norte']").val(data.norte);
                $("input[name='sur']").val(data.sur);
                $("input[name='este']").val(data.este);
                $("input[name='oeste']").val(data.oeste);
            }
        });
    });
});
</script>
