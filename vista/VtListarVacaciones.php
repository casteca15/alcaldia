<?php
    include_once("../controlador/CtVacaciones.php");
    session_start();
    if($_SESSION['nivel'] == "NORMAL"){
        $extra = " AND ced_emp_vac='".$_SESSION['cedula']."'";
    }else{
        $extra = "";
    }
    if(isset($_GET['s'])){
        $tit = "Vacaciones Sin Procesar";
        $sql = "SELECT * FROM vacaciones as P, empleado as E, estado as ES, peaje as PE WHERE P.ced_emp_vac=E.ced_emp AND E.id_estado=ES.id_estado AND E.id_peaje=PE.id_peaje AND est_vac='SIN PROCESAR'$extra;";
    }else if(isset($_GET['a'])){
        $sql = "SELECT * FROM vacaciones as P, empleado as E, estado as ES, peaje as PE WHERE P.ced_emp_vac=E.ced_emp AND E.id_estado=ES.id_estado AND E.id_peaje=PE.id_peaje AND est_vac='APROBADO'$extra;";
        $tit = "Vacaciones Aprobadas";
    }else if(isset($_GET['r'])){
        $sql = "SELECT * FROM vacaciones as P, empleado as E, estado as ES, peaje as PE WHERE P.ced_emp_vac=E.ced_emp AND E.id_estado=ES.id_estado AND E.id_peaje=PE.id_peaje AND est_vac='RECHAZADO'$extra;";
        $tit = "Vacaciones Rechazadas";
    }
?>
<h2 class="alert-success" style="margin:0;"><?php echo $tit; ?></h2>
<br>
<a class="btn btn-default" href="?op=lvc&a">VACACIONES APROBADAS</a>
<a class="btn btn-default" href="?op=lvc&r">VACACIONES RECHAZADAS</a>
<?php if($_SESSION['nivel'] == "ADMINISTRADOR"){ ?>
<a class="btn btn-default" href="?op=lvc&s">VACACIONES SIN PROCESAR</a>
<?php } ?>
<br><br>

<div class="container">
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $tit; ?></h3>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>CÉDULA</th>
                        <th>NOMBRE Y APELLIDO</th>
                        <th>PEAJE</th>
                        <th>FECHA SOLICITUD</th>
                        <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $ctP->listarPersonal($sql);
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
