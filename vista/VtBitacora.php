<?php
    include_once("../controlador/CtBitacora.php");
    session_start();

    if($_GET['tip'] == 'g'){
        $tit = "Bitacora general de todos los usuarios";
        $sql = "select * From bitacora as B, empleado as E WHERE B.usuario=E.ced_emp;";
    }else if($_GET['tip'] == 'e' && !isset($_GET['id'])){
        $tit = "Bitacora por usuarios";
        $sql = "SELECT * FROM empleado";
    }else if($_GET['tip'] == 'e' && isset($_GET['id'])){
        $c = $_GET['id'];
        $tit = "Bitacora de $c";
        $sql = "select * From bitacora as B, empleado as E WHERE B.usuario=E.ced_emp AND E.ced_emp=$c;";
    }else{
        header("Location: ?op=bit&tip=g");
    }

if($_GET['tip'] == 'g' || isset($_GET['id'])){
?>
<h2 class="alert-success" style="margin:0;"><?php echo $tit; ?></h2>
<br>
<a class="btn btn-default" href="?op=bit&tip=g">General</a>
<a class="btn btn-default" href="?op=bit&tip=e">Por Usuarios</a>
<br>
<br>
<div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $tit; ?></h3>
    </div>
    <br>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>CEDULA - NOMBRE Y APELLIDO</th>
                    <th>ACTIVIDAD REALIZADA</th>
                    <th>FECHA</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $ctB->listar($sql);
                ?>
            </tbody>
        </table>
    </div>
</div>
<?php
}else{
?>
<h2 class="alert-success" style="margin:0;"><?php echo $tit; ?></h2>
<br>
<a class="btn btn-default" href="?op=bit&tip=g">General</a>
<a class="btn btn-default" href="?op=bit&tip=e">Por Usuarios</a>
<br>
<br>
<div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $tit; ?></h3>
    </div>
    <br>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>CEDULA</th>
                    <th>NOMBRE Y APELLIDO</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $ctB->listarEmpleado($sql);
                ?>
            </tbody>
        </table>
    </div>
</div>

<?php
}
?>
