$(document).on('ready', function(){
    var fecha = new Date();
    Y = fecha.getFullYear();

    $(".calendario").datepicker({
        changeMonth: true,
        changeYear: true
    });

    $(".fecha_nacimiento").datepicker({
        minDate: new Date("1950-01-01"),
        maxDate: new Date((Y-18)+"-01-01"),
        changeMonth: true,
        changeYear: true
    });

    $(".fecha_nacimiento_familiar").datepicker({
        minDate: new Date("1950-01-01"),
        maxDate: new Date((Y-1)+"-01-01"),
        changeMonth: true,
        changeYear: true
    });
    
    FA = new Date();
    $(".fecha_inicio_p").datepicker({
        minDate: new Date(),
        maxDate: new Date((Y+2)+"-01-01"),
        changeMonth: true,
        changeYear: true
    });

    $(".fecha_fin_p").datepicker({
        minDate: new Date(),
        maxDate: new Date((Y+2)+"-01-01"),
        changeMonth: true,
        changeYear: true
    });

    $(".solonumeros").validCampo("1234567890");
    $(".sololetras").validCampo("abcdefghijklmnñopqrstuvwxyz ");
    $(".todoletras").validCampo("abcdefghijklmnñopqrstuvwxyz._#1234567890 ");

});
