<?php
    include_once("../controlador/CtSolicitante.php");
    include_once("../controlador/CtSector.php");
    include_once("../modelo/Orm.php");

    $sql = "SELECT * FROM expedientes WHERE nro_exp like '".date('Y')."%';";
    $r = $orm->consultaPersonalizada($sql);
    $nro = date("Y")."-".($r->num_rows+1);
    if(isset($_POST['btg'])){
        $ne = $_POST['exp'];
        $es = "INICIADO";
        $fe = date('Y-m-d');
        $ce = $_POST['ced'];
        $ob = $_SESSION['usuario']." REALIZO LA SIGUIENTE OBSERVACION:\n".$_POST['obs'];

        $sql = "SELECT * FROM expedientes WHERE ced_sol='$ce';";
        $r = $orm->consultaPersonalizada($sql);
        if($r->num_rows == 1){
            echo "<script>alert('Esta persona ya tiene un expediente');</script>";
            echo "<script>window.location = '?op=PendienteRuta';</script>";
            return;
        }

        $sql = "INSERT INTO expedientes VALUES('$ne', '$ce', '$fe', '', '$es', '', '', '', '')";
        if($r = $orm->insertarPersonalizado($sql)){
            $sql = "INSERT INTO observaciones VALUES(null, '$ne', '$ob', '$fe ".date('H:i:s')."');";
            $orm->insertarPersonalizado($sql);
            echo "<script>alert('Expedientes creado exitosamente');</script>";
            echo "<script>window.location = '?op=cex&id=$ne';</script>";
        }else{
            echo "<script>alert('No se pudo crear el expediente.');</script>";
            echo "<script>window.location = '?op=cex&id=$ne';</script>";
        }
    } 
?>

<script src="js/real_time.js"></script>
<h2 class="alert-success" style="margin:0;">Crear un nuevo Expediente</h2>
<br>
<br>
<div class="col-md-3" style="margin: auto;padding: 0.5em;"></div>
<div class="panel panel-primary col-md-6" style="margin: auto;padding: 0.5em;">
    <form method="post" action="?op=cex">
        <div class="input-group input-group-sm">
            <span class="input-group-addon">NUMERO DEL EXPEDIENTE</span>
            <input type="text" class="form-control solonumeros" name="exp" required value="<?php echo $nro;?>" readonly>
            <span class="input-group-addon">ESTATUS</span>
            <input type="text" class="form-control" name="est" required value="INICIANDO" readonly>

        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">CI</span>
            <input type="text" class="form-control solonumeros cedexiste" name="ced" required value="">
            <span class="input-group-addon">NOMBRE Y APELLIDO</span>
            <input type="text" class="form-control solonumeros" name="" id="name" required value="" readonly>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">CUMUNIDAD</span>
            <input type="text" class="form-control solonumeros" name="" id="sector" required value="" readonly>
            <span class="input-group-addon">FECHA DE LA SOLICITUD</span>
            <input type="text" class="form-control solonumeros" name="fec" required value="<?php echo date('Y-m-d');?>" readonly>
        </div>
        <br>

        <div class="input-group input-group-sm">
            <span class="input-group-addon">OBSERVACION</span>
            <textarea name="obs" class="" rows="5" style="width: 100%;" maxlength="200"></textarea>
        </div>
        <br><br>
        <button class='btn btn-info abrirdialog' name="btg" id="btg" disabled>Crear Expediente</button>
    </form>
</div>
<script>
$(document).on('ready', function(){
    $(".cedexiste").on('keyup', function(){
        ced = $(this).val();
        tab = "solicitantes";
        $.post('real_time.php', {tab: tab, tipo: "ced_sol", id: ced}, function(data){
            console.log(data);
            if(data==null){
                $("#btg").attr('disabled', true);
                $("#name").val("");
            }else{
                $("#btg").attr('disabled', false);
                $("#name").val(data.nom_sol+" "+data.ape_sol);
                $("#sector").val(data.des_com);
                $("input[name='norte']").val(data.norte);
                $("input[name='sur']").val(data.sur);
                $("input[name='este']").val(data.este);
                $("input[name='oeste']").val(data.oeste);
            }
        });
    });
});
</script>
