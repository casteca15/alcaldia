<?php
    include('../PDF/fpdf.php');
    include("../modelo/Orm.php");

    $titulo = "SOLICITUD DE PERMISO";

    $id = $_GET['id'];

    $sql = "SELECT * FROM permiso as P, empleado as E, estado as ES, cargo as C, peaje as PE, departamento as DE WHERE P.ced_emp_per=E.ced_emp AND E.id_estado=ES.id_estado AND P.id_permiso=$id AND E.id_peaje=PE.id_peaje AND E.id_cargo=C.id_cargo AND E.id_dep=DE.id_dep";

    $r = $orm->consultaPersonalizada($sql);
    $f = $r->fetch_assoc();

    $sql2 = "SELECT * FROM empleado where ced_emp=".$f['ced_sup_per'].";";
    $r2 = $orm->consultaPersonalizada($sql2);
    $f2 = $r2->fetch_assoc();


    class PDF extends FPDF{

        function Header(){
            $this->SetFont('Arial', 'B', 16);
            $this->Image('img/minis.png', 5, 5, 270, 20); 
        }

        function setTitulo($title){
            $this->titulo = $title;
        }
    }

    $pdf = new PDF('L', 'mm', 'LETTER');
    $pdf->AddPage();
    $pdf->Ln(15);
    $pdf->Image('img/fon.png', 5, 30, 40, 25); 
    $pdf->SetFont('Arial', 'B', 15);
    $pdf->Cell(260,10, "Republica Bolivariana de Venezuela Ministerio Poder Popular",0, 0,'C');
    $pdf->Ln();
    $pdf->Cell(260,10, "para el Transporte Terrestre",0, 0,'C');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetTextColor(220,50,50);
    $pdf->Cell(260,10, $titulo,0, 0,'C');
    $pdf->SetDrawColor(0,80,180);
    $pdf->SetTextColor(000,000,000);
    $pdf->Ln();
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetFillColor(230,230,0);
    $pdf->Cell(30,8, "NUMERO",1, 0,'C');
    $pdf->Cell(25,8, "FECHA",1, 0,'C');
    $pdf->Cell(30,8, "CEDULA",1, 0,'C');
    $pdf->Cell(100,8, "NOMBRE Y APELLIDO",1, 0,'C');
    $pdf->Cell(75,8, "DEPARTAMENTO",1, 0,'C');
    $pdf->ln();
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30,8, $f['num_per'],1, 0,'C');
    $pdf->Cell(25,8, $f['fec_per'],1, 0,'C');
    $pdf->Cell(30,8, $f['ced_emp'],1, 0,'C');
    $pdf->Cell(100,8,$f['nom_emp']." ".$f['ape_emp'],1, 0,'C');
    $pdf->Cell(75,8, $f2['ced_emp'],1, 0,'C');
    $pdf->ln();
    $pdf->ln();
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetFillColor(230,230,0);
    $pdf->Cell(55,8, "CARGO",1, 0,'C');
    $pdf->Cell(55,8, "ESTADO",1, 0,'C');
    $pdf->Cell(50,8, "PEAJE",1, 0,'C');
    $pdf->Cell(100,8, "SUPERVISOR INMEDIATO",1, 0,'C');
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln();
    $pdf->Cell(55,8, $f['cargo'],1, 0,'C');
    $pdf->Cell(55,8, $f['estado'],1, 0,'C');
    $pdf->Cell(50,8, $f['peaje'],1, 0,'C');
    $pdf->Cell(100,8,$f2['ced_emp']." ".$f2['nom_emp']." ".$f2['ape_emp'],1, 0,'C');
    $pdf->ln();
    $pdf->ln();
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetFillColor(230,230,0);
    $pdf->Cell(260,8, "PERIODO DE AUSENCIA",1, 0,'C');
    $pdf->ln();
    $pdf->Cell(65,8, "DESDE",1, 0,'C');
    $pdf->Cell(65,8, "HASTA",1, 0,'C');
    $pdf->Cell(65,8, "DURACION",1, 0,'C');
    $pdf->Cell(65,8, "REMUNERADO",1, 0,'C');
    $pdf->SetFont('Arial', '', 10);
    $pdf->ln();
    $pdf->Cell(65,8, $f['fec_ini_per'],1, 0,'C');
    $pdf->Cell(65,8, $f['fec_fin_per'],1, 0,'C');
    $pdf->Cell(65,8, $f['dur_per']." dias",1, 0,'C');
    $pdf->Cell(65,8, $f['con_per'],1, 0,'C');
    $pdf->SetFont('Arial', '', 10);
    $pdf->ln();
    $pdf->ln();
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetFillColor(230,230,0);
    $pdf->Cell(130,8, "MOTIVO DEL PERMISO",1, 0,'C');
    $pdf->Cell(130,8, "ESTATUS DEL PERMISO",1, 0,'C');
    $pdf->SetFont('Arial', '', 10);
    $pdf->ln();
    $pdf->Cell(130,8, $f['mot_per'],1, 0,'C');
    $pdf->Cell(130,8, $f['est_per'],1, 0,'C');
    $pdf->ln();
    $pdf->ln();
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetFillColor(230,230,0);
    $pdf->Cell(65,8, "FIRMA DEL TRABAJADOR",1, 0,'C');
    $pdf->Cell(65,8, "FIRMA DEL SUPERVISOR",1, 0,'C');
    $pdf->Cell(50,8, "ANEXOS",1, 0,'C');
    $pdf->Cell(80,8, "APROBADO POR",1, 0,'C');
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln();
    $pdf->Cell(65,8, "",1, 0,'C');
    $pdf->Cell(65,8, "",1, 0,'C');
    $pdf->Cell(50,8, "SI ( )   NO ( )",1, 0,'C');
    $pdf->Cell(80,8, "",1, 0,'C');

    $pdf->Output();
?>
