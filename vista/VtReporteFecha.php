<?php
    include_once("../controlador/CtVacaciones.php");
    session_start();
?>
<script src="js/real_time.js"></script>
<h2 class="alert-success" style="margin:0;">Reporte Por Fechas</h2>
<br>
<div class="col-md-4" ></div>
<div class="col-md-4" style="margin: auto;padding: 0.5em;">
    <form target="__blank" action="pdfGeneralFecha.php" method="get">
        <div class="input-group input-group-sm">
            <span class="input-group-addon">TIPO DE REPORTE</span>
            <select class="form-control" name="tipo" id="tipo" required>
                <option>GENERAL</option>
                <option>INDIVIDUAL</option>
            </select>
        </div>
        <br>
        <div class="input-group input-group-sm" style="display:none" id="panel">
            <span class="input-group-addon">CEDULA DEL EMPLEADO</span>
            <input type="text" class="form-control" name="ced" id="ced">
            <span class="input-group-addon" id="nombre"></span>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">SELECCIONE EL TIPO SOLICITUD</span>
            <select class="form-control" name="idp" required>
                <option value="">---</option>
                <option>VACACIONES</option>
                <option>PERMISOS</option>
            </select>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">FECHA DE INICIO</span>
            <input type="text" class="form-control calendario" name="fecc1" id="fini" required>
            <span class="input-group-addon">FECHA DE FINAL</span>
            <input type="text" class="form-control calendario" name="fecc2" id="ffin" required>

        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon"> </span>
            <input type="submit" class="form-control btn btn-info" value="Imprimir" id="bti">
            <span class="input-group-addon"> </span>
        </div>
        <br>
        <b>Nota: Las busquedas se haran por la fecha en que inicia un permiso o unas vacaiones</b>
    </form>
</div>
<br>
<br>
<br>
<script>
    $(document).on('ready', function(){
        $("#ced").on('keyup', function(){
            ced = $(this).val();
            $.post('real_time.php', {tipo: "ced_emp", id: ced}, function(data){
                if(data!=null){
                    $("#nombre").text(data.nom_emp+" "+data.ape_emp);
                    $("#bti").attr('disabled', false);
                }else{
                    $("#nombre").text("Empleado no encontrado");
                    $("#bti").attr('disabled', true);
                }
            });
        });

        $("#tipo").on('change', function(){
            var v = $("#tipo option:selected").val();
            if(v == "GENERAL"){
                $("#bti").attr('disabled', false);
                $("#panel").hide();
            }else{
                $("#bti").attr('disabled', true);
                $("#panel").show();
            }
        });
    });
</script>













