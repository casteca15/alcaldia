<script src="js/real_time.js"></script>
<?php
    include_once("../controlador/CtSector.php");
    include_once("../controlador/Orm.php");
?>
<h2 class="alert-success" style="margin:0;">Modulo Sectores</h2>
<br>
<div class="container">
    <button class='btn btn-info abrirdialog'>Nuevo Sector</button>
    <br>
    <br>
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title">Sectores Registradas</h3>
        </div>
        <br>
        <div class="table-responsive">
            <?php
                if(isset($_GET['ele'])){
                    $ctD->borrar($_GET['ele']);
                }
                $t = $_GET['t'];
                if(isset($_GET['txt'])){
                    $t = $_GET['txt'];
                }
            ?>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th colspan="6" style="text-align: center;">
                        <form action="index.php?op=rse" method="GET" id="formulario">
                            <input type="hidden" name="op" value="rse" id="opcion_buscar">
                            <input type="text" name="txt" placeholder="Buscar por nombre o sector" style="max-width: 30%;width: 100%;border-radius:0.5em;" id="caja_buscar" class="">
                            <input type="submit" class="btn btn-success" value="Buscar" disabled="false" id="boton_buscar">
                        </form>
                    </th>
                </tr>
                    <tr>
                        <th>SECTOR</th>
                        <th>COMUNIDAD</th>
                        <th>COORDINADOR</th>
                        <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $ctD->listarBuscar($t);
                    ?>
                </tbody>
            </table>
        <br><br>
        <a href="index.php" class="btn btn-danger">Salir</a>
    </div>

    </div>
    <div class="col-md-1" style="">
    </div>
    <div id='mensajes_grandes' style="<?php if(isset($_GET['edi'])) echo 'display:block'; ?>">
        <a href="<?php echo '?op='.$op; ?>" id='cerrar_mensaje_grande'> X </a>
    <div class="panel panel-primary col-md-12" style="margin:auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title">Nueva Sector</h3>
        </div>
        <?php

        if(isset($_POST['btg']) || isset($_POST['btc'])){
            $id = $_POST['idp'];
            $sec = $_POST['sec'];
            $com = $_POST['com'];
            $ced = $_POST['ced'];
            $nom = $_POST['nom'];
            $ape = $_POST['ape'];
            $tel = $_POST['tel'];
            $tel2 = $_POST['tel2'];
            $tel3 = $_POST['tel3'];

            if(isset($_POST['btc']))
                $ctD->addData($id);
            else
                $ctD->addData(0);
            $ctD->addData($sec);
            $ctD->addData($com);
            $ctD->addData($ced);
            $ctD->addData($nom);
            $ctD->addData($ape);
            $ctD->addData($tel);
            $ctD->addData($tel2);
            $ctD->addData($tel3);

            if(isset($_POST['btg'])){
                $ctD->registrar();
                echo "<script>window.location = '?op=$op';</script>";
            }else if(isset($_POST['btc'])){
                $ctD->cambiar();
                echo "<script>window.location = '?op=$op';</script>";
            }
        }else if(isset($_GET['edi'])){
            $r = $ctD->buscar($_GET['edi']);
            if($r==false){
                $ENC = false;
            }else{
                $ENC = true;
               $rs = $r; 
            }
            
        }
        ?>
        <form id="formulario" class="bs-example bs-example-form" action="?op=rse" method="POST" role="form" style="">
            <br>
            <div class="input-group input-group-sm">
                <?php if($ENC){
                ?>
                <input type="hidden" class="form-control" name="idp" required value="<?php echo $rs[0]; ?>" readonly>
                <?php
                }
                ?>
            </div>
            <br>
            <div class="input-group input-group-sm">
                <span class="input-group-addon">CODIGO DEL SECTOR</span>
                <input type="text" class="form-control solonumeros" title="" minlength="1" name="sec" required value="<?php echo $rs[1]; ?>" maxlength="3" >
                <span class="input-group-addon">NOMBRE DE LA COMUNIDAD</span>
                <input type="text" class="form-control" title="" minlength="5" name="com" required value="<?php echo $rs[2]; ?>" maxlength="150" >
            </div>
            <br>
            <div class="input-group input-group-sm">
                <span class="input-group-addon">DATOS DEL COORDINADOR</span>
            </div>
            <div class="input-group input-group-sm">
                <span class="input-group-addon">CÉDULA</span>
                <?php if($ENC){
                ?>
                <input type="text" class="form-control solonumeros" name="ced" required value="<?php echo $rs[3]; ?>"  maxlength=8>
                <?php
                }else{
                ?>
                <input type="text" class="form-control solonumeros cedexiste" id="ins_cor"  name="ced" minlength="6" required maxlength=8>
                <b style="color: red; display: none;" id="errorcedula">Ya existe un coordinador o inspector con esta cedula</b>
                <?php
                }
                ?>
                <span class="input-group-addon">NOMBRE</span>
                <input type="text" class="form-control sololetras" name="nom" minlength="2" required value="<?php echo $rs[4]; ?>" maxlength="20">
                <span class="input-group-addon">APELLIDO</span>
                <input type="text" class="form-control sololetras"  name="ape" minlength="2" required value="<?php echo $rs[5]; ?>" maxlength="25">
            </div>
            <br>
            <div class="input-group input-group-sm">
                <span class="input-group-addon">TELEFONO</span>
                <input type="text" class="form-control"  name="tel"  required value="<?php echo $rs[6];?>" maxlength="11" placeholder="Ej: 4121234567">
                <span class="input-group-addon">TELEFONO 2</span>
                <input type="text" class="form-control"  name="tel2" value="<?php echo $rs[7];?>" maxlength="11" placeholder="Ej: 4121234567">
                <span class="input-group-addon">TELEFONO 3</span>
                <input type="text" class="form-control"  name="tel3" value="<?php echo $rs[8];?>" maxlength="11" placeholder="Ej: 4121234567">
            </div>
            <br>
        <br><br>
        <button type="button" class="btn btn-primary" name="btnuevo">Nuevo</button>
        <button type="submit" class="btn btn-success" name="btg" id="btguardarempleado">Guardar</button>
        <button type="submit" class="btn btn-info" name="btc">Editar</button>
        <a href="?op=rse&ele=<?php echo $rs[0];?>" class="btn btn-danger" name="bte">Eliminar</a>
        <a href="?op=rse&t" class="btn btn-danger" name="btcancelar">Cancelar</a>

            </div>
        </form>
    </div>
</div>
</div>
<script>
    $(document).on('ready', function(){

        <?php if(!$ENC){?>
            $("input, select").attr('disabled', true);
            $("button[name='btg']").attr('disabled', true);
            $("button[name='btc']").attr('disabled', true);
            $("a[name='bte']").attr('disabled', true);
        <?php }else{ ?>
            $("input, select").attr('disabled', false);
            $("button[name='btg']").attr('disabled', true);
            $("button[name='btc']").attr('disabled', false);
            $("a[name='bte']").attr('disabled', false);
        <?php } ?>

            $("button[name='btnuevo']").on('click', function(){
                $("input, select").attr('disabled', false);
               $("button[name='btg']").attr('disabled', false); 
            });
            $("#caja_buscar").attr('disabled', false);
            $("#opcion_buscar").attr('disabled', false);
            $("#boton_buscar").attr('disabled', false);

    });

</script>
