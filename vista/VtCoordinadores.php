<?php
    include_once("../controlador/CtCoordinador.php");
    include_once("../controlador/CtDepartamento.php");
    include_once("../modelo/Orm.php");
?>
<script src="js/real_time.js"></script>
<h2 class="alert-success" style="margin:0;">Modulo de inspectores</h2>
<br>
<button class='btn btn-info abrirdialog'>Registrar Inspectores</button>
<br>
<br>

<div class="container">
<div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
    <div class="panel-heading">
        <h3 class="panel-title">Inspectores Registrados</h3>
    </div>
    <br>
    <div class="table-responsive">
        <?php
            if(isset($_GET['ele'])){
                if($ctE->borrar($_GET['ele'])){
                    echo "<script>alert('Persona eliminada correctamente');</script>";
                    echo "<script>window.location = '?op=$op';</script>";
                }else{
                    echo "<script>alert('No se pudo eliminar, estos datos estan siendo utilizados');</script>";
                    echo "<script>window.location = '?op=$op';</script>";
                }
            }
            $t = $_GET['t'];
            if(isset($_GET['txt'])){
                $t = $_GET['txt'];
            }
        ?>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="6" style="text-align: center;">
                        <form action="index.php?op=rco" method="GET" id="formulario">
                            <input type="hidden" name="op" value="rco" id="opcion_buscar">
                            <input type="text" name="txt" placeholder="Buscar por cedula, nombre o apellido" style="max-width: 30%;width: 100%;border-radius:0.5em;" id="caja_buscar" class="">
                            <input type="submit" class="btn btn-success" value="Buscar" disabled="false" id="boton_buscar">
                        </form>
                    </th>
                </tr>
                <tr>
                    <th>CÉDULA</th>
                    <th>NOMBRE</th>
                    <th>APELLIDO</th>
                    <th>TELEFONO</th>
                    <th>SEXO</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $ctE->listar($t);
                ?>
            </tbody>
        </table>
    <br><br>
    <a href="index.php" class="btn btn-danger">Salir</a>
    </div>

</div>

<div id='mensajes_grandes' style="<?php if(isset($_GET['edi'])) echo 'display:block'; ?>">
    <a href="<?php echo '?op='.$op; ?>" id='cerrar_mensaje_grande'> X </a>
    <div class="panel panel-primary col-md-12" style="margin:auto;padding: 0.5em;">
    <div class="panel-heading">
        <h3 class="panel-title">Nuevo Inspector</h3>
    </div>
    <?php
    if(isset($_POST['btg']) || isset($_POST['btc'])){

        $ced = $_POST['ced'];
        $nom = $_POST['nom'];
        $ape = $_POST['ape'];
        $tel = $_POST['tel'];
        $tel2 = $_POST['tel2'];
        $tel3 = $_POST['tel3'];
        $sex = $_POST['sex'];

        $ctE->addData($ced);
        $ctE->addData($nom);
        $ctE->addData($ape);
        $ctE->addData(strval($tel));
        $ctE->addData(strval($tel2));
        $ctE->addData(strval($tel3));
        $ctE->addData($sex);

        if(isset($_POST['btg'])){
            if($ctE->registrar()){
                $ctE->resetearData();
                echo "<script>alert('Registrado correctamente');</script>";
                echo "<script>window.location = '?op=$op';</script>";
            }
        }else if(isset($_POST['btc'])){
            // CARGA FAMILIAR
            $nombs = $_POST['nomc'];
            $apels = $_POST['apec'];
            $fechs = $_POST['fecc'];
            $pares = $_POST['parc'];
            $i = 0;
            for($i = 0; $i < count($nombs); $i++){
                $a[0] = 0;
                $a[1] = $ced;
                $a[2] = $nombs[$i];
                $a[3] = $apels[$i];
                $a[4] = $fechs[$i];
                $a[5] = $pares[$i];
                $orm->insertar($a, "carga_familiar");
            }
            $ctE->cambiarUsuario();
            if($ctE->cambiar() || $i != 0){
                echo "<script>alert('Informacion actualizada correctamente');</script>";
            }else{
                echo "<script>alert('No se pudo actualizar la información');</script>";
            }
            $ctE->resetearData();
            $ctE->addData($usu);
            $ctE->addData(md5($pas));
            $ctE->addData($acc);
            $ctE->addData($ced);
            $ctE->cambiarUsuario();
            echo "<script>window.location = '?op=$op&edi=$ced';</script>";
        }
    }else if(isset($_GET['edi'])){
        $r = $ctE->buscar($_GET['edi']);
        if($r==false){
            $ENC = false;
        }else{
            $ENC = true;
            $rs = $r; 
            $r2 = $rx; 
        }
    }
    ?>
    <form class="bs-example bs-example-form" id="formulario" action="?op=rco" method="POST" role="form" style="">
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">CÉDULA</span>
            <?php if($ENC){
            ?>
            <input type="text" class="form-control solonumeros" name="ced" required value="<?php echo $rs[0]; ?>" readonly maxlength=8>
            <?php
            }else{
            ?>
            <input type="text" class="form-control solonumeros cedexiste" id="ins_cor"  name="ced" minlength="6" required maxlength=8>
            <b style="color: red; display: none;" id="errorcedula">Ya existe un coordinador o inspector con esta cedula</b>
            <?php
            }
            ?>
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">NOMBRE</span>
            <input type="text" class="form-control sololetras" name="nom" minlength="3" required value="<?php echo $rs[1]; ?>" maxlength="20">
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">APELLIDO</span>
            <input type="text" class="form-control sololetras"  name="ape" minlength="3" required value="<?php echo $rs[2]; ?>" maxlength="25">
            <span class="input-group-addon">TELEFONO</span>
            <input type="text" class="form-control solonumeros"  name="tel" minlength="10" maxlength="10" required value="<?php echo $rs[3];?>" maxlength="11" placeholder="Ej: 4121234567">
        </div>
        <br>
        <div class="input-group input-group-sm">
            <span class="input-group-addon">TELEFONO 2</span>
            <input type="text" class="form-control solonumeros"  name="tel2" value="<?php echo $rs[4];?>" maxlength="11" placeholder="Ej: 4121234567">
            <span class="input-group-addon">TELEFONO 3</span>
            <input type="text" class="form-control solonumeros"  name="tel3" value="<?php echo $rs[5];?>" maxlength="11" placeholder="Ej: 4121234567">
            <span class="input-group-addon">SEXO</span>
            <select name="sex" class='form-control' required>
                <option value="">--</option>
                <option <?php if($rs[6] == "Masculino") echo "selected"; ?>>Masculino</option>
                <option <?php if($rs[6] == "Femenino") echo "selected"; ?>>Femenino</option>
            </select>
        </div>
        <br><br>
        <button type="button" class="btn btn-primary" name="btnuevo">Nuevo</button>
        <button type="submit" class="btn btn-success" name="btg" id="btguardarempleado">Guardar</button>
        <button type="submit" class="btn btn-info" name="btc">Editar</button>
        <a href="?op=rco&ele=<?php echo $rs[0];?>" class="btn btn-danger" name="bte">Eliminar</a>
        <a href="?op=rco&t" class="btn btn-danger" name="btcancelar">Cancelar</a>

</div>
        </div>
    </form>
</div>

</div>
<script>
    $(document).on('ready', function(){

        $(".cedexiste").on('keyup', function(){
            var ced = $(this).val().trim();
            console.log(ced);
            $.post('real_time.php', {tipo: 'ced_ins', id: ced}, function(data){
                if(data == null){
                }else{
                    window.location = "?op=rco&edi="+ced;
                }
            });

        });

    <?php if(!$ENC){?>
        $("input, select").attr('disabled', true);
        $("button[name='btg']").attr('disabled', true);
        $("button[name='btc']").attr('disabled', true);
        $("a[name='bte']").attr('disabled', true);
    <?php }else{ ?>
        $("input, select").attr('disabled', false);
        $("button[name='btg']").attr('disabled', true);
        $("button[name='btc']").attr('disabled', false);
        $("a[name='bte']").attr('disabled', false);
    <?php } ?>

        $("button[name='btnuevo']").on('click', function(){
            $("input, select").attr('disabled', false);
           $("button[name='btg']").attr('disabled', false); 
        });
            $("#caja_buscar").attr('disabled', false);
            $("#opcion_buscar").attr('disabled', false);
            $("#boton_buscar").attr('disabled', false);

    });
</script>
