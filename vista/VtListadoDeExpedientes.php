<?php
    include_once("../modelo/Orm.php");
    session_start();
    
    if(isset($_GET['sec'])){
        $t = $_GET['sec'];
        if($t == "t"){
            $sql = "select * from expedientes E, solicitantes S, sectores SE WHERE E.ced_sol=S.ced_sol AND S.id_sector=SE.id_sector;";
            $tit = "LISTADO TOTAL DE EXPEDIENTES";
        }else{
            $sql = "select * from expedientes E, solicitantes S, sectores SE WHERE E.ced_sol=S.ced_sol AND S.id_sector=SE.id_sector AND SE.id_sector='$t';";
            $tit = "LISTADO TOTAL DE EXPEDIENTES POR SECTOR";
        }
    }else{
        $sql = "select * from expedientes E, solicitantes S, sectores SE WHERE E.ced_sol=S.ced_sol AND S.id_sector=SE.id_sector;";
            $tit = "LISTADO TOTAL DE EXPEDIENTES";
    }

    $r = $orm->consultaPersonalizada($sql);
?>
<h2 class="alert-success" style="margin:0;"><?php echo $tit; ?></h2>
<br>
<div class="container">
    <form action="" method="GET">
        <div class="input-group input-group-sm">
            <input type="hidden" name="op" value="lex">
            <span class="input-group-addon">SELECCIONE EXPEDIENTES POR SECTOR </span>
            <select name="sec" class="form-control">
                <option value="t">TODOS</option>
                <?php 
                    $sql1 = "SELECT * FROM sectores;";
                    $r1 = $orm->consultaPersonalizada($sql1);
                    while($f1 = $r1->fetch_array()){
                        if($t == $f1[0])
                            echo "<option value='".$f1[0]."' selected>".$f1[2]."</option>";
                        else
                            echo "<option value='".$f1[0]."'>".$f1[2]."</option>";
                    }
                ?>
            </select>
            <span class="input-group-addon"></span>
            <input type="submit" class='btn btn-success form-control' value='VER'>
        </div>
    </form>
    <br>
    <div class="panel panel-primary col-md-12" style="margin: auto;padding: 0.5em;">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $tit; ?></h3>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>SOLICITADO POR</th>
                        <th>SECTOR DEL TERRENO</th>
                        <th>FECHA APERTURA</th>
                        <th>ESTATUS</th>
                        <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($f = $r->fetch_assoc()){
                            echo "<tr>";
                            echo "<td>".$f['ced_sol']." - ".$f['nom_sol']." ".$f['ape_sol']."</td>";
                            echo "<td>".$f['des_com']."</td>";
                            echo "<td>".$f['fec_exp']."</td>";
                            echo "<td>".$f['est_ext']."</td>";
                            echo "<td><a href='?op=sex&id=".$f['nro_exp']."' class='btn btn-success'>VER</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='pdfExpediente.php?ced=".$f['ced_sol']."' target='__blank' class='btn btn-danger'>IMPRIMIR</a></td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
