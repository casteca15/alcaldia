<?php
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    include("modelo/Orm.php");

    if(isset($_POST['btc'])){
        $pre = $_POST['preg']; 
        $res = $_POST['resp'];
        $usu = $_POST['usuario'];

        $sql = "SELECT * FROM preguntas WHERE usuario='$usu' AND id_pregunta=$pre AND respuesta='$res'";
        $r = $orm->consultaPersonalizada($sql);

        if($r->num_rows == 1){
            $CAMBIO = true;
        ?>
        <?php
        }else{
            echo "<script>alert('RESPUESTA INCORRECTA!');</script>";
            echo "<script>window.location = 'recuperar.php';</script>";
        }

    }else if(isset($_POST['btlisto'])){
        $pas = $_POST['npas'];
        $cpas = $_POST['cpas'];

        if($pas != $cpas){
            echo "<script>alert('ERROR:las contraseñas no coinciden');</script>";
            echo "<script>window.location = 'index.php';</script>";
        }else{
            $sql = "UPDATE usuario set password=md5('$pas'), estado='activo', fecha_cambio='".date('Y-m-d')."' WHERE usuario='".$_POST['usuario']."';";
            if($orm->editarPersonalizado($sql)){
                echo "<script>alert('Contraseña actualizada correctamente');</script>";
                echo "<script>window.location = 'index.php';</script>";
            }else{
                echo "<script>alert('No se pudo actualizar la contraseña, intente con otra');</script>";
                echo "<script>window.location = 'recuperar.php';</script>";
            }
        }
    }

?>
<!DOCTYPE HTML>
<html lang="ES">
    <head>
        <title>Recuperar Contraseña</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="vista/css/bootstrap.min.css">
        <link rel="stylesheet" href="vista/css/estilos.css">
        <link rel="stylesheet" href="vista/css/estilos_slide.css">
        <link rel="stylesheet" href="vista/css/index.css">
        <script src="vista/js/jquery.js"></script>
		<script src="vista/js/jquery.js"></script>
		<script src="vista/js/facturar.js"></script>
		<script src="vista/js/bootstrap.min.js"></script>
        <script src="vista/js/require.js" data-main="vista/js/validaciones.js"></script>
    </head>

    <body>
        <div id="contenedor">
            <a href="index.php"><img src="vista/img/minis.png"></a>
            <div id="titulo">
                <b style="font-size: 20px;vertical-align:top;color: red;text-shadow: 1px 1px #000">SISTEMA DE NÓMINA DEL PEAJE LA LUCÍA </b>
        </div>
        <div id="central">
            <?php
                if($CAMBIO){
            ?>
                <br><br>
                <div class="panel panel-primary col-md-6" style="margin:auto;padding: 0.5em;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Nueva Contraseña</h3>
                    </div>
                    <form class="bs-example bs-example-form" id="formulario" action="?op=rem" method="POST" role="form" style="">
                        <input type="hidden" name="usuario" value="<?php echo $usu; ?>">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">NUEVA CONTRASEÑA</span>
                            <input type="password" class="form-control" title="" minlength="4" maxlength="100" name="npas" required maxlength="50" >
                        </div>
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">CONFIRMAR CONTRASEÑA</span>
                            <input type="password" class="form-control" title="" minlength="4" maxlength="100" name="cpas" required maxlength="50" >
                        </div>
                        <br>
                        <input type="submit" class="btn btn-info"  value="ENVIAR" name="btlisto">
                    </form>
                </div>

            <?php
                }else{
            ?>
            <div id="central" class="row row-centered">
                <br><br>
                <div class="panel panel-primary col-md-6" style="margin:auto;padding: 0.5em;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recuperar Contraseña</h3>
                    </div>
                    <br><br>
                    <?php
                        if(isset($_POST['btb'])){
                            $ced = $_POST['ced'];
                            $r = $orm->consultaCondicion("ced_emp", $ced, "empleado");
                            if($ced == "webmaster" || $r->num_rows == 1){
                                $f = $r->fetch_array();
                                $ENC = true;
                                $r2 = $orm->consultaCondicion("usuario", $ced, "preguntas");
                                if($ced=="webmaster"){
                                    $nombre = "WEBMASTER DEL SISTEMA";
                                }else{
                                    $nombre = $f[1]." ".$f[2];
                                }

                                if($r2->num_rows == 0){
                                    echo "<script>alert('Lo sentimos este usuario, no tiene preguntas de seguridad.');</script>";
                                    echo "<script>window.location = 'index.php';</script>";

                                }
                            }else{
                                echo "<script>alert('Usuario no encontrado');</script>";
                                echo "<script>window.location = 'index.php';</script>";
                            }
                        }
                        
                        if(!$ENC){
                    ?>
                    <form class="bs-example bs-example-form" id="formulario" action="?op=rem" method="POST" role="form" style="">
                            <input type="text" style='text-transform: none;' class="form-control" name="ced" placeholder="INTRODUZCA EL USUARIO" required>
                            <br>
                            <input type="submit" class="btn btn-info"  value="BUSCAR USUARIO" name="btb">
                    </form>
                    <?php
                    }else{
                    ?>
                    <form class="bs-example bs-example-form" id="formulario" action="?op=rem" method="POST" role="form" style="">
                        <input type="hidden" name="usuario" value="<?php echo $ced; ?>">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon"><?php echo $nombre; ?></span>
                        </div>
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">PREGUNTAS DE SEGURIDAD</span>
                            <select name="preg" class="form-control">
                                <?php
                                    while($f2 = $r2->fetch_array()){
                                        echo "<option value='".$f2[0]."'>".$f2[2]."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">RESPUESTA</span>
                            <input type="password" style='text-transform: none;' class="form-control" name="resp" maxlength="50" required>
                        </div>
                        <br>
                        <input type="submit" class="btn btn-info"  value="ENVIAR" name="btc">
                    </form>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
        <div id="pie">
            <b>Todos los derechos reservados.
        </div>

    </body>

</html>
