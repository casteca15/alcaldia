<?php
    include('PDF/fpdf.php');
    include("php/modelo/Consultas.php");

    class PDF extends FPDF{

        function Header(){
            $this->SetFont('Arial', 'B', 16);
            $this->Image('img/logo.png', 5, 5, 270, 30); 
        }

        function setTitulo($title){
            $this->titulo = $title;
        }
    }

    $est = $_GET['est'];
    if($est=='INSCRITO'){
        $titulo = "LISTADO DE ALUMNOS - INSCRITOS";
        $r = $consulta->consultaCondicion("estatus_al", $est, "alumnos");
    }else if($est=="PRE-INSCRITO"){
        $titulo = "LISTADO DE ALUMNOS - PRE-INSCRITOS";
        $r = $consulta->consultaCondicion("estatus_al", $est, "alumnos");
    }else if($est=="INACTIVO"){
        $titulo = "LISTADO DE ALUMNOS - INACTIVOS";
        $r = $consulta->consultaCondicion("estatus_al", $est, "alumnos");
    }else{
        $titulo = "LISTADO DE ALUMNOS";
        $r = $consulta->consultaGeneral("alumnos");
    }




    $pdf = new PDF('L', 'mm', 'LETTER');
    $pdf->AddPage();
    $pdf->Ln(25);
    
    $pdf->SetFont('Arial', 'B', 15);
    $pdf->Cell(260,10, "Republica Bolivariana de Venezuela Secretaria del Poder Popular",0, 0,'C');
    $pdf->Ln();
    $pdf->Cell(260,10, "para el Desarrollo Comunal",0, 0,'C');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetTextColor(220,50,50);
    $pdf->Cell(260,10, $titulo,0, 0,'C');
    $pdf->SetDrawColor(0,80,180);
    $pdf->SetTextColor(000,000,000);
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetFillColor(230,230,0);
    $pdf->Cell(30,8, "CEDULA",1, 0,'C');
    $pdf->Cell(70,8, "NOMBRE Y APELLIDO",1, 0,'C');
    $pdf->Cell(30,8, "SEXO",1, 0,'C');
    $pdf->Cell(55,8, "FECHA DE NACIMIENTO",1, 0,'C');
    $pdf->Cell(35,8, "TELEFONO",1, 0,'C');
    $pdf->Cell(35,8, "ESTATUS",1, 0,'C');
    $pdf->SetFont('Arial', '', 13);
    $pdf->Ln();
    $i = 0;
    $m = 0;
    $fe = 0;
    while($f = $r->fetch_assoc()){
        $i++;
        if($f['sexo_al']=='Masculino'){
            $m++;
        }else{
            $fe++;
        }
        $pdf->Cell(30,6, $f['cedula_al'],1, 0,'C');
        $pdf->Cell(70,6,$f['nombres_al']." ".$f['apellidos_al'],1, 0,'L');
        $pdf->Cell(30,6, $f['sexo_al'],1, 0,'C');
        $pdf->Cell(55,6, $f['fecha_n_al'],1, 0,'C');
        $pdf->Cell(35,6, $f['telefono_al'],1, 0,'C');
        $pdf->Cell(35,6, $f['estatus_al'],1, 0,'L');
        $pdf->Ln();
    }
    $pdf->SetTextColor(220,50,50);
    $pdf->Cell(280,10, "Total de alumnos: $i    -    Total de hombres: $m       -       Total de mujeres $fe.",0, 0,'C');
    $pdf->Ln();

    $pdf->Output();
?>
