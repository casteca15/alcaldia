<?php
    include_once("Orm.php"); 

    class Sector{
        private $con = null;
        private $datos = array();
        private $tabla = "sectores";
        private $idc = "id_sector";
        private $head = array("id_sector", "codigo_sector", "des_com", "cedula", "nombres", "apellidos", "tele1", "tele2", "tele3");

        public function Sector($orm){
            $this->con = $orm;
        }

        public function cargarDatosArreglo($datos){
            $this->datos = $datos;
        }

        public function guardar(){
            $r = $this->con->insertar($this->datos, $this->tabla);
            if(!$r){
                return false;
            }else{
                if($r->affected_rows==1) 
                    return true;
                else
                    return false;
            }
        }

        public function buscar($id){
            $r = $this->con->consultaCondicion($this->idc, $id, $this->tabla);
            if($r->num_rows==1){
                $d = array();
                $filas = $r->fetch_array();
                for($i = 0; $i < $r->field_count; $i++)
                    $d[] = $filas[$i];
                $this->cargarDatosArreglo($d);
                return $d;
            }else{
                return "no_encontrado";
            }
        }

        public function eliminar($id){
            if($this->buscar($id)=="no_encontrado"){
                return "no_encontrado";
            }else{
                $r = $this->con->eliminar($this->idc, $id, $this->tabla);
                if($r)
                    return "eliminado";
                else
                    return false;
            }
        }

        public function editar($id){
            $r = $this->con->editar($this->head, $this->datos, $this->idc, $id, $this->tabla);
            return $r;
        }
        public function listar(){
            $sql = "SELECT * FROM sectores S;";
            $r = $this->con->consultaPersonalizada($sql);
            return $r;
        }

        public function listarP($sql){
            $r = $this->con->consultaPersonalizada($sql);
            return $r;
        }
    }

    $obj = new Sector($orm);
?>
