<?php
	class Conexion{
		private $host = "localhost";
		private $user = "root";
		private $pas  = "castellanos";
		private $db   = "alcaldia";
		public $con  = "";
		
		public function Conexion(){
			$this->conectar();
		}
		
        // Esta es la funcion que se que conecta al mysql
		public function conectar(){
			$this->con = new mysqli($this->host, $this->user, $this->pas, $this->db);
            $this->con->query("SET NAMES 'UTF8'");
		}
		
		public function ejecutar($comando){
			$this->con->query($comando);
			return $this->con;
		}
		
		public function consultar($comando){
			$resp = $this->con->query($comando);
			return $resp;
		}
	}
	$conx = new Conexion();
?>
