<?php
    include_once("Orm.php"); 

    class Usuario{
        private $con = null;
        private $datos = array();
        private $tabla = "usuario";
        private $idc = "usuario";
        private $head = array("usuario", "password", "nivel", "ced_emp");

        public function Usuario($orm){
            $this->con = $orm;
        }

        public function cargarDatosArreglo($datos){
            $this->datos = $datos;
        }

        public function guardar(){
            $r = $this->con->insertar($this->datos, $this->tabla);
            if(!$r){
                return false;
            }else{
                if($r->affected_rows==1) 
                    return true;
                else
                    return false;
            }
        }

        public function buscar($id){
            $r = $this->con->consultaCondicion($this->idc, $id, $this->tabla);
            if($r->num_rows==1){
                $d = array();
                $filas = $r->fetch_array();
                for($i = 0; $i < $r->field_count; $i++)
                    $d[] = $filas[$i];
                $this->cargarDatosArreglo($d);
                return $d;
            }else{
                return "no_encontrado";
            }
        }

        public function eliminar($id){
            if($this->buscar($id)==1){
                $r = $this->con->eliminar($this->idc, $id, $this->tabla);
                return $r;
            }else{
                return "no_encontrado";
            }
        }

        public function editar($id){
            $r = $this->con->editar($this->head, $this->datos, $this->idc, $id, $this->tabla);
            return $r;
        }
    }

    $usuario = new Usuario($orm);
?>
