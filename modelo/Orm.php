<?php
	include_once("Conexion.php");

    class Orm{
		private $mysql = null;
		
		public function Orm($mysql){
			$this->mysql = $mysql;
		}	


        public function insertarPersonalizado($sql){
			$resp = $this->mysql->ejecutar($sql);
			if($resp->errno){
				return false;
			}else{
				return $resp;
			}
        }

		public function insertar($datos, $tabla){
			$valores = $this->unir(",", $datos); 
		    $sql = "INSERT INTO $tabla VALUES($valores);";
			$resp = $this->mysql->ejecutar($sql);
			if($resp->errno){
				return false;
			}else{
				return $resp;
			}
		}

		public function consultaCondicion($campo, $valor, $tabla){
            $valor = $this->mysql->con->real_escape_string($valor);
            $sql = "SELECT * FROM $tabla WHERE $campo='$valor'";
			$resp = $this->mysql->consultar($sql);
			return $resp; 
		}
		
		public function consultaGeneral($tabla){
			$resp = $this->mysql->consultar("SELECT * FROM $tabla");
			return $resp;
		}

        public function ultimoCambioDeClave($usuario){
            $f = date('Y-m-d');
            $sql = "SELECT DATEDIFF('$f', fecha_cambio) n FROM usuario;";
			$resp = $this->mysql->consultar($sql);
            $x = $resp->fetch_array();
            return $x[0];
        }

        public function sesion($u, $p, $tabla){
            $u = $this->mysql->con->real_escape_string($u);
            $p = $this->mysql->con->real_escape_string($p);
            $sql = "SELECT * FROM usuarios where usuario='$u' AND password=md5('$p');";
            $r = $this->mysql->consultar($sql);
            if($f = $r->fetch_assoc()){
                if($f['nivel'] == 'WEBMASTER'){
                    return $this->mysql->consultar($sql);
                }else{
                    $sql = "SELECT * FROM usuarios as U WHERE U.usuario='$u' AND U.password=md5('$p');";
                }
            }else{
                $sql = "SELECT * FROM usuarios as U WHERE U.usuario='$u' AND U.password=md5('$p');";
            }
			$resp = $this->mysql->consultar($sql);
            if($resp->num_rows==1){
                return $resp;
            }else{
                return false;
            }
        }

        public function consultaPersonalizada($sql){
			$resp = $this->mysql->consultar($sql);
			return $resp;
        }

		public function eliminar($campo, $valor, $tabla){
            $valor = $this->mysql->con->real_escape_string($valor);
		    $sql = "DELETE FROM $tabla WHERE $campo='$valor'"; 	
			$resp = $this->mysql->ejecutar($sql);
			if($resp->affected_rows<1)
				return false;
			else
				return true;
		}

		public function editar($cabeceras, $nuevos, $campo, $valor, $tabla){
            $valor = $this->mysql->con->real_escape_string($valor);
			$valores = $this->unirUpdate($cabeceras, $nuevos, "=", ",");
            $sql = "UPDATE $tabla SET $valores WHERE $campo='$valor'";
			$resp = $this->mysql->ejecutar($sql);
			if($resp->affected_rows<1)
				return false;
			else
				return true;
		}

        public function editarPersonalizado($sql){
			$resp = $this->mysql->ejecutar($sql);
			if($resp->affected_rows<1)
				return false;
			else
				return true;
        }

		private function unir($separador, $datos){
			$cadena = "";
			for($i = 0; $i< count($datos); $i++){
                $datos[$i] = $this->mysql->con->real_escape_string($datos[$i]);
				if(is_numeric($datos[$i]))
					$cadena = $cadena.$datos[$i];
				else
					$cadena = $cadena."'".$datos[$i]."'";
				
				if($i != count($datos)-1)
					$cadena = $cadena.$separador;
			}
			return $cadena;
		}
		
		private function unirUpdate($cabeceras, $nuevos, $comparador, $separador){
			$cadena = "";
			for($i = 0; $i< count($nuevos); $i++){
                //$nuevos[$i] = $this->mysql->con->real_escape_string($nuevos[$i]);
				//if(is_numeric($nuevos[$i]))
					//$cadena = $cadena.$cabeceras[$i].$comparador.$nuevos[$i];
				//else
					$cadena = $cadena.$cabeceras[$i].$comparador."'".$nuevos[$i]."'";
				
				if($i != count($nuevos)-1)
					$cadena = $cadena.$separador;
			}
			return $cadena;
		}


    }
    $orm = new Orm($conx);
?>
