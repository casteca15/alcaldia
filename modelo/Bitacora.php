<?php
    session_start();
    include_once("Orm.php"); 

    class Bitacora{
        private $datos = array();
        private $tabla = "bitacora";
        private $con = null;

        public function Bitacora($orm){
            $this->con = $orm;
        }

        public function cargarDatosArreglo($datos){
            $this->datos = $datos;
        }

        public function guardar($u, $accion){
            $a[] = 0;
            $a[] = $u;
            $a[] = date('Y-m-d')." a las ".date('H:i:s');
            $a[] = $accion;
            $this->cargarDatosArreglo($a);

            $r = $this->con->insertar($this->datos, $this->tabla);
            if(!$r){
                return false;
            }else{
                if($r->affected_rows==1) 
                    return true;
                else
                    return false;
            }
        }

        public function buscar($id){
            $r = $this->con->consultaCondicion($this->idc, $id, $this->tabla);
            if($r->num_rows==1){
                $d = array();
                $filas = $r->fetch_array();
                for($i = 0; $i < $r->field_count; $i++)
                    $d[] = $filas[$i];
                $this->cargarDatosArreglo($d);
                return $d;
            }else{
                return "no_encontrado";
            }
        }

        public function editarPersonalizado($sql){
            return $this->con->editarPersonalizado($sql);
        }

        public function listar(){
            $r = $this->con->consultaGeneral($this->tabla);
            return $r;
        }

        public function listarP($sql){
            $r = $this->con->consultaPersonalizada($sql);
            return $r;
        }
    }
    $bitacora = new Bitacora($orm);
?>
