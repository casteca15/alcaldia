<?php
    include_once("Orm.php"); 

    class Solicitante{
        private $con = null;
        private $datos = array();
        private $tabla = "solicitantes";
        private $idc = "ced_sol";
        private $head = array("ced_sol", "nac_sol", "nom_sol", "ape_sol", "tel_sol", "tel2_sol", "tel3_sol", "sex_sol", "id_sector", "dir_sol");

        public function Solicitante($orm){
            $this->con = $orm;
        }

        public function cargarDatosArreglo($datos){
            $this->datos = $datos;
        }

        public function guardar(){
            $r = $this->con->insertar($this->datos, $this->tabla);
            if(!$r){
                return false;
            }else{
                if($r->affected_rows==1) 
                    return true;
                else
                    return false;
            }
        }

        public function buscar($id){
            $sql = "SELECT * FROM solicitantes E, sectores S WHERE E.ced_sol=$id AND E.id_sector=S.id_sector;";
            $r = $this->con->consultaPersonalizada($sql);
            if($r->num_rows==1){
                $d = array();
                $filas = $r->fetch_array();
                for($i = 0; $i < $r->field_count; $i++)
                    $d[] = $filas[$i];
                $this->cargarDatosArreglo($d);
                return $d;
            }else{
                return "no_encontrado";
            }
        }

        public function listar($sql){
            $r = $this->con->consultaPersonalizada($sql);
            return $r;
        }

        public function eliminar($id){
            if($this->buscar($id)=="no_encontrado"){
                return "no_encontrado";
            }else{
                $r = $this->con->eliminar($this->idc, $id, $this->tabla);
                if($r)
                    return "eliminado";
                else
                    return false;
            }
        }

        public function editar($id){
            $r = $this->con->editar($this->head, $this->datos, $this->idc, $id, $this->tabla);
            return $r;
        }
    }

    $obj = new Solicitante($orm);
?>

