<?php
    include_once("Orm.php"); 

    class Vacaciones{
        private $con = null;
        private $datos = array();
        private $tabla = "vacaciones";
        private $idc = "id_vacacion";
        private $head = array("id_vacacion", "est_vac", "fec_vac", "ced_emp_vac", "fec_ini_vac", "fec_fin_vac", "fec_rei_vac", "obs_vac");

        public function Vacaciones($orm){
            $this->con = $orm;
        }

        public function cargarDatosArreglo($datos){
            $this->datos = $datos;
        }

        public function guardar(){
            $r = $this->con->insertar($this->datos, $this->tabla);
            if(!$r){
                return false;
            }else{
                if($r->affected_rows==1) 
                    return true;
                else
                    return false;
            }
        }

        public function buscar($id){
            $r = $this->con->consultaCondicion($this->idc, $id, $this->tabla);
            if($r->num_rows==1){
                $d = array();
                $filas = $r->fetch_array();
                for($i = 0; $i < $r->field_count; $i++)
                    $d[] = $filas[$i];
                $this->cargarDatosArreglo($d);
                return $d;
            }else{
                return "no_encontrado";
            }
        }

        public function eliminar($id){
            if($this->buscar($id)=="no_encontrado"){
                return "no_encontrado";
            }else{
                $r = $this->con->eliminar($this->idc, $id, $this->tabla);
                if($r)
                    return "eliminado";
                else
                    return false;
            }
        }

        public function editar($id){
            $r = $this->con->editar($this->head, $this->datos, $this->idc, $id, $this->tabla);
            return $r;
        }
        
        public function editarPersonalizado($sql){
            return $this->con->editarPersonalizado($sql);
        }

        public function listar(){
            $r = $this->con->consultaGeneral($this->tabla);
            return $r;
        }
        public function listarP($sql){
            $r = $this->con->consultaPersonalizada($sql);
            return $r;
        }
    }

    $obj = new Vacaciones($orm);
?>
