<?php
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING); // Para que nos salgam warnings ni noticias
?>
<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Bienvenidos</title>
        <link rel="stylesheet" href="vista/css/bootstrap.min.css">
        <link rel="stylesheet" href="vista/css/estilos.css">
        <link rel="stylesheet" href="vista/css/estilos_slide.css">
        <link rel="stylesheet" href="vista/css/index.css">

        <!-- Custom CSS -->
        <link href="vista/css/landing-page.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="vista/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script src="vista/js/jquery-latest.min.js"></script>
        <script src="vista/js/bootstrap.min.js"></script>
        <script src="vista/js/unslider.min.js"></script>

        <script>

         $(document).on('ready', function(){
             if(window.chrome) {
             $('.banner li').css('background-size', '100% 100%');
             }

             $('.banner').unslider({
                fluid: true,
                dots: true,
                speed: 500,
                delay: 7000
                });
            });
        </script>
    </head>
    <body>
        <div id="contenedor"style="max-width: 100%; width:100%;">
            <a href="index.php"><img src="vista/img/minis.png"></a>
        </div>
    <nav class="navbar navbar-default topnav" role="navigation">
        <div class="container topnav">
            <div class="row">
                
                <div class="col-md-1">
                </div>
                <div class="col-md-10" style="height: 500px;">
                    <br><br><br><br>
                    <h4 style='margin-top:1em;'>SISTEMA PARA EL REGISTRO Y CONTROL DE TIERRAS</h4>
                        <!-- FORMULARIO PARA INICIAR SESION -->
                            <form  role="form" action="" method="POST" style="display: inline-block"> 
                                <input class="text" style='text-transform: none;width: 300px;' type="text" name="user" placeholder="Usuario" required>
                                <br>
                                <br>
                                <input class="text" style='text-transform: none;width:300px;' type="password" name="pass" placeholder="Contraseña" required>
                                <br>
                                <br>
                                <input class="btn btn-success" type="submit" id="btentrar" style="background: #1E8E77;" value="Entrar" name="bte" >
                                <br>
                                <?php
                                    include_once("controlador/CtUsuario.php");
                                    // Esto se ejecuta cuando presionan el boton entrar
                                    if(isset($_POST['bte'])){
                                        $u = $_POST['user'];
                                        $p = $_POST['pass'];
                                        $obj->iniciarSesion($u, $p);
                                    }
                                ?>
                            </form>
                        <!-- FIN DE FORMULARIO -->
                    <br><br><br><br>
                </div>
                <div class="col-md-1">
                </div>
            </div>
        </div>
    </nav>
        <footer>
            <br>
            <b>Todos los derechos reservados.</b>
        </footer>
    </body>
</html>


